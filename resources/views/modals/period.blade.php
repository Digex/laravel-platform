<!-- Period modal -->
<div id="periodModal" class="modal fade bs-example-modal-lg">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div id="" class="modal-body">
        <div class="row">
          <div class="col-md-12 container-box">
            <div class="white-box">
              <h1>Create Report for</h1>
              
              <form method="POST" @submit.prevent="create_period_form_submit" @keydown="create_period_form.errors.clear($event.target.name)">
                {{ csrf_field() }}
                
                <div class="form-group">
                  <label>Start Date:</label>
                  <datepicker name="start_date" v-model="create_period_form.start_date" :monday-first="true" :bootstrap-styling="true"></datepicker>
                  <span class="help is-danger" v-if="create_period_form.errors.has('start_date')" v-text="create_period_form.errors.get('start_date')"></span>
                </div>

                <div class="form-group">
                  <label>End Date:</label>
                  <datepicker name="end_date" v-model="create_period_form.end_date" :monday-first="true" :bootstrap-styling="true"></datepicker>
                  <span class="help is-danger" v-if="create_period_form.errors.has('end_date')" v-text="create_period_form.errors.get('end_date')"></span>
                  
                </div>
                <button class="button is-primary" v-bind:disabled="create_period_form.errors.any()">Create</button>
              </form>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>