<!-- Plugin modal -->
<div id="pluginModal" class="modal fade bs-example-modal-lg">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div id="" class="modal-body">
        <div class="row">
          <div class="col-md-12 container-box">
            <div class="white-box">
              <h1>Create Plugin</h1>
              <form method="POST" @submit.prevent="plugin_form_submit" @keydown="plugin_form.errors.clear($event.target.name)">
                {{ csrf_field() }}
                <div class="form-group">
                  <label>Name:</label>
                  <input type="text" class="form-control" name="name" v-model="plugin_form.name">
                  <span class="help is-danger" v-if="plugin_form.errors.has('name')" v-text="plugin_form.errors.get('name')"></span>
                </div>
                <button class="button is-primary" v-bind:disabled="plugin_form.errors.any()">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>