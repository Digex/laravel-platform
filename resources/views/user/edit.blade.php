@extends('layouts.admin')

@section('content')
<div id="appUser" class="container">
    <div class="row">
        <div class="col-md-12 container-box">
            <div class="white-box">
                <form method="POST" @submit.prevent="onSubmit" @keydown="form.errors.clear($event.target.name)">
                  {{ csrf_field() }}
                  <div class="form-group">
                    <label>Name:</label>
                    <input type="text" class="form-control" name="name"  v-model="form.name">
                    <span class="is-danger" v-if="form.errors.has('name')" v-text="form.errors.get('name')"></span>
                  </div>
                  <div class="form-group">
                    <label>Lastname:</label>
                    <input type="text" class="form-control" name="lastname"  v-model="form.lastname">
                    <span class="is-danger" v-if="form.errors.has('lastname')" v-text="form.errors.get('lastname')"></span>
                  </div>
                  <div class="form-group">
                    <label>Email: </label>
                    <input type="email" class="form-control" name="email"  v-model="form.email">
                    <span class="help is-danger" v-if="form.errors.has('email')" v-text="form.errors.get('email')"></span>
                  </div>
                  <div class="form-group">
                    <label>Password: </label>
                    <input type="password" class="form-control" name="password"  v-model="form.password">
                    <span class="help is-danger" v-if="form.errors.has('password')" v-text="form.errors.get('password')"></span>
                  </div>
                  <div class="form-group">
                    <label>Password Confirm: </label>
                    <input type="password" class="form-control" name="password_confirmation"  v-model="form.password_confirmation">
                    <span class="help is-danger" v-if="form.errors.has('password_confirmation')" v-text="form.errors.get('password_confirmation')"></span>
                  </div>

                  <button class="button is-primary" v-bind:disabled="form.errors.any()">Create</button>
                </form>
            </div>


        </div>
    </div>
</div>

@endsection

@section('scripts')
    <script src="{{ asset('js/vueUser.js') }}"></script>
@endsection