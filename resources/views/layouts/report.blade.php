<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Styles -->

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{ asset('img/favicon.ico') }}?123">
    <link rel="apple-touch-icon" href="{{ asset('img/dashboard_favicon-ipad.png') }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>

    <div id="appDashboard" class="container header">
        <div class="row">
            <div class="col-md-8">
                <div class="row">
                  <div class="col-md-3 no-padding-right">
                    <img src="{{asset('uploads/'.$client->logo)}}" class="img-responsive">
                  </div>
                  <div class="col-md-9">
                    <h1 style="color:#{{$client->primary_color}}">MySiteDashboard</h1>
                    <h2>{{$client->name}} - <a href="{{$client->site}}">
                        @php
                            $input = $client->site;
                            if (!preg_match('#^http(s)?://#', $input)) {
                                $input = 'http://' . $input;
                            }
                            $urlParts = parse_url($input);
                            $domain = preg_replace('/^www\./', '', $urlParts['host']);
                            echo $domain;
                        @endphp
                    </a></h2>
                  </div>
                </div>
                
            </div>
            <div class="col-md-4">
                <!-- Right Side Of Navbar -->

                <ul class="nav navbar-nav ">
                    <li>
                        <client-component 
                            @setclient="getClient" client={{$clientId}}
                            @setperiod="getPeriod" period="{{$periodId}}"
                            ref="ClientComponent">
                        </client-component>
                        <label>View Previous Month: </label>
                        <select type="text" name="period" v-on:change="changePeriod" v-model="period_id" >
                            <option v-for="period in listPeriods" :value="period.id">
                                @{{period.month}} @{{period.year}}
                            </option>
                        </select> 
                    </li>
                    <li class="logout">
                        <a href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                            <span>Sign Out</span>
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-6">
                            <a v-if="period_id != 0" class="button-blue" :href="'/period/'+ period_id +'/edit'">Edit</a>                            
                        </div>
                        <div class="col-xs-6">
                            <a class="button-aprove" :href="'/period/' + client_id + '/create'">New Report</a>   
                        </div>
                    </div>
                </div>
                 
            </div>
        </div>
    </div>

    <div class="content">
        @yield('content')  
    </div>


        <!-- Scripts -->
        <script src="{{ asset('js/vendor.js') }}"></script>
        <script src="{{ asset('js/vueBus.js') }}"></script>
        <script src="{{ asset('js/vueDashboard.js') }}"></script>

        @yield('scripts')
</body>
</html>
