<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Styles -->

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{ asset('img/favicon.ico') }}?123">
    <link rel="apple-touch-icon" href="{{ asset('img/dashboard_favicon-ipad.png') }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
  <div id="appDashboard">
    <div class="preheader">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-9 col-lg-10  hidden-print">
            <img src="{{asset('img/MySiteDashboard-lightgray-logo.svg')}}" class="img-responsive">
          </div>
          <div class="col-xs-12 col-sm-3 col-lg-2 bold row-eq-height-container hidden-print">
            <div class="col-xs-12 col-sm-3 no-padding row-eq-height hidden-print">
              <div class="minilogo ">
                <img src="{{asset('uploads/'.$client->logo)}}" class="img-responsive ">  
              </div>
            </div>
            <div class="col-xs-12 col-sm-9 no-padding-left row-eq-height hidden-print">
              {{$userName}}, <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><span>Sign Out</span></a>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  {{ csrf_field() }}
              </form>
            </div>

          </div>
        </div>
      </div>
    </div>

    <div  class="container header">
        <div class="row">
            <div class="col-md-6">
                <div class="row row-eq-height-container">
                  <div class="col-xs-12 col-md-2 no-padding-right row-eq-height">
                    <img src="{{asset('uploads/'.$client->logo)}}" class="img-responsive">
                  </div>
                  <div class="col-xs-12 col-md-10 row-eq-height">
                    <h1 style="color:#{{$client->primary_color}}">{{$client->name}}</h1>
                    <h2><a href="{{$client->site}}">
                        @php
                            $input = $client->site;
                            if (!preg_match('#^http(s)?://#', $input)) {
                                $input = 'http://' . $input;
                            }
                            $urlParts = parse_url($input);
                            $domain = preg_replace('/^www\./', '', $urlParts['host']);
                            echo $domain;
                        @endphp
                    </a></h2> 
                  </div>
                </div>
                
            </div>
            <div class="col-md-6 text-right select-container">
                    <client-component 
                        @setclient="getClient" client={{$clientId}}
                        @setperiod="getPeriod" period="{{$periodId}}"
                        ref="ClientComponent">
                    </client-component>
                    <label class="hidden-print">Select a Month: </label>
                    <select type="text" name="period" v-on:change="changePeriod" v-model="period_id" >
                        <option v-for="period in listPeriods" :value="period.id">
                            @{{period.month}} @{{period.year}}
                        </option>
                    </select> 
            </div>
        </div>
    </div>
  </div>

    <div class="content">
        @yield('content')  
    </div>

    
    <div class="container">
        <div class="row print-section">
            <div class="col-xs-12 text-right">
            <a href="#" class="btn-file hidden-print visible-lg visible-md" onclick="window.print();">Print</a>
            </div>
        </div>
    </div>

    @include('partials.footer') 

        <!-- Scripts -->
        <script src="{{ asset('js/vendor.js') }}"></script>
        <script src="{{ asset('js/vueBus.js') }}"></script>
        <script src="{{ asset('js/vueDashboard.js') }}"></script>

        @yield('scripts')
</body>
</html>
