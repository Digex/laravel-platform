<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{ asset('img/favicon.ico') }}?123">
    <link rel="apple-touch-icon" href="{{ asset('img/dashboard_favicon-ipad.png') }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="appLogin" class="login">
      <div class="container header">
        <div class="row">
          <div class="col-md-12">
                  <img src="{{asset('img/MySiteDashboard-fullcolor-logo.svg')}}" class="img-responsive">
                  @php
                      if($client){
                        $input = $client->site;
                        if (!preg_match('#^http(s)?://#', $input)) {
                            $input = 'http://' . $input;
                        }
                        $urlParts = parse_url($input);
                        $domain = preg_replace('/^www\./', '', $urlParts['host']);  
                      }
                  @endphp

                  @if ($client)
                    <h2>{{$client->name}} - <a href="{{$client->site}}">{{$domain}}</a></h2>    
                  @else
                    <h2>{{$agency->name}}</h2>      
                  @endif      
          </div>
        </div>
        
      </div>
      <div class="content">
        @yield('content')  
      </div>

      @include('partials.footer') 

    </div>

    <!-- Scripts -->
    {{--<script src="{{ asset('js/app.js') }}"></script>--}}
</body>
</html>
