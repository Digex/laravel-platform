@extends('layouts.login')

@section('content')
<div class="container login-form email">
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
              <div class="row row-eq-height-container">

                <div class="col-md-4 logo">
                  <img src="{{asset('uploads/'.$client->logo)}}" class="img-responsive">
                </div>
                <div class="col-md-8">
                  <div class="panel-body">
                      <div class="col-md-7 no-padding">
                        <h2>Reset Password</h2>
                        @if (session('status') == 'success')
                            <div class="alert alert-success">
                                We have e-mailed your password reset link!
                            </div>
                        @endif

                        <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                            {{ csrf_field() }}

                            <div class="">
                                    <input type="hidden" name="subdomain" value="{{ $subdomain }}">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="E-Mail Address" required>
                                    
                                    @if (session('status') == 'error')
                                        <span class="help is-danger">We can't find a user with that e-mail address.</span>
                                    @endif

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                            </div>

                            <div class="">
                                    <button type="submit" class="button-green no-margin-sides">
                                        Send Password Reset Link
                                    </button>
                            </div>
                        </form>
                      </div>

                  </div>
                </div>
              </div>  


            </div>
        </div>
    </div>
</div>
@endsection
