@extends('layouts.login')

@section('content')
<div class="container login-form">
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="row row-eq-height-container">

                  <div class="col-md-4 logo">
                    <img src="{{asset('uploads/'.$client->logo)}}" class="img-responsive">
                  </div>
                  <div class="col-md-8">
                    <div class="panel-body">
                      <div class="col-md-7 no-padding">
                        <h2>Reset Password</h2>
                        <form class="form-horizontal" method="POST" action="{{ route('password.request') }}">
                            {{ csrf_field() }}

                            <input type="hidden" name="subdomain" value="{{ $subdomain }}">
                            <input type="hidden" name="token" value="{{ $token }}">

                            <div class="">

                                    <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" placeholder="E-Mail Address" required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="help is-danger">{{ $errors->first('email') }}</span>
                                    @endif
                            </div>

                            <div class="">

                                    <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>

                                    @if ($errors->has('password'))
                                        <span class="help is-danger">{{ $errors->first('password') }}</span>
                                    @endif
                            </div>

                            <div class="">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required>

                                    @if ($errors->has('password_confirmation'))
                                        <span class="help is-danger">{{ $errors->first('password_confirmation') }}</span>
                                    @endif
                                    @if (session('error'))
                                        <span class="help is-danger">{{ session()->get('error') }}</span>
                                    @endif
                            </div>

                            <div class="">
                                    <button type="submit" class="button-green no-margin-sides">
                                        Reset Password
                                    </button>
                                </div>
                            </div>
                        </form>
                      </div>

                    </div>

                  </div>
                </div>


            </div>
        </div>
    </div>
</div>
@endsection
