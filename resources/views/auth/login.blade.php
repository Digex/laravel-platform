@extends('layouts.login')

@section('content')
<div class="container login-form">
    <div class="row">
        <div class="col-md-12 ">
            <div class="white-box">
                <div class="row row-eq-height-container">
                  <div class="col-xs-12 col-md-4 row-eq-height logo">
                      @if ($client)
                        <img src="{{asset('uploads/'.$client->logo)}}" class="img-responsive">
                      @else
                          <img src="{{asset('uploads/'.$agency->logo)}}" class="img-responsive">
                      @endif  
                  </div>
                  <div class="col-xs-12 col-md-8 row-eq-height">
                    <div class="panel-body">
                        <h1>MySiteDashboard Login</h1>

                        <div class="col-md-9 no-padding">
                          <form class="form-horizontal" method="POST" action="{{ route('post.login') }}">
                              {{ csrf_field() }}

                              <div class="row">
                                <div class="col-md-12">
                                    <input type="hidden" name="subdomain" value="{{ $subdomain }}">
                                    <input id="email" type="email" class="form-control" name="email" value="" required autofocus placeholder="Username">  
                                </div>
                              </div>    
                              
                              <div class="row">
                                <div class="col-md-12">
                                    <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>
                                    @if (session('error'))
                                        <span class="help is-danger">{{ session()->get('error') }}</span>
                                    @endif
                                </div>
                              </div>

                              <div class="row">
                                <div class="col-md-5 col-lg-4">
                                    <button type="submit" class="button-green no-margin-sides">
                                        Login
                                    </button>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="remember"> Remember Me
                                        </label>
                                    </div>
                                </div>

                                <div class="col-md-7 col-lg-8 no-padding-left">
                                    @if ($subdomain == 'root')
                                        <a class="btn btn-link" href="{{ route('password.request') }}">
                                            Forgot Your Password?
                                        </a>
                                    @else
                                        <a class="btn btn-link" href="{{ route('client.password.request', ['subdomain' => $subdomain]) }}">
                                            Forgot Your Password?
                                        </a>
                                    @endif
                                </div>
                              </div>

                          </form>
                        </div>   

                    </div>
                  </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
