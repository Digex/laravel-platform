@extends('layouts.admin')

@section('content')

{{-- Vue App --}}
<div id="appClient">
    {{-- Header --}}
    <div id="header" class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-xs-10 col-sm-6 col-md-8">
                        <h3>Clients</h3>
                    </div>
                    <div class="col-xs-6 col-sm-3 col-md-2 no-padding-right">
                        <a href="agency/create" class="button-add pull-right">+&nbsp;&nbsp;Add Agency</a>
                    </div>
                    <div class="col-xs-6 col-sm-3 col-md-2 no-padding-left">
                        <a href="client/create" class="button-add pull-right">+&nbsp;&nbsp;Add Client</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- /Header --}}
    {{-- Content --}}
    <div class="container">
      <agency-information
        v-for="agency in agencies"
        :key="agency.id"
        :id="agency.id"
        :logo="agency.logo"
        :name="agency.name"
        :users="agency.users"
        :count="agency.clients"
        @deleteagency="deleteagency"
        @deleteclientagency="deleteclientagency"
      ></agency-information> 
    </div>
    
    <confirm-modal
        :title="modal.title"
        :text="modal.text"
        :button="modal.button"
        :type="modal.type"
        class="red"
        @confirm="confirm"
    ></confirm-modal>
    {{-- /Content --}}
</div>
{{-- /Vue App --}}

@endsection

@section('scripts')
    <script src="{{ asset('js/vueClient.js') }}"></script>
@endsection