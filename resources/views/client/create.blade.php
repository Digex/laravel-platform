@extends('layouts.admin')

@section('content')

{{-- Vue App --}}
<div id="appClient">
    {{-- Header --}}
    <div id="header" class="container">
        <div class="row">
            <div class="col-xs-12">
                <a class="button-back" href="/client">< Back to Clients</a>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <h3>Create Client</h3>
            </div>
        </div>
    </div>
    {{-- /Header --}}
    {{-- Content --}}
    <div id="content" class="container">
        <div class="row">
            <div class="col-md-12">
                <form method="POST" @submit.prevent="onSubmit" @keydown="form.errors.clear($event.target.name)" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-7 col-lg-8">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Name:</label>
                                        <input type="" class="form-control" name="name"  v-model="form.name">
                                        <span class="help is-danger" v-if="form.errors.has('name')" v-text="form.errors.get('name')"></span>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Subdomain:</label>
                                        <input type="text" class="form-control" name="subdomain" v-model="form.subdomain" >
                                        <span class="help is-danger" v-if="form.errors.has('subdomain')" v-text="form.errors.get('subdomain')"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Website:</label>
                                        <input type="text" class="form-control" name="site"  v-model="form.site">
                                        <span class="help is-danger" v-if="form.errors.has('site')" v-text="form.errors.get('site')"></span>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Google View ID:</label>
                                        <input type="text" class="form-control" name="view_id"  v-model="form.view_id">
                                        <span class="help is-danger" v-if="form.errors.has('view_id')" v-text="form.errors.get('view_id')"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label>Primary Color:</label>
                                        <color-picker  name="color1"  :color="form.color1" v-model="form.color1" />
                                        <span class="help is-danger" v-if="form.errors.has('color1')" v-text="form.errors.get('color1')"></span>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label>Secondary Color:</label>
                                        <color-picker name="color2"  :color="form.color2" v-model="form.color2" />
                                        <span class="help is-danger" v-if="form.errors.has('color2')" v-text="form.errors.get('color2')"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" style="width: 100%;">
                                            <tbody>
                                                <tr>
                                                    <td align="left" valign="top" style="">
                                                        <label>Agency:</label>
                                                        <select class="form-control" name="agency"  v-model="form.agency" v-on:click="form.errors.clear($event.target.name)">
                                                            <option v-for="agency in listAgencies" :value="agency.id" selected="agency.id == currentAgency" >@{{ agency.name }}</option>
                                                        </select>
                                                    </td>
                                                    <td align="right" valign="top" width="170" style="">
                                                        <a href="#" class="button-add" data-toggle="modal" data-target="#agencyModal">+&nbsp;&nbsp;New Agency</a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <span class="help is-danger" v-if="form.errors.has('agency')" v-text="form.errors.get('agency')"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5 col-lg-4">
                            <div class="form-group">
                                <label>Logo</label>
                                <label class="btn-file">
                                    Browse... <input type="file" class="form-control" name="logo"  ref="file"  v-on:change="previewFile" v-on:click="form.errors.clear($event.target.name)">
                                </label>
                                <span class="help is-danger" v-if="form.errors.has('logo')" v-text="form.errors.get('logo')"></span>
                                <img class="preview img-responsive" :src="form.logo" v-if="form.logo">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <button class="button-submit" v-bind:disabled="form.errors.any()">Create</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <agency-modal @updateagency="getNewAgency" class="blue"></agency-modal>
    {{-- /Content --}}
</div>
{{-- /Vue App --}}

@endsection

@section('scripts')
    <script src="{{ asset('js/vueClientCreate.js') }}"></script>
@endsection