@extends('layouts.admin')

@section('content')

{{-- Vue App --}}
<div id="appClientShow">
    {{-- Header --}}
    <div id="client-header" v-if="clientName">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h3>@{{clientName}}</h3>
                </div>
            </div>
        </div>
    </div>
    <div id="header" class="container">
        <div class="row">
            <div class="col-xs-12">
                <a class="button-back" href="/client">< Back to Clients</a>
                <client-component @setclient="getClient" client={{$id}} ></client-component>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-xs-6">
                        <h3>Users</h3>
                    </div>
                    <div class="col-xs-6">
                        <a @click.prevent="newUser" class="button-add pull-right">+&nbsp;&nbsp;Add New</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- /Header --}}
    {{-- Content --}}
    <div id="content" class="container">
        <div class="row">
            <div class="col-sm-12">
                    <user-information
                        v-for="user in users"
                        :key="user.id"
                        :id="user.id"
                        :name="user.name"
                        :lastname="user.lastname"
                        :email="user.email"
                        @edit="editUser"
                        @del="deleteUser"
                    ></user-information>
            </div>
        </div>
    </div>
    <user-modal 
        v-if="clientId" 
        :id="clientId"
        :edit="edit"
        :info="editInfo"
        class="blue"
        @update="getUsers"
    ></user-modal>
    <confirm-modal
        :title="modal.title"
        :text="modal.text"
        :button="modal.button"
        class="red"
        @confirm="confirm"
    ></confirm-modal>
    {{-- /Content --}}
</div>
{{-- /Vue App --}}

@endsection

@section('scripts')
    <script src="{{ asset('js/vueClientShow.js') }}"></script>
@endsection