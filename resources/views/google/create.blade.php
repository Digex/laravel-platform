@extends('layouts.admin')

@section('content')

<div id="appGoogleAnalytics" class="container">
    <div class="row">
        <div class="white-box">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-sm-6">
                        <p>Client</p>
                        <select v-model="selectedClient" v-on:change="getPeriods">
                            <option value="">Select</option>
                            <option v-for="client in clients" :value="client">@{{ client.name }}</option>
                        </select>  
                    </div>
                    <div class="col-sm-6">
                        <p>Period</p>
                        <select v-model="selectedPeriod">
                            <option value="">Select</option>
                            <option v-for="period in periods" :value="period">@{{ period.start }} - @{{ period.end }}</option>
                        </select>  
                    </div>
                </div>
            </div>
            <div v-if="selectedClient">
                <div v-if="metrics" v-cloak>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <h6>New Visitors: @{{ metrics.new_visitors }}%</h6>
                                <h6>Returning Visitors: @{{ metrics.returning_visitors }}%</h6>
                            </div>
                            <div class="col-sm-6">
                                <h6>@{{ metrics.day_top }}</h6>
                                <h6>1. @{{ metrics.day_1 }}</h6>
                                <h6>2. @{{ metrics.day_2 }}</h6>
                                <h6>3. @{{ metrics.day_3 }}</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-sm-4">
                                <h6>@{{ metrics.unique }}</h6>
                                <p>
                                    <div v-if="metrics.unique_percentage < 0" class="text-danger">! @{{ metrics.unique_percentage * -1 }}%</div>
                                    <div v-else class="text-success">¡ @{{ metrics.unique_percentage }}%</div>
                                </p>
                            </div>
                            <div class="col-sm-4">
                                <h6>@{{ metrics.total }}</h6>
                                <p>
                                    <div v-if="metrics.total_percentage < 0" class="text-danger">! @{{ metrics.total_percentage * -1 }}%</div>
                                    <div v-else class="text-success">¡ @{{ metrics.total_percentage }}%</div>
                                </p>
                            </div>
                            <div class="col-sm-4">
                                <h6>@{{ metrics.conversion }}</h6>
                                <p>
                                    <div v-if="metrics.conversion_percentage < 0" class="text-danger">! @{{ metrics.conversion_percentage * -1 }}%</div>
                                    <div v-else class="text-success">¡ @{{ metrics.conversion_percentage }}%</div>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <h5>@{{ metrics.avg_pages }}</h5>
                            </div>
                            <div class="col-sm-6">
                                <h5>@{{ metrics.avg_time }}</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div v-if="traffic" v-cloak>
                    <div class="col-md-12">
                        <p>@{{ traffic.page_1_link }}  <span class="pull-right">@{{ traffic.page_1_link_views }}</span></p>
                        <p>@{{ traffic.page_2_link }}  <span class="pull-right">@{{ traffic.page_2_link_views }}</span></p>
                        <p>@{{ traffic.page_3_link }}  <span class="pull-right">@{{ traffic.page_3_link_views }}</span></p>
                        <p>@{{ traffic.page_4_link }}  <span class="pull-right">@{{ traffic.page_4_link_views }}</span></p>
                        <p>@{{ traffic.page_5_link }}  <span class="pull-right">@{{ traffic.page_5_link_views }}</span></p>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-sm-3">@{{ traffic.android }}%</div>
                            <div class="col-sm-3">@{{ traffic.windows }}%</div>
                            <div class="col-sm-3">@{{ traffic.mac + traffic.ios }}%</div>
                            <div class="col-sm-3">@{{ traffic.linux }}%</div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-sm-3">@{{ traffic.chrome }}%</div>
                            <div class="col-sm-3">@{{ traffic.firefox }}%</div>
                            <div class="col-sm-3">@{{ traffic.edge }}%</div>
                            <div class="col-sm-3">@{{ traffic.safari }}%</div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-sm-4">@{{ traffic.desktop }}%</div>
                            <div class="col-sm-4">@{{ traffic.mobile }}%</div>
                            <div class="col-sm-4">@{{ traffic.tablet }}%</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="padding-top: 30px;">
                    <button type="button" class="btn btn-primary" v-on:click="getAnalytics">Get Analytics</button>
                    <button type="button" class="btn btn-success" v-if="metrics" v-on:click="storeAnalytics">Save</button>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
    <script src="{{ asset('js/vueGetAnalytics.js') }}?1000"></script>
@endsection