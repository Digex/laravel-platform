<div id="navbar">
    <div class="container">
        <div class="row">
            <div class="col-xs-6">
                <img src="{{asset('img/MySiteDashboard-fullcolor-logo.svg')}}" class="img-responsive">
            </div>
            <div class="col-xs-6 text-right">
                <p>{{ Auth::user()->name }},
                <a href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    sign out
                </a></p>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
    </div>
</div>