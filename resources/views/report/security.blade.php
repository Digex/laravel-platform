<div class="row report-section">
    <div class="col-md-12">
        <div class="row title">
            <div class="col-xs-12">
                <h1 class="status">Website Security Snapshot</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6 col-md-3">
                <div class="form-group">
                    <label>Site Security:</label>
                    <select class="form-control" name="https" v-model="edit_form.https" @change="edit_form.errors.clear($event.target.name)">
                        <option value="1">HTTPS</option>
                        <option value="0">HTTP</option>
                    </select>
                    <span class="help is-danger" v-if="edit_form.errors.has('https')" v-text="edit_form.errors.get('https')"></span>
                </div>
            </div>
            <div class="col-xs-6 col-md-3">
                <div class="form-group">
                    <label>Certified by:</label>
                    <input type="text" name="certified" class="form-control"  v-model="edit_form.certified">
                    <span class="help is-danger" v-if="edit_form.errors.has('certified')" v-text="edit_form.errors.get('certified')"></span>
                </div>
            </div>
            <div class="col-xs-6 col-md-3">
                <div class="form-group">
                    <label>Expiration Date: </label>
                    <date-picker name="expiration" v-model="edit_form.expiration" :monday-first="true" @selected="clearError('expiration')"></date-picker>
                    <span class="help is-danger" v-if="edit_form.errors.has('expiration')" v-text="edit_form.errors.get('expiration')"></span>
                </div>
            </div>
            <div class="col-xs-6 col-md-3">
                <div class="form-group">
                    <label>Attacks Blocked</label>
                    <input name="attacks" class="form-control"  v-model="edit_form.attacks" type="number">
                    <span class="help is-danger" v-if="edit_form.errors.has('attacks')" v-text="edit_form.errors.get('attacks')"></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="form-group">
                    <label>Attack Country of Origin/Attacks:</label> 
                    <div class="row">
                        <div class="col-xs-8">
                            <select class="form-control" name="country_1" v-model="edit_form.country_1" @change="edit_form.errors.clear($event.target.name)"> 
                                @include('partials.countries') 
                            </select> 
                            <span class="help is-danger" v-if="edit_form.errors.has('country_1')" v-text="edit_form.errors.get('country_1')"></span> 
                        </div>
                        <div class="col-xs-4">
                            <input name="country_1_number" class="form-control"  v-model="edit_form.country_1_number" type="number"> 
                            <span class="help is-danger" v-if="edit_form.errors.has('country_1_number')" v-text="edit_form.errors.get('country_1_number')"></span> 
                        </div>  
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <div class="row">
                        <div class="col-xs-8">
                            <select class="form-control" name="country_2" v-model="edit_form.country_2" @change="edit_form.errors.clear($event.target.name)"> 
                                @include('partials.countries') 
                            </select> 
                            <span class="help is-danger" v-if="edit_form.errors.has('country_2')" v-text="edit_form.errors.get('country_2')"></span> 
                        </div>
                        <div class="col-xs-4">
                            <input name="country_2_number" class="form-control"  v-model="edit_form.country_2_number" type="number"> 
                            <span class="help is-danger" v-if="edit_form.errors.has('country_2_number')" v-text="edit_form.errors.get('country_2_number')"></span> 
                        </div>  
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <div class="row">
                        <div class="col-xs-8">
                            <select class="form-control" name="country_3" v-model="edit_form.country_3" @change="edit_form.errors.clear($event.target.name)"> 
                                @include('partials.countries') 
                            </select> 
                            <span class="help is-danger" v-if="edit_form.errors.has('country_3')" v-text="edit_form.errors.get('country_3')"></span> 
                        </div>
                        <div class="col-xs-4">
                            <input name="country_3_number" class="form-control"  v-model="edit_form.country_3_number" type="number"> 
                            <span class="help is-danger" v-if="edit_form.errors.has('country_3_number')" v-text="edit_form.errors.get('country_3_number')"></span> 
                        </div>  
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <div class="row">
                        <div class="col-xs-8">
                            <select class="form-control" name="country_4" v-model="edit_form.country_4" @change="edit_form.errors.clear($event.target.name)"> 
                                @include('partials.countries') 
                            </select> 
                            <span class="help is-danger" v-if="edit_form.errors.has('country_4')" v-text="edit_form.errors.get('country_4')"></span> 
                        </div>
                        <div class="col-xs-4">
                            <input name="country_4_number" class="form-control"  v-model="edit_form.country_4_number" type="number"> 
                            <span class="help is-danger" v-if="edit_form.errors.has('country_4_number')" v-text="edit_form.errors.get('country_4_number')"></span>  
                        </div>  
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <div class="row">
                        <div class="col-xs-8">
                            <select class="form-control" name="country_5" v-model="edit_form.country_5" @change="edit_form.errors.clear($event.target.name)"> 
                                @include('partials.countries') 
                            </select> 
                            <span class="help is-danger" v-if="edit_form.errors.has('country_5')" v-text="edit_form.errors.get('country_5')"></span> 
                        </div>
                        <div class="col-xs-4">
                            <input name="country_5_number" class="form-control"  v-model="edit_form.country_5_number" type="number"> 
                            <span class="help is-danger" v-if="edit_form.errors.has('country_5_number')" v-text="edit_form.errors.get('country_5_number')"></span> 
                        </div>  
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
