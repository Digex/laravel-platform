@extends('layouts.admin')

@section('content')
    {{-- Vue App --}}
    <div id="appReportList">
        {{-- Header --}}
        <div id="client-header" v-if="clientName">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h3>@{{clientName}}</h3>
                    </div>
                </div>
            </div>
        </div>
        <div id="header" class="container">
            <div class="row">
                <div class="col-xs-12">
                    <a class="button-back" href="/client">< Back to Clients</a>
                    <client-component @setclient="getClient" client={{$clientId}} ></client-component>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <h3>Reports</h3>
                </div>
                <div class="col-xs-6">
                    <a :href="'/period/'+clientId+'/create'" class="button-add pull-right">+&nbsp;&nbsp;New Report</a>
                </div>
            </div>
        </div>
        {{-- /Header --}}
        {{-- Content --}}
        <div id="content" class="container">
            <div class="row">
                <div class="col-sm-12">
                        <period-information
                            v-for="period in periods"
                            :key="period.id"
                            :id="period.id"
                            :month="period.month"
                            :year="period.year"
                            :start="period.start"
                            :end="period.end"
                            :issues="period.issues_status"
                            @del="deleteReport"
                            @reset="resetReport"
                        ></period-information>
                </div>
            </div>
        </div>
        <confirm-modal
            :title="modal.title"
            :text="modal.text"
            :button="modal.button"
            class="red"
            @confirm="confirm"
        ></confirm-modal>
        {{-- /Content --}}
    </div>
    {{-- /Vue App --}}

@endsection

@section('scripts')
    <script src="{{ asset('js/vueReportList.js') }}"></script>
@endsection