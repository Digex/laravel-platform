@extends('layouts.admin')

@section('content')
<div id="appReport">
    {{-- Header --}}
    <div id="client-header" v-if="clientName">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h3>@{{clientName}}</h3>
                </div>
            </div>
        </div>
    </div>
    <div id="header" class="container">
        <div class="row">
            <div class="col-xs-12">
                <a v-if="client_id" class="button-back" :href="'/report/list/'+client_id">< Back to Reports</a>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <h3>Create Report</h3>
            </div>
        </div>
    </div>
    {{-- /Header --}}
    <form id="edit_form"  method="POST" @submit.prevent="edit_form_submit" @keydown="edit_form.errors.clear($event.target.name)" autocomplete="off">
        <div class="container">
            @include('report.period')
            @include('report.status')
            @include('report.server')
            @include('report.security')
            @include('report.code')
            @include('report.issues')
            @include('report.google')
            <div class="row">
                <div class="col-xs-12 col-md-2">
                    <div class="form-group">
                        <button v-show="buttonAnalytics" class="button-add" v-bind:disabled="edit_form.errors.any()">Create</button>
                    </div>  
                </div>
                <div class="col-xs-12 col-md-10">
                  <span class="is-danger" v-if="edit_form.errors.any()">Please verify the empty/error fields <a href="#" @click.prevent="scrollTop"> <img width="32" height="32" src="{{ asset('img/scrolltop.png') }}"></a></span>
                </div>
            </div>
        </div>
    </form>
  <plugin-modal @updateplugin="getNewPlugin"></plugin-modal>
</div>
@endsection

@section('scripts')
    <script src="{{ asset('js/vueReportCreate.js') }}"></script>
@endsection