<div class="row report-section">
    <div class="col-md-12">
        <div class="row title">
            <div class="col-xs-12">
                <h1 class="status">Period</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6 col-md-3">
                <div class="form-group">
                    <label>Start date:</label>
                    <date-picker name="start_date" v-model="edit_form.start_date" :monday-first="true" :use-utc='true' @selected="clearError('start_date')" autocomplete="off" ></date-picker>
                    <span class="help is-danger" v-if="edit_form.errors.has('start_date')" v-text="edit_form.errors.get('start_date')"></span>
                </div>
            </div>
            <div class="col-xs-6 col-md-3">
                <label>End date:</label>
                <date-picker name="end_date" v-model="edit_form.end_date" :monday-first="true" :use-utc='true' @selected="clearError('end_date')" autocomplete="off"></date-picker>
                <span class="help is-danger" v-if="edit_form.errors.has('end_date')" v-text="edit_form.errors.get('end_date')"></span>
            </div>
        </div>
    </div>
</div>  