<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                @if ($client->agency_id == 1)
                  <img src="{{asset('img/MySiteDashboard-fullcolor-logo.svg')}}" class="img-responsive">
                @else 
                  <img src="{{asset('uploads/'.$agency->logo)}}" class="img-responsive">
                @endif 
            </div>
            <div class="col-md-8 company-info">
                <p>&copy; 2018 {{$agency->name}}. All rights reserved.</p>
                @if ($client->agency_id == 1)
                    <p>1726 East Branch Hollow Drive &bull; Carrollton, Texas 75007</p>
                    <p><a href="mailto:connect@mysitedashboard.com">connect@mysitedashboard.com</a></p>
                @else
                    <p>{{ $agency->address }}</p>
                    <p><a href="{{ $agency->site }}">{{ $agency->site }}</a></p>
                @endif
            </div>
        </div>
    </div>
</div>