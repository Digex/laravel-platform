@extends('layouts.admin')

@section('content')
    {{-- Vue App --}}
    <div id="appDashboard">
        {{-- Header --}}
        <div id="client-header">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h3>{{$client->name}}</h3>
                    </div>
                </div>
            </div>
        </div>
        <div id="header" class="container">
            <div class="row">
                <div class="col-xs-12">
                    <a class="button-back" href="/report/list/{{$clientId}}">< Back to Reports</a>
                    <client-component 
                        @setclient="getClient" client={{$clientId}} 
                        @setperiod="getPeriod" period="{{$periodId}}"
                        ref="ClientComponent">
                    </client-component>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <h3>{{ $period->month }} {{ $period->year }} <span>({{ $period->start }} - {{ $period->end }})</span></h3>
                </div>
            </div>
        </div>
    </div>
        {{-- /Header --}}
        {{-- Content --}}
        <div class="container dashboard">  
            <div class="row"> 
                <div class="col-md-4"> 
                    @include('dashboard.report') 
                    @include('dashboard.server') 
                    @include('dashboard.security') 
                </div> 
                <div class="col-md-4"> 
                    @include('dashboard.code') 
                    @include('dashboard.issue') 
                </div> 
                <div class="col-md-4"> 
                    @include('dashboard.google')       
                </div> 
            </div>
        </div> 
        {{-- /Content --}}
    {{-- /Vue App --}}

    @include('report.footer') 

@endsection



@section('scripts')
    <script src="{{ asset('js/vueDashboard.js') }}"></script>
    <script src="{{ asset('js/vueStatus.js') }}"></script> 
    <script src="{{ asset('js/vueServer.js') }}"></script> 
    <script src="{{ asset('js/vueSecurity.js') }}"></script> 
    <script src="{{ asset('js/vueCode.js') }}"></script> 
    <script src="{{ asset('js/vueIssue.js') }}"></script> 
    <script src="{{ asset('js/vueComment.js') }}"></script> 
    <script src="{{ asset('js/vueGoogle.js') }}"></script> 
@endsection