<div class="row report-section">
    <div class="col-md-12">
        <div class="row title">
            <div class="col-xs-12">
                <h1 class="status">Issues Needing Attention/Fixing</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="form-group">
                    <a href="#" class="button-create" @click.prevent="addIssue">New Issue</a>
                </div>
            </div>
        </div>
        <div v-if="edit_form.issue">
            <div class="row" v-for="(issue, index) in edit_form.issue">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Issue</label>
                        <input type="text" :name='"issue." + index + ".name"' class="form-control" v-model="edit_form.issue[index].name">
                        <span class="help is-danger" v-if="edit_form.errors.has('issue.'+index+ '.name')" v-text="edit_form.errors.get('issue.'+index+ '.name')"></span>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-xs-6 col-sm-3">
                            <div class="form-group">
                                <label>Number</label>
                                <input type="text" :name='"issue." + index + ".fix_number"' class="form-control" v-model="edit_form.issue[index].fix_number">
                                <span class="help is-danger" v-if="edit_form.errors.has('issue.'+index+ '.fix_number')" v-text="edit_form.errors.get('issue.'+index+ '.fix_number')"></span>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-3">
                            <div class="form-group">
                                <label>Hours</label>
                                <input type="text" :name='"issue." + index + ".fix_time"' class="form-control" v-model="edit_form.issue[index].fix_time">
                                <span class="help is-danger" v-if="edit_form.errors.has('issue.'+index+ '.fix_time')" v-text="edit_form.errors.get('issue.'+index+ '.fix_time')"></span>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-3">
                            <div class="form-group">
                                <label>Actions</label>
                                <a v-if="period_id" href="#" class="button-add" @click.prevent="fixIssue(edit_form.issue[index].id, index)">Fixed</a>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-3">
                            <div class="form-group">
                                <label>&nbsp;</label>
                                <a href="#" class="button-remove" @click.prevent="removeIssue(index)">Remove</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
