@extends('layouts.report') 
 
@section('content') 
<div class="container dashboard"> 
    <div class="white-box"> 
      <div class="row"> 
          <div class="col-md-4"> 
              @include('dashboard.report') 
              @include('dashboard.server') 
              @include('dashboard.security') 
          </div> 
          <div class="col-md-4"> 
              @include('dashboard.code') 
              {{--  Issue and Comment --}} 
              @include('dashboard.issue') 
          </div> 
          <div class="col-md-4"> 
              @include('dashboard.google')       
          </div> 
      </div> 
    </div> 
</div> 
@endsection 
 
@section('scripts') 
    <script src="{{ asset('js/vueStatus.js') }}"></script> 
    <script src="{{ asset('js/vueServer.js') }}"></script> 
    <script src="{{ asset('js/vueSecurity.js') }}"></script> 
    <script src="{{ asset('js/vueCode.js') }}"></script> 
    <script src="{{ asset('js/vueIssue.js') }}"></script> 
    <script src="{{ asset('js/vueComment.js') }}"></script> 
    <script src="{{ asset('js/vueGoogle.js') }}"></script> 
@endsection