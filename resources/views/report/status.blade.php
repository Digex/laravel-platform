<div class="row report-section">
    <div class="col-md-12">
        <div class="row title">
            <div class="col-xs-12">
                <h1 class="status">WordPress Status</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6">
                <div class="form-group">
                    <label>Version:</label>
                    <input type="text"  class="form-control" name="core"  v-model="edit_form.core">
                    <span class="help is-danger" v-if="edit_form.errors.has('core')" v-text="edit_form.errors.get('core')"></span>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    <label>Last update:</label>
                    <date-picker name="last_update" v-model="edit_form.last_update" :monday-first="true" :use-utc='true' @selected="clearError('last_update')"></date-picker>
                    <span class="help is-danger" v-if="edit_form.errors.has('last_update')" v-text="edit_form.errors.get('last_update')"></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="form-group">
                    <label>Plugins:</label>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" style="width: 100%;">
                        <tr>
                            <td class="responsive" align="left" valign="top">
                                <select class="form-control" type="text" name="plugin" v-on:change="updatePluginName" v-model="plugin_selected" >
                                    <option v-for="(select_plugin, index) in select_plugins" :value="select_plugin.id">
                                        @{{select_plugin.name}}
                                    </option>
                                </select>        
                            </td>
                            <td class="responsive p-t-15" align="left" valign="top" width="230" style="">
                                <table border="0" cellpadding="0" cellspacing="0" width="230" align="center" style="width: 230px;">
                                    <tr>
                                        <td align="center" style="">
                                            <a href="#" class="button-add" @click.prevent="addPlugin">Add</a>
                                        </td>
                                        <td align="center" width="100" style="">
                                            <a href="#" class="button-create" data-toggle="modal" data-target="#pluginModal">Create</a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="form-group">
                    <table class="table-striped" border="0" cellpadding="0" cellspacing="0" width="100%" align="center" style="width: 100%;">
                        <tbody>
                            <tr v-for="(plugin, index) in user_plugins">
                                <td class="responsive plugin-name" align="left">
                                    @{{plugin.name}}
                                </td>
                                <td class="responsive plugin-options" align="left" valign="top" width="445" >
                                    <table border="0" cellpadding="0" cellspacing="0" width="445" align="center" style="width: 445px;">
                                        <tbody>
                                            <tr>
                                                <td class="plugin-version" align="center">
                                                    <input type="text" :name='"plugins." + index + ".version"' class="form-control" v-model="edit_form.plugins[index].version">
                                                    
                                                    <span class="help is-danger" v-if="edit_form.errors.has('plugins.'+index+ '.version')" v-text="edit_form.errors.get('plugins.'+index+ '.version')"></span>
                                                </td>
                                                <td class="plugin-version" align="center">
                                                    <select  :name='"plugins." + index + ".active"' class="form-control" v-model="edit_form.plugins[index].active">
                                                      <option value="1" selected="selected">Active</option>
                                                      <option value="0">Inactive</option>
                                                    </select>
                                                    
                                                    <span class="help is-danger" v-if="edit_form.errors.has('plugins.'+index+ '.version')" v-text="edit_form.errors.get('plugins.'+index+ '.version')"></span>
                                                </td>

                                                <td align="center" width="100" style="">
                                                    <a href="#" class="button-remove" @click.prevent="removePlugin(index)">Remove</a> 
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <span class="help is-danger" v-if="edit_form.errors.has('plugins')" v-text="edit_form.errors.get('plugins')"></span>
            </div>
        </div>
    </div>
</div>