<div class="row report-section">
    <div class="col-md-12">
        <div class="row title">
            <div class="col-xs-12">
                <h1 class="status">Website Server Metrics</h1>
            </div>
        </div>
         <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Last Cloud Backup:</label>
                    <date-picker name="backup" v-model="edit_form.backup" :monday-first="true" :use-utc='true' @selected="clearError('backup')"></date-picker>
                    <span class="help is-danger" v-if="edit_form.errors.has('backup')" v-text="edit_form.errors.get('backup')"></span>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Website Host Server:</label>
                    <input type="text" name="host" class="form-control" v-model="edit_form.host">
                    <span class="help is-danger" v-if="edit_form.errors.has('host')" v-text="edit_form.errors.get('host')"></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Page Load Time:</label>
                    <input name="load_time" class="form-control" v-model="edit_form.load_time" type="number" step="any">
                    <span class="help is-danger" v-if="edit_form.errors.has('load_time')" v-text="edit_form.errors.get('load_time')"></span>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Content Delivery Network:</label>
                    <select class="form-control" name="cdn" v-model="edit_form.cdn" @change="edit_form.errors.clear($event.target.name)">
                        <option value="0">No</option>
                        <option value="1">Yes</option>
                    </select>
                    <span class="help is-danger" v-if="edit_form.errors.has('cdn')" v-text="edit_form.errors.get('cdn')"></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Media Items:</label>
                    <input name="media" class="form-control" v-model="edit_form.media" type="number">
                    <span class="help is-danger" v-if="edit_form.errors.has('media')" v-text="edit_form.errors.get('media')"></span>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Storage Volume, All Files:</label>
                    <input type="text" name="files" class="form-control" v-model="edit_form.files">
                    <span class="help is-danger" v-if="edit_form.errors.has('files')" v-text="edit_form.errors.get('files')"></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Uptime:</label>
                    <input name="uptime" class="form-control" v-model="edit_form.uptime" type="number" step="any">
                    <span class="help is-danger" v-if="edit_form.errors.has('uptime')" v-text="edit_form.errors.get('uptime')"></span>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Site Last Down:</label>
                    <date-picker name="last_down" v-model="edit_form.last_down" :monday-first="true" :use-utc='true' @selected="clearError('last_down')"></date-picker>
                    <span class="help is-danger" v-if="edit_form.errors.has('last_down')" v-text="edit_form.errors.get('last_down')"></span>
                </div>
            </div>
        </div>
    </div>
</div>