<div v-if="client.client.google_view_id" class="row report-section">
    <div class="col-md-12">
        <div class="row title">
            <div class="col-xs-12">
                <h1 class="status">Google Analytics</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="form-group">
                    <a v-show="buttonAnalytics" href="" class="button-create" @click.prevent="getAnalytics">Get Info</a>
                    <p v-show="!buttonAnalytics">Getting information form Google Analytics ...</p>
                </div>
            </div>
        </div>
        <div v-if="metrics">
            <div class="row">
                <div class="col-xs-6 col-md-3">
                    <p><b>New Visitors:</b> @{{ metrics.new_visitors }}%</p>
                    <p><b>Returning Visitors:</b> @{{ metrics.returning_visitors }}%</p>
                </div>
                <div class="col-xs-6 col-md-3">
                    <p><b>Top Trafifc Days</b></p>
                    <p><b>1.</b> @{{ metrics.day_1 }}</p>
                    <p><b>2.</b> @{{ metrics.day_2 }}</p>
                    <p><b>3.</b> @{{ metrics.day_3 }}</p>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="row">
                        <div class="col-xs-6 col-sm-4">
                            <p><b>Unique Visits</b></p>
                            <p>@{{ metrics.unique }}</p>
                            <p>
                                <div v-if="metrics.unique_percentage < 0" class="text-danger">! @{{ metrics.unique_percentage * -1 }}%</div>
                                <div v-else class="text-success">¡ @{{ metrics.unique_percentage }}%</div>
                            </p>
                        </div>
                        <div class="col-xs-6 col-sm-4">
                            <p><b>Total Visits</b></p>
                            <p>@{{ metrics.total }}</p>
                            <p>
                                <div v-if="metrics.total_percentage < 0" class="text-danger">! @{{ metrics.total_percentage * -1 }}%</div>
                                <div v-else class="text-success">¡ @{{ metrics.total_percentage }}%</div>
                            </p>
                        </div>
                        <div class="col-xs-6 col-sm-4">
                            <p><b>Coversion Rate</b></p>
                            <p>@{{ metrics.conversion }}</p>
                            <p>
                                <div v-if="metrics.conversion_percentage < 0" class="text-danger">! @{{ metrics.conversion_percentage * -1 }}%</div>
                                <div v-else class="text-success">¡ @{{ metrics.conversion_percentage }}%</div>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <p><b>Average Number of Pages:</b> @{{ metrics.avg_pages }}</p>
                </div>
                <div class="col-xs-6">
                    <p><b>Average Time per Visit:</b> @{{ metrics.avg_time }}</p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-8">
                    <p><b>Page Link</b></p>
                </div>
                <div class="col-xs-4">
                    <p><b>Page Views</b></p>
                </div>
                <div class="col-xs-8">
                    <p>@{{ traffic.page_1_link }}</p>
                </div>
                <div class="col-xs-4">
                    <p>@{{ traffic.page_1_link_views }}</p>
                </div>
                <div class="col-xs-8">
                    <p>@{{ traffic.page_2_link }}</p>
                </div>
                <div class="col-xs-4">
                    <p>@{{ traffic.page_2_link_views }}</p>
                </div>
                <div class="col-xs-8">
                    <p>@{{ traffic.page_3_link }}</p>
                </div>
                <div class="col-xs-4">
                    <p>@{{ traffic.page_3_link_views }}</p>
                </div>
                <div class="col-xs-8">
                    <p>@{{ traffic.page_4_link }}</p>
                </div>
                <div class="col-xs-4">
                    <p>@{{ traffic.page_4_link_views }}</p>
                </div>
                <div class="col-xs-8">
                    <p>@{{ traffic.page_5_link }}</p>
                </div>
                <div class="col-xs-4">
                    <p>@{{ traffic.page_5_link_views }}</p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <p><b>Operating System Used</b></p>
                </div>
                <div class="col-xs-3"><img class="icon-img" src="{{ asset('img/dashboard/os/icon-android.svg') }}"></div>
                <div class="col-xs-3"><img class="icon-img" src="{{ asset('img/dashboard/os/icon-windows.svg') }}"></div>
                <div class="col-xs-3"><img class="icon-img" src="{{ asset('img/dashboard/os/icon-apple.svg') }}"></div>
                <div class="col-xs-3"><img class="icon-img" src="{{ asset('img/dashboard/os/icon-linux.svg') }}"></div>
                <div class="col-xs-3">@{{ traffic.android }}%</div>
                <div class="col-xs-3">@{{ traffic.windows }}%</div>
                <div class="col-xs-3">@{{ traffic.mac + traffic.ios }}%</div>
                <div class="col-xs-3">@{{ traffic.linux }}%</div>
                <div class="col-xs-12">
                    <p><b>Browser Used</b></p>
                </div>
                <div class="col-xs-3"><img class="icon-img" src="{{ asset('img/dashboard/browsers/icon-chrome.svg') }}"></div>
                <div class="col-xs-3"><img class="icon-img" src="{{ asset('img/dashboard/browsers/icon-firefox.svg') }}"></div>
                <div class="col-xs-3"><img class="icon-img" src="{{ asset('img/dashboard/browsers/icon-edge.svg') }}"></div>
                <div class="col-xs-3"><img class="icon-img" src="{{ asset('img/dashboard/browsers/icon-safari.svg') }}"></div>
                <div class="col-xs-3">@{{traffic.chrome}}%</div>
                <div class="col-xs-3">@{{traffic.firefox}}%</div>
                <div class="col-xs-3">@{{traffic.edge}}%</div>
                <div class="col-xs-3">@{{traffic.safari}}%</div>
                <div class="col-xs-12">
                    <p><b>Device Used</b></p>
                </div>
                <div class="col-xs-4"><img class="icon-img" src="{{asset('img/dashboard/devices/icon-desktop.svg')}}"></div>
                <div class="col-xs-4"><img class="icon-img" src="{{ asset('img/dashboard/devices/icon-tablet.svg') }}"></div>
                <div class="col-xs-4"><img class="icon-img" src="{{ asset('img/dashboard/devices/icon-phone.svg') }}"></div>
                <div class="col-xs-4">@{{traffic.desktop}}%</div>
                <div class="col-xs-4">@{{traffic.tablet}}%</div>
                <div class="col-xs-4">@{{traffic.mobile}}%</div>
            </div>
        </div>
    </div>
</div>  