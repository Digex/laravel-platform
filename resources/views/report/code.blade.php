<div class="row report-section">
    <div class="col-md-12">
        <div class="row title">
            <div class="col-xs-12">
                <h1 class="status">Website Code Status</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label>HTTP/HTTPS page conflicts:</label>
                    <select name="conflicts" class="form-control" v-model="edit_form.conflicts">
                      <option value="0">No</option>
                      <option value="1">Yes</option>
                    </select>
                    <span class="help is-danger" v-if="edit_form.errors.has('conflicts')" v-text="edit_form.errors.get('conflicts')"></span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Broken Links:</label>
                    <input name="broken_links" class="form-control" v-model="edit_form.broken_links" type="number">
                    <span class="help is-danger" v-if="edit_form.errors.has('broken_links')" v-text="edit_form.errors.get('broken_links')"></span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label >Mobile Friendly:</label>
                    <select class="form-control" name="mobile_friendly" v-model="edit_form.mobile_friendly" @change="edit_form.errors.clear($event.target.name)">
                        <option value="1">Yes</option>
                        <option value="0">No</option>
                    </select>
                    <span class="help is-danger" v-if="edit_form.errors.has('mobile_friendly')" v-text="edit_form.errors.get('mobile_friendly')"></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label>Images without alt text: </label>
                    <input name="alt_text" class="form-control" v-model="edit_form.alt_text" type="number">
                    <span class="help is-danger" v-if="edit_form.errors.has('alt_text')" v-text="edit_form.errors.get('alt_text')"></span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Pages without title tags:</label>
                    <input name="title_tags" class="form-control" v-model="edit_form.title_tags" type="number">
                    <span class="help is-danger" v-if="edit_form.errors.has('title_tags')" v-text="edit_form.errors.get('title_tags')"></span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Missing meta description:</label>
                    <input name="meta_description" class="form-control" v-model="edit_form.meta_description" type="number">
                    <span class="help is-danger" v-if="edit_form.errors.has('meta_description')" v-text="edit_form.errors.get('meta_description')"></span>
                </div>
            </div>
        </div>
    </div>
</div>