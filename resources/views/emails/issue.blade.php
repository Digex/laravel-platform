<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Client Name</title>
    <style type="text/css">
        /* ======================================= DESKTOP STYLES */
        * { -webkit-text-size-adjust: none; }
        body { margin: 0 !important; padding: 0 !important; }
        body,table,td,p,a { -ms-text-size-adjust: 100% !important; -webkit-text-size-adjust: 100% !important; }
        table, td { border-spacing: 0 !important; mso-table-lspace: 0px !important; mso-table-rspace: 0pt !important; border-collapse: collapse !important; }
        .ExternalClass * { line-height: 100% }
        .mobile-link a, .mobile-link span { text-decoration:none !important; color: inherit !important; border: none !important; }
        /* ======================================= CUSTOM DESKTOP STYLES */
    
        /* ======================================= MOBILE STYLES */
        @media only screen and (max-width: 600px) {       
        body { min-width: 320px; margin: 0; }
        .hide-m { display: none !important; }
        .show-for-small { display: block !important; overflow: visible !important; width: auto !important; max-height: inherit !important; }
        .no-float { float: none !important; }
        .block { display: block !important; }
        .resize-image { width: 100%; height: auto; }
        .center-image { display: block; margin: 0 auto; }
    
        .text-center { text-align: center !important; }
        .font-14 { font-size: 14px !important; line-height: 16px !important; }
        .font-16 { font-size: 16px !important; line-height: 18px !important; }
        .font-20 { font-size: 20px !important; line-height: 22px !important; }
        .font-22 { font-size: 22px !important; line-height: 24px !important; }
    
        .pad-t-0 { padding-top: 0px !important; }
        .pad-r-0 { padding-right: 0px !important; }
        .pad-b-0 { padding-bottom: 0px !important; }
        .pad-l-0 { padding-left: 0px !important; }
        .pad-t-20 { padding-top: 20px !important; }
        .pad-r-20 { padding-right: 20px !important; }
        .pad-b-20 { padding-bottom: 20px !important; }
        .pad-l-20 { padding-left: 20px !important; }
        .pad-0 { padding-top: 0px !important; padding-right: 0px !important; padding-bottom: 0px !important; padding-left: 0px !important; }
        .pad-10 { padding-top: 10px !important; padding-right: 10px !important; padding-bottom: 10px !important; padding-left: 10px !important; }
        .pad-20 { padding-top: 20px !important; padding-right: 20px !important; padding-bottom: 20px !important; padding-left: 20px !important; }
        .pad-sides-0 { padding-right: 0px !important; padding-left: 0px !important; }
        .pad-sides-10 { padding-right: 10px !important; padding-left: 10px !important; }
        .pad-sides-20 { padding-right: 20px !important; padding-left: 20px !important; }
    
        .w100 { width: 100% !important; min-width: initial !important; }
        .w90 { width: 90% !important; min-width: initial !important; }
        .w50 { width: 50% !important; min-width: initial !important; }
        /* ======================================= CUSTOM MOBILE STYLES */
        .border-corners { border-radius: 5px !important; }
        .bg-blue { background-color: #1a6980 !important; }
    
        }
      </style>

</head>

<body>

    <!-- WRAPPER -->
    <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" bgcolor="#c9d0de" style="width: 100%; background-color: #c9d0de;">
        <tbody>
            <tr>
                <td class="pad-20" align="center" style="padding-top: 80px; padding-bottom: 80px;">
                    <table class="w100" border="0" cellpadding="0" cellspacing="0" width="500" align="center" bgcolor="#ffffff" style="width: 500px; margin: 0 auto; background-color: #ffffff;">
                        <tbody>
                            <tr>
                                <td class="pad-20 font-22" align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 28px; line-height: 30px; font-weight: bold; color: #1a6980; padding-top: 25px; padding-bottom: 65px;">
                                    MySiteDashboard
                                </td>
                            </tr>
                            <tr>
                                <td class="pad-sides-20 font-22" align="center" style="font-family: Helvetica, Arial, sans-serif; font-size: 24px; line-height: 26px; font-weight: bold; color: #7a5904; padding-bottom: 20px;">
                                New Issue Status
                                </td>
                              </tr>
                            <tr>
                               <td class="pad-sides-20" align="left" style="padding-left: 40px; padding-right: 40px; padding-bottom: 40px;">
                                  <table class="w100" cellspacing="0" cellpadding="0" border="0" align="left" width="350" style="width:350px;">
                                     <tbody>
                                        <tr>
                                           <td class="w100 block" align="left" width="17%" style="width: 17%; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 22px; font-weight: bold; color: #6e7688;" valign="top">
                                              Client:
                                           </td>
                                           <td class="w100 block pad-l-0" align="left" width="83%" style="width: 83%; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 22px; font-weight: normal; color: #6e7688; padding-bottom: 10px; padding-left: 10px;">
                                               {{ $data['client'] }}
                                           </td>
                                        </tr>
                                        <tr>
                                            <td class="w100 block" align="left" width="17%" style="width: 17%; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 22px; font-weight: bold; color: #6e7688;" valign="top">
                                               Date:
                                            </td>
                                            <td class="mobile-link w100 block pad-l-0" align="left" width="83%" style="width: 83%; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 22px; font-weight: normal; color: #6e7688; padding-bottom: 10px; padding-left: 10px;">
                                                {{ $data['date'] }}
                                            </td>
                                         </tr>
                                         <tr>
                                            <td class="w100 block" align="left" width="17%" style="width: 17%; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 22px; font-weight: bold; color: #6e7688;" valign="top">
                                               Status:
                                            </td>
                                            @if( $data['status'] == 'fix')
                                            <td class="mobile-link w100 block pad-l-0 text-center" align="left" width="83%" style="width: 83%; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 22px; font-weight: normal; color: #e32b2b; padding-bottom: 10px; padding-left: 10px;">
                                                YES, FIX MY ISSUES
                                            </td>
                                         </tr>
                                         <tr>
                                            <td class="w100 block" align="left" width="17%" style="width: 17%; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 22px; font-weight: bold; color: #6e7688;" valign="top">
                                               Issues:
                                            </td>
                                            <td class="mobile-link w100 block pad-l-0 text-center" align="left" width="83%" style="width: 83%;">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" style="width: 100%;">
                                                    <tbody>
                                                        @foreach ($data['issues'] as $issue)
                                                            <tr>
                                                                <td align="left" valign="top" style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 22px; font-weight: normal; color: #6e7688; padding-bottom: 10px;">
                                                                    {{ $issue->name }}
                                                                </td>
                                                                <td align="left" width="90" valign="top" style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 22px; font-weight: normal; color: #6e7688; padding-bottom: 10px;">
                                                                    {{ $issue->fix_time }} hrs
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </td>
                                            @endif
                                            @if( $data['status'] == 'ignore')
                                            <td class="mobile-link w100 block pad-l-0 text-center" align="left" width="83%" style="width: 83%; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 22px; font-weight: normal; color: #7a5904; background-color: #fff1c6; border:1px solid #ffedba; padding-left: 10px;">
                                                IGNORE SITE STATUS
                                            </td>
                                            @endif
                                         </tr>
                                     </tbody>
                                  </table>
                               </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <table  border="0" cellpadding="0" cellspacing="0" width="100%" align="center" bgcolor="#e0e5f0" style="width: 100%; background-color: #e0e5f0;">
                                        <tbody>
                                            <tr>
                                                <td class="pad-sides-20" align="center" style="font-family: Helvetica, Arial, sans-serif; font-size: 12px; line-height: 14px; font-weight: normal; color: #878fa0; padding-top: 20px; padding-bottom: 20px;">
                                                    &copy; 2018 MySiteDashboard. All rights reserved.
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <!-- /WRAPPER -->

</body>

</html>