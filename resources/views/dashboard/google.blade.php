<div id="appGoogleAnalytics">
    <h1 class="bg traffic">Website Traffic</h1>
    
    <transition name="fade">
      <div v-if="authorize" v-cloak>
          
          <div v-if="metrics">
            <div class="container-section no-padding-top no-margin-bottom">
              <div class="row">
                <div class="col-sm-6 col-sm-push-6 ipad-100">
                  <div class="row">
                    <div class="col-sm-12">
                      <h2 class="no-margin-bottom">Top Traffic Days:</h2>  
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-7 col-sm-12">
                      <div class="img-container left">
                        <img class="calendar-img" src="{{asset('img/dashboard/misc/icon-calendar.svg')}}">
                        <p class="calendar text-center">
                          <span>@{{metrics.month_top}}</span>
                          <br>
                          <span class="font-24">@{{metrics.day_top}}</span>
                        </p>
                       </div> 
                    </div>
                    <div class="col-xs-12 col-sm-12 days">
                      <p><span>1</span> @{{metrics.day_1}}</p>
                      <p><span>2</span> @{{metrics.day_2}}</p>
                      <p><span>3</span> @{{metrics.day_3}}</p>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6 col-sm-pull-6 ipad-100 visitors">
                  <br>
                    <pie-chart :labels="['Returning','New']" :values="[metrics.returning_visitors, metrics.new_visitors]" :colors="['#008f71','#136981']"></pie-chart>
                    <p class="returning"> <span class="bullet green"></span>&nbsp;Returning site visitors</p>
                    <p class="new"> <span class="bullet blue"></span>&nbsp;New site visitors</p>
                </div>
              </div>
            </div>
            
            <div class="divider"></div>
            <div class="container-section  no-margin-bottom visits">
              <div class="row">
                <div class="col-sm-4 text-center ipad-100 ">
                  <label>Unique <br class="visible-lg-block"> Site Visits </label>
                  <p class="font-24">@{{metrics.unique}}</p>
                  <span class="circle success" v-if="metrics.unique_percentage >= 0 "> &#8593; @{{metrics.unique_percentage}}%</span>
                  <span class="circle is-danger" v-else> &#8595; @{{metrics.unique_percentage * -1}}%</span>
                </div>
                <div class="col-sm-4 text-center ipad-100 ">
                  <label>Total Site <br class="visible-lg-block">Visits </label>
                  <p class="font-24">@{{metrics.total}}</p>
                  <span class="circle success" v-if="metrics.total_percentage >= 0 "> &#8593; @{{metrics.total_percentage}}%</span>
                  <span class="circle is-danger" v-else> &#8595; @{{metrics.total_percentage * -1}}%</span>
                </div>
                <div v-if="metrics.conversion_percentage > 0" class="col-sm-4 text-center ipad-100">
                  <label>Conversion <br class="visible-lg-block"> Rate </label>
                  <p class="font-24">@{{metrics.conversion}}%</p>
                  <span class="circle success" v-if="metrics.conversion_percentage >= 0 "> &#8593; @{{metrics.conversion_percentage}}%</span>
                  <span class="circle is-danger" v-else> &#8595; @{{metrics.conversion_percentage * -1}}%</span>
                </div>
              </div>
            </div>
            
            <div class="divider"></div>
            <div class="container-section  no-margin-bottom averages">
              <div class="row text-center">
                <div class="col-sm-6 ipad-100">
                  <label>Average Number of Pages Viewed:</label>
                  <div class="img-container">
                    <img class="laptop-img" src="{{asset('img/dashboard/misc/icon-laptop.svg')}}">
                    <p class="laptop">@{{metrics.avg_pages.toFixed(2)}}</p>
                  </div>
                </div>
                <div class="col-sm-6 ipad-100">
                  <label>Average Time per User Visit:</label>
                  <div class="img-container">
                    <img class="clock-img" src="{{asset('img/dashboard/misc/icon-clock.svg')}}">
                    <p class="clock">@{{metrics.avg_time.substring(0,5)}}</p>
                    <label>minutes : seconds</label>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="divider"></div>
            <div class="container-section  no-margin-bottom">
              <h2>Most Popular Pages:</h2>
              
              <div class="row">
                <div class="col-sm-8">
                  <label>Page Links</label>
                </div>
                <div class="col-sm-4 text-right">
                  <label>Page Views</label>
                </div>
              </div>  
              <div class="table">

                <div class="row">
                  <div class="col-xs-9 ">
                    1. <a :href="traffic.page_1_url">@{{traffic.page_1_link}}</a>
                  </div>
                  <div class="col-xs-3 text-right"><p>@{{traffic.page_1_link_views}}</p></div>
                </div>

                <div class="row">
                  <div class="col-xs-9 ">
                    2. <a :href="client.site + traffic.page_2_link">@{{traffic.page_2_link}}</a>
                  </div>
                  <div class="col-xs-3 text-right"><p>@{{traffic.page_2_link_views}}</p></div>
                </div>

                <div class="row">
                  <div class="col-xs-9 ">
                    3. <a :href="client.site + traffic.page_3_link">@{{traffic.page_3_link}}</a>
                  </div>
                  <div class="col-xs-3 text-right"><p>@{{traffic.page_3_link_views}}</p></div>
                </div>

                <div class="row">
                  <div class="col-xs-9 ">
                    4. <a :href="client.site + traffic.page_4_link">@{{traffic.page_4_link}}</a>
                  </div>
                  <div class="col-xs-3 text-right"><p>@{{traffic.page_4_link_views}}</p></div>
                </div>

                <div class="row">
                  <div class="col-xs-9 col-md-9">
                    5. <a :href="client.site + traffic.page_5_link">@{{traffic.page_5_link}}</a>
                  </div>
                  <div class="col-xs-3 col-md-3 text-right"><p>@{{traffic.page_5_link_views}}</p></div>
                </div>

              </div>
            </div>
            
            <div class="divider"></div>
            <div class="container-section  no-margin-bottom">
              <p>
                <b>
                  What Your Website Visitors Are Using:  
                </b>
              </p>


              <div class="row os">
                <div class="col-sm-12">
                  <label>
                    Operating System Used:
                  </label>
                </div>

                <div class="col-xs-3">
                  <div class="img-container">
                    <img class="os-img" src="{{ asset('img/dashboard/os/icon-android.svg') }}">
                    <p>@{{traffic.android.toFixed(2)}}%</p>
                  </div>
                </div>

                <div class="col-xs-3">
                  <div class="img-container">
                    <img class="os-img" src="{{ asset('img/dashboard/os/icon-windows.svg') }}">
                    <p>@{{traffic.windows.toFixed(2)}}%</p>
                  </div>
                </div>

                <div class="col-xs-3">
                  <div class="img-container">
                    <img class="os-img" src="{{ asset('img/dashboard/os/icon-apple.svg') }}">
                    <p>@{{(traffic.mac + traffic.ios).toFixed(2)}}%</p>
                  </div>
                </div>

                <div class="col-xs-3">
                  <div class="img-container">
                    <img class="os-img" src="{{ asset('img/dashboard/os/icon-linux.svg') }}">
                    <p>@{{traffic.linux.toFixed(2)}}%</p>
                  </div>
                </div>

              </div>

              <div class="row browser">
                <div class="col-sm-12">
                  <label>
                    Browser Used:
                  </label>
                </div>
                
                <div class="col-xs-3">
                  <div class="img-container">
                    <img class="browser-img" src="{{ asset('img/dashboard/browsers/icon-chrome.svg') }}">
                    <p>@{{traffic.chrome.toFixed(2)}}%</p>
                  </div>
                </div>

                <div class="col-xs-3">
                  <div class="img-container">
                    <img class="browser-img" src="{{ asset('img/dashboard/browsers/icon-edge.svg') }}">
                    <p>@{{traffic.edge.toFixed(2)}}%</p>
                  </div>
                </div>

                <div class="col-xs-3">
                  <div class="img-container">
                    <img class="browser-img" src="{{ asset('img/dashboard/browsers/icon-firefox.svg') }}">
                    <p>@{{traffic.firefox.toFixed(2)}}%</p>
                  </div>
                </div>

                <div class="col-xs-3">
                  <div class="img-container">
                    <img class="browser-img" src="{{ asset('img/dashboard/browsers/icon-safari.svg') }}">
                    <p>@{{traffic.safari.toFixed(2)}}%</p>
                  </div>
                </div>
                
              </div>

              <div class="row device">
                <div class="col-sm-12">
                  <label>
                    Device Used:
                  </label>
                </div>
                
                <div class="col-xs-3">
                  <div class="img-container">
                    <img class="device-img" class="laptop-img" src="{{asset('img/dashboard/devices/icon-computer-2.svg')}}">
                    <p>@{{traffic.desktop.toFixed(2)}}%</p>
                  </div>
                </div>

                <div class="col-xs-3">
                  <div class="img-container">
                     <img class="device-img tablet" src="{{ asset('img/dashboard/devices/icon-tablet-2.svg') }}">
                    <p>@{{traffic.tablet.toFixed(2)}}%</p>
                  </div>
                </div>

                <div class="col-xs-3">
                  <div class="img-container">
                     <img class="device-img mobile" src="{{ asset('img/dashboard/devices/icon-phone-2.svg') }}">
                    <p>@{{traffic.mobile.toFixed(2)}}%</p>
                  </div>
                </div>

                
              </div>

            </div>
          </div>

          <div v-else>
            <div class="container-section">
                <div class="row">
                    <div class="col-sm-12">
                        <p class="text-center google-text">In order to be able to display important information related to your website traffic, we need your authorization to insert an additional Google Tag Manager code on your website. This code will not interfere with any tracking that is currently in place.</p>
                    </div>
                    <div class="col-sm-12">
                        <a href="#" @click.prevent="authorizeGtm" class="button-aprove">Authorize</a>
                    </div>
                </div>
            </div>
          </div>

      </div>
    </transition>
</div>