<div id="appServer">
    

    <h1 class="bg metrics">Website Server Metrics</h1>
    <div v-if="dashboard_server" v-cloak>
        <div class="container-section no-margin-bottom">
          <div class="row">
            <div class="col-xs-5">
              <label>Last cloud backup: </label>
              <p class="success">@{{dashboard_server.backup}}</p>
            </div>
            <div class="col-xs-7">
              <label>Website Host Server: </label>
              <p>@{{dashboard_server.host}}</p>
            </div>
          </div>
        </div>    
        
        <div class="divider"></div>

        <div class="container-section no-margin-bottom">
          <div class="row">
            <div class="col-xs-5">
              <label>Page Load Time: </label>
              <p v-bind:class="{ conflict: dashboard_server.load_time > 10,  success: dashboard_server.load_time < 10}">@{{dashboard_server.load_time.toFixed(2)}} SEC</p>
            </div>
            <div class="col-xs-7">
              <label>Content Delivery Network:</label>
              <p>
                <span class="bold" v-if="dashboard_server.content == 1">Yes</span>
                <span class="bold" v-else>No</span>
              </p>
            </div>
          </div>
        </div>    
        
        <div class="divider"></div>

        <div class="container-section no-margin-bottom">  
          <div class="row">
            <div class="col-xs-5">
              <label>Media Items: </label>
              <p>@{{dashboard_server.items}}</p>
            </div>
            <div class="col-xs-7">
              <label>Storage Volume, All Files:</label>
              <p> @{{dashboard_server.storage}}</p>
            </div>
          </div>  
        </div>  
        
        <div class="divider"></div>

        <div class="container-section">
          <div class="row">
            <div class="col-xs-5">
              <label>Uptime: </label>
              <p>@{{dashboard_server.uptime}}%</p>
            </div>
            <div class="col-xs-7">
              <label>Site Last Down:</label>
              <p class="conflict">@{{dashboard_server.last_down}} </p>
            </div>
          </div> 
        </div>


    </div>  


</div>




