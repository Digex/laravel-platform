<div id="appSecurity">
    
    <h1 class="bg security">Website Security Snapshot</h1>
    
    <div v-if="dashboard_security" v-cloak>
        <div class="container-section no-margin-bottom">
          <div class="row">
            <div class="col-xs-2">
              <img v-if="dashboard_security.https == 1" class="check" src="{{asset('img/dashboard/misc/icon-check.svg')}}">
              <img v-else class="check" src="{{asset('img/dashboard/misc/icon-error.svg')}}">
            </div>
            <div class="col-xs-3 no-padding">
              <label>Site Security:</label>
              <p>
                <span class="bold" v-if="dashboard_security.https == 1">HTTPS</span>
                <span class="bold" v-else>HTTP</span>
              </p>
            </div>
            <div v-if="dashboard_security.certified" class="col-xs-3 no-padding-right">
              <label>Certified by:</label>
              <p>@{{dashboard_security.certified}}</p>
            </div>
            <div v-if="dashboard_security.expiration" class="col-xs-4 no-padding-right">
              <label>Expiration Date: </label>
              <p>@{{dashboard_security.expiration}}</p>
            </div>
          </div>
        </div>  

        <div class="divider"></div>

        <div class="container-section">
          <div class="row attacks">
            <div class="col-xs-2">
              <img class="check" src="{{asset('img/dashboard/misc/icon-check.svg')}}">
            </div>
            <div class="col-xs-9 no-padding-left">
                <div class="text-center">
                <h2 class="no-margin-bottom font-24 gray">@{{dashboard_security.attacks}}</h2> <h2 class="no-margin-bottom font-24 gray">Attacks Blocked</h2>
                <span class="block ">in the Last 30 days </span>
              </div>
              
            </div>
          </div>
          
          <div v-if="dashboard_security.country1_full" class="row flags-container">
            <div class="col-xs-12 text-center">
              <label>Attack Country of Origin/Attacks:</label>
              
              <div v-if="dashboard_security.country1_full" class="table no-margin-bottom">
                <div class="row">
                  <div class="col-xs-2 font-20 text-right">
                    <flag :squared="false" :iso="dashboard_security.country1" />
                  </div>
                  <div class="col-xs-6 no-padding text-left">
                    <label>@{{dashboard_security.country1_full}}</label>
                  </div>
                  <div class="col-xs-4">
                    <label>@{{dashboard_security.country1_number}}</label>
                  </div>
                </div>
              </div>
              
              <div v-if="dashboard_security.country2_full" class="table no-margin-bottom">
                <div class="row">
                  <div class="col-xs-2 font-20 text-right">
                    <flag :squared="false" :iso="dashboard_security.country2" />
                  </div>
                  <div class="col-xs-6 no-padding text-left">
                    <label>@{{dashboard_security.country2_full}}</label>
                  </div>
                  <div class="col-xs-4">
                    <label>@{{dashboard_security.country2_number}}</label>
                  </div>
                </div>
              </div>
              
              <div v-if="dashboard_security.country3_full" class="table no-margin-bottom">
                <div class="row">
                  <div class="col-xs-2 font-20 text-right">
                    <flag :squared="false" :iso="dashboard_security.country3" />
                  </div>
                  <div class="col-xs-6 no-padding text-left">
                    <label>@{{dashboard_security.country3_full}}</label>
                  </div>
                  <div class="col-xs-4">
                    <label>@{{dashboard_security.country3_number}}</label>
                  </div>
                </div>
              </div>
              
              <div v-if="dashboard_security.country4_full" class="table no-margin-bottom">
                <div class="row">
                  <div class="col-xs-2 font-20 text-right">
                    <flag :squared="false" :iso="dashboard_security.country4" />
                  </div>
                  <div class="col-xs-6 no-padding text-left">
                    <label>@{{dashboard_security.country4_full}}</label>
                  </div>
                  <div class="col-xs-4">
                    <label>@{{dashboard_security.country4_number}}</label>
                  </div>
                </div>
              </div>
              
              <div v-if="dashboard_security.country5_full" class="table no-margin-bottom">
                <div class="row">
                  <div class="col-xs-2 font-20 text-right">
                    <flag :squared="false" :iso="dashboard_security.country5" />
                  </div>
                  <div class="col-xs-6 no-padding text-left">
                    <label>@{{dashboard_security.country5_full}}</label>
                  </div>
                  <div class="col-xs-4">
                    <label>@{{dashboard_security.country5_number}}</label>
                  </div>
                </div>
              </div>

            </div>
          </div>




        </div>

    </div>      


  


</div>