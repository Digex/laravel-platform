<div id="appIssue">
    
    <div v-if="dashboard_issues.length > 0" v-cloak>
        <div class="container-section" v-bind:class="{ noPaddingTop: show }">
          <h2 v-if="show == 'show'">Issues Needing Attention/Fixing!</h2>
          <h2 v-if="show == 'fix'">You have chosen to FIX the following issues:</h2>
          <h2 v-if="show == 'ignore'">You have chosen to ignore the following issues:</h2>
          
          <div v-if="" class="table">
            <div class="row" v-for="(issue, index) in dashboard_issues">
              <div class="col-xs-9">
                 <img v-if="show != 'ignore'" class="check-small" src="{{asset('img/dashboard/misc/icon-error.svg')}}">
                <label v-bind:class="{ disabled: show == 'ignore', conflict: show != 'ignore' }">@{{issue.name}}</label>
              </div>
              <div class="col-xs-3">
                <p>@{{issue.fix_number}} </p>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12 no-padding-right">
              <p v-if="show == 'show'" class="font-14">Estimated time needed to FIX all urgent issues: 
                <br>
                <span class="blue">@{{total_hours}}&nbsp;hours</span>
                <br>
                <span>(Standard hourly rates apply)</span>
              </p>

              <p v-if="show == 'fix'">We will notify you when the issues have been resolved.</p>
              <p v-if="show == 'ignore'">Changed your mind?</p>
            </div>
          </div>

          <div class="row">
            <div class="col-xs-12 text-center">
              <a v-if="show != 'fix'" href="#" @click.prevent="ignoreIssues('fix')" class="button-red hidden-print">YES, Fix My Site »</a>
              <a href="#" @click.prevent="showCommentBox" class="button-blue hidden-print">Contact Us&nbsp;&#187;</a>
              <a v-if="show == 'show'" href="#" @click.prevent="ignoreIssues('ignore')" class="button-yellow hidden-print">Ignore Site Issues »</a>

            </div>
          </div>

        </div>
    </div>  
    <issue-modal :message="issueMessage"></issue-modal>
</div>

<div id="appComment">
    <transition name="fade">
        <div class="comments-section" v-if="commentBox" v-cloak>
            <h1 class="bg comments">Comments &amp; Questions?</h1>  
            <div v-cloak>
                <div class="container-section">
                  <form id=comment_form"  method="POST" @submit.prevent="comment_form_submit" @keydown="comment_form.errors.clear($event.target.name)">
                      {{ csrf_field() }}
                  
                      <textarea name="comment" v-model="comment_form.comment" placeholder="Please send any feedback, comments, or questions. You have 500 characters. Thank you!">
                      </textarea>
                      
                      <span class="help is-danger" v-if="comment_form.errors.has('comment')" v-text="comment_form.errors.get('comment')"></span>
                      <button class="button is-primary button-blue-contact" v-bind:disabled="comment_form.errors.any()">Send Message »</button>
                  </form>
                </div> 
            </div>  
        </div>
    </transition>
</div>