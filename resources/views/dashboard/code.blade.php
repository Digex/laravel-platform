<div id="appCode">

    <h1 class="bg code">Website Code Status</h1>
     
    <transition name="fade">
      <div v-if="dashboard_code" v-cloak>
        <div class="container-section no-padding-top no-margin-bottom" >
          
          <div class="row">
            <div class="col-xs-12">
               <h2>Six Critical Code Elements: </h2>
            </div>
          </div>
          
          <div class="table no-margin-bottom">
            <div class="row">
              <div class="col-xs-9 no-padding-right">
                <transition name="fade">
                  <img v-if="dashboard_code.conflicts > 0" class="check-small" src="{{asset('img/dashboard/misc/icon-error.svg')}}">
                  <img v-else class="check-small" src="{{asset('img/dashboard/misc/icon-check.svg')}}">
                  </transition>
                  <label  v-bind:class="{ conflict: dashboard_code.conflicts > 0 }">HTTP/HTTPS page conflicts: </label>

              </div>
              <div class="col-xs-3">
                <p v-if="dashboard_code.conflicts > 0">Yes</p>
                <p v-else>No</p>
              </div>
            </div>

            <div class="row">
              <div class="col-xs-9">
                <transition name="fade">
                  <img v-if="dashboard_code.broken_links > 0" class="check-small" src="{{asset('img/dashboard/misc/icon-error.svg')}}">
                  <img v-else class="check-small" src="{{asset('img/dashboard/misc/icon-check.svg')}}">
                  </transition>
                  <label v-bind:class="{ conflict: dashboard_code.broken_links > 0 }">Broken Links: </label>

              </div>
              <div class="col-xs-3">
                <p>@{{dashboard_code.broken_links}}</p>
              </div>
            </div>

            <div class="row">
              <div class="col-xs-9">
                <transition name="fade">
                  <img v-if="dashboard_code.mobile_friendly != 1 " class="check-small" src="{{asset('img/dashboard/misc/icon-error.svg')}}">
                  <img v-else class="check-small" src="{{asset('img/dashboard/misc/icon-check.svg')}}">
                  </transition>
                  <label  v-bind:class="{ conflict: dashboard_code.mobile_friendly != 1 }">Mobile Friendly: </label>

              </div>
              <div class="col-xs-3">
                <p><span class="bold" v-if="dashboard_code.mobile_friendly == 1">Yes</span><span class="bold" v-else>No</span></p>
              </div>
            </div>

            <div class="row">
              <div class="col-xs-9">
                <transition name="fade">
                  <img v-if="dashboard_code.alt_text > 0" class="check-small" src="{{asset('img/dashboard/misc/icon-warning.svg')}}">
                  <img v-else class="check-small" src="{{asset('img/dashboard/misc/icon-check.svg')}}">
                  </transition>
                  <label>Images without alt text: </label>

              </div>
              <div class="col-xs-3">
               <p>@{{dashboard_code.alt_text_format}} </p>
              </div>
            </div>

            <div class="row">
              <div class="col-xs-9">
                <transition name="fade">
                  <img v-if="dashboard_code.title_tags  > 0" class="check-small" src="{{asset('img/dashboard/misc/icon-error.svg')}}">
                  <img v-else class="check-small" src="{{asset('img/dashboard/misc/icon-check.svg')}}">
                  </transition>
                  <label  v-bind:class="{ conflict: dashboard_code.title_tags  > 0 }">Pages without title tags:</label>

              </div>
              <div class="col-xs-3">
                <p>@{{dashboard_code.title_tags_format}}</p>
              </div>
            </div>

            <div class="row">
              <div class="col-xs-9">
                <transition name="fade">
                  <img v-if="dashboard_code.meta_description > 0" class="check-small" src="{{asset('img/dashboard/misc/icon-warning.svg')}}">
                  <img v-else class="check-small" src="{{asset('img/dashboard/misc/icon-check.svg')}}">
                  </transition>
                  <label>Missing meta description:</label>

              </div>
              <div class="col-xs-3">
                <p>@{{dashboard_code.meta_description_format}}</p>
              </div>
            </div>
          </div>

        </div>
      </div>  
    </transition>

    <div class="divider"></div>

 




</div>