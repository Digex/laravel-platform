<div id="appReport">

      <h1 class="status bg">Update Status</h1>
      
      <transition name="fade">
        <div v-if="dashboard_status" v-cloak>
          <div class="container-section no-padding-top no-margin-bottom">
            <div class="row">
              <div class="col-md-12">
                <h2>WordPress Core:</h2>
              </div>
            </div>

            <div class="row">
              <div class="col-xs-2">
                <img class="check" src="{{asset('img/dashboard/misc/icon-check.svg')}}">
              </div>
              <div class="col-xs-4">
                <label>Version:</label>
                <p>@{{dashboard_status.core}}</p>
              </div>
              <div class="col-xs-6">
                <label>Last update:</label>
                <p class="success">@{{dashboard_status.update}}</p>
              </div>
            </div>
          </div>
          <div class="divider"></div>
          <div class="container-section no-padding-top">  
            <div class="row">
              <div class="col-md-12">
                <h2>WordPress Plugins:</h2> 
                <transition name="fade">
                  <a href="#" class="underline hidden-print" v-if="!dashboard_status_plugins_period.show" @click.prevent="showPlugins" >View List &#9660;</a>
                </transition>
                <transition name="fade">
                  <div v-if="dashboard_status_plugins_period.show">
                    <div class="table" v-for="(plugin, index) in dashboard_status_plugins">
                      <div class="row pluginRow" v-bind:class="{ pluginActive : plugin.active == 1, pluginInactive : plugin.active == 0  }">
                        <div class="col-xs-6">@{{plugin.name}}</div>
                        <div class="col-xs-3">@{{plugin.version}}</div>
                        <div class="col-xs-3">
                          <span v-if="plugin.active == 1"> Active</span> 
                          <span v-else> Inactive</span> 
                        </div>
                      </div>
                    </div>
                  <a href="#" class="underline" @click.prevent="hidePlugins" >Hide List &#9650;</a>
                  </div>
                </transition>
              </div>
            </div>
            
            <div class="row">
              <div class="col-xs-2">
                <img class="check" src="{{asset('img/dashboard/misc/icon-check.svg')}}">
              </div>
              <div class="col-xs-3">
                <label>Installed:</label>
                <p>@{{dashboard_status_plugins_period.installed}}</p>
              </div>
              <div class="col-xs-3">
                <label>Active:</label>
                <p>@{{dashboard_status_plugins_period.active}}</p>
              </div>
              <div class="col-sm-4 no-padding padding-left-mobile">
                <label>Last update:</label>
                <p class="success">@{{dashboard_status.update}}</p>
              </div>
            </div>
          </div>
        </div>
      </transition>

</div>