@extends('layouts.admin')

@section('content')
    {{-- Vue App --}}
    <div id="appReportAgencyList">
        {{-- Header --}}
        <div id="client-header" v-if="clientName">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h3>@{{clientName}}</h3>
                    </div>
                </div>
            </div>
        </div>
        <div id="header" class="container">
            <div class="row">
                <div class="col-xs-12">
                    <a class="button-back" href="/{{$subdomain}}/clients">< Back to Clients</a>
                    <client-component @setclient="getClient" client="{{$clientId}}" @setsubdomain="getSubdomain" subdomain="{{$subdomain}}" ></client-component>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <h3>Reports </h3>
                </div>
            </div>
        </div>
        {{-- /Header --}}
        {{-- Content --}}
        <div id="content" class="container">
            <div class="row">
                <div class="col-sm-12">
                        <period-agency-information
                            v-for="period in periods"
                            :key="period.id"
                            :id="period.id"
                            :month="period.month"
                            :year="period.year"
                            :start="period.start"
                            :end="period.end"
                            subdomain="{{$subdomain}}"
                        ></period-agency-information>
                </div>
            </div>
        </div>
        {{-- /Content --}}
    </div>
    {{-- /Vue App --}}

@endsection

@section('scripts')
    <script src="{{ asset('js/vueReportAgencyList.js') }}"></script>
@endsection