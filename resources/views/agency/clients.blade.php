@extends('layouts.admin')

@section('content')

  <div id="header" class="container">
      <div class="row">
          <div class="col-xs-12">
            <h3>Clients</h3>
          </div>
      </div>
  </div>

<div class="container">
  @foreach ($clients as $client)
    <div class="row client-information">
        <div class="col-md-7 col-lg-7 clearfix">
            <div class="logo pull-left">
                <a href="agency/report/list/{{$client->id}}"><img src="{{ asset('uploads/'. $client->logo ) }}" class="img-responsive"></a>
            </div>
            <div class="name pull-left">
                <a href="agency/report/list/{{$client->id}}"><h4>{{ $client->name }}</h4></a>
                <h5>{{ $client->site }}</h5>
            </div>
        </div>
        <!--<div class="col-xs-5">
            <div class="row">
                <div class="col-xs-6">
                    <a href="agency/report/list/{{$client->id}}">
                        <div class="client-info">
                            View Reports
                        </div>
                    </a>
                </div>
            </div>
        </div>-->
    </div>
  @endforeach
</div>


@endsection

