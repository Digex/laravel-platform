@extends('layouts.admin')

@section('content')
<div id="app" class="container">
    <div class="row">
        <div class="col-md-12 container-box">
            <div class="white-box">
                <form method="POST" @submit.prevent="onSubmit" @keydown="form.errors.clear($event.target.name)">
                  {{ csrf_field() }}
                  <div class="form-group">
                    <label>Name:</label>
                    <input type="" class="form-control" name="name"  v-model="form.name">
                    <span class="help is-danger" v-if="form.errors.has('name')" v-text="form.errors.get('name')"></span>
                  </div>
                  <div class="form-group">
                    <label>Logo</label>
                    <input type="file" class="form-control" name="file"  ref="file"  v-on:change="previewFile">
                    <span class="help is-danger" v-if="form.errors.has('file')" v-text="form.errors.get('file')"></span>
                    <img :src="form.file" v-if="form.file">
                  </div>
                  <div class="form-group">
                    <label>Address:</label>
                    <input type="" class="form-control" name="address"  v-model="form.address">
                    <span class="help is-danger" v-if="form.errors.has('address')" v-text="form.errors.get('address')"></span>
                  </div>
                  <div class="form-group">
                    <label>Site url: </label>
                    <input type="" class="form-control" name="site"  v-model="form.site">
                    <span class="help is-danger" v-if="form.errors.has('site')" v-text="form.errors.get('site')"></span>
                  </div>

                  <button class="button is-primary" v-bind:disabled="form.errors.any()">Create</button>
                </form>
            </div>


        </div>
    </div>
</div>

@endsection

@section('scripts')
    <script src="{{ asset('js/vueAgency.js') }}"></script>
@endsection