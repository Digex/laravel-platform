@extends('layouts.admin')

@section('content')

{{-- Vue App --}}
<div id="appAgency" class="container">
  <div id="header" class="container">
      <div class="row">
          <div class="col-xs-12">
              <a class="button-back" href="/client">< Back to Clients</a>
          </div>
      </div>
      <div class="row">
          <div class="col-xs-12">
              <h3>Create Agency</h3>
          </div>
      </div>
  </div>

  <div id="content" class="container">
      <div class="row  white-box">
          <div class="col-md-12">
              <form method="POST" @submit.prevent="onSubmit" @keydown="form_agency.errors.clear($event.target.name)">
                  <div class="form-group">
                      <label>Name:</label>
                      <input type="" class="form-control" name="name"  v-model="form_agency.name">
                      <span class="help is-danger" v-if="form_agency.errors.has('name')" v-text="form_agency.errors.get('name')"></span>
                  </div>
                  <div class="form-group">
                      <label>Subdomain:</label>
                      <input type="" class="form-control" name="subdomain"  v-model="form_agency.subdomain">
                      <span class="help is-danger" v-if="form_agency.errors.has('subdomain')" v-text="form_agency.errors.get('subdomain')"></span>
                  </div>
                  <div class="form-group">
                      <label>Address:</label>
                          <input type="" class="form-control" name="address"  v-model="form_agency.address">
                          <span class="help is-danger" v-if="form_agency.errors.has('address')" v-text="form_agency.errors.get('address')"></span>
                  </div>
                  <div class="form-group">
                      <label>Website:</label>
                      <input type="" class="form-control" name="site"  v-model="form_agency.site">
                      <span class="help is-danger" v-if="form_agency.errors.has('site')" v-text="form_agency.errors.get('site')"></span>
                  </div>
                  <div class="form-group">
                      <label>Logo</label>
                      <label class="btn-file">
                      Browse... <input type="file" class="form-control" name="file"  ref="file"  v-on:change="previewFile" v-on:click="form_agency.errors.clear($event.target.name)">
                      </label>
                      <span class="help is-danger" v-if="form_agency.errors.has('file')" v-text="form_agency.errors.get('file')"></span>
                      <img class="preview img-responsive" :src="form_agency.file" v-if="form_agency.file">
                  </div>
                  <button class="button-submit is-primary" v-bind:disabled="form_agency.errors.any()">Create</button>
              </form>
          </div>
      </div>
  </div>
</div>
{{-- /Vue App --}}

@endsection

@section('scripts')
     <script src="{{ asset('js/vueAgency.js') }}"></script>
@endsection