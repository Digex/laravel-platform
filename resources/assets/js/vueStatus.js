import Form from './core/Form';
import Datepicker from 'vuejs-datepicker';

new Vue({
  el: '#appReport',

  components: {
    Datepicker,
  },

  data: {
    period_id: Dashboard.period_id,
    client_id: Dashboard.client_id,
    
    /*create_period_form: new Form({
      start_date: '',
      end_date: '',
      client_id: '',
    }),*/

    /*status_form: new Form({
      plugins: [],
      core: '',
      update: '',
      plugin_id: '',
      plugin_name: '',
      plugin_version: '',
      period_id: '',
      
    }),*/

    /*plugin_form: new Form({
      name: '',
    }),*/

    /*select_plugins: [],*/
    select_periods: [],
    
    dashboard_status : null,
    dashboard_status_plugins: [],
    dashboard_status_plugins_period: {
      active: '',
      installed: '',
      show: false,
    }
  },

  mounted() {
    /*this.getPlugins();*/
    this.getStatus();
    this.getStatusPlugins();

    VueBus.$on('updatePeriod', (periodId) => {
        this.period_id = periodId;
        this.getStatus();
        this.getStatusPlugins();
    });
    
  },

  methods: {
    
    /*getPlugins(){
      axios.get('/plugin')
      .then( (response) =>  {
        for (var i = 0; i <= response.data.length - 1; i++) {
          this.select_plugins.push({ id: response.data[i].id, name: response.data[i].name} )
        }
      })
      .catch( (error) => {
        console.log(error)
      })
    },*/

    /*addPlugin(){
      this.status_form.plugins.push({
        id: this.status_form.plugin_id,
        name: this.status_form.plugin_name,
        version: this.status_form.plugin_version,

      });

      this.status_form.plugin_id      ='';
      this.status_form.plugin_name    =''
      this.status_form.plugin_version ='';
    },*/

    /*removePlugin(index){
      this.status_form.plugins.splice(index, 1);
    },*/

    /*updatePluginName(){
      this.status_form.plugin_name = this.select_plugins[this.status_form.plugin_id].name

    },*/

    showPlugins(){
      this.dashboard_status_plugins_period.show = true;
    },

    hidePlugins(){
      this.dashboard_status_plugins_period.show = false;
    },

    updateClientId(id){
      this.client_id = id;
    },

    getStatus(){
      axios.get('/dashboard_status', {
        params: {
          periodId: this.period_id,
        }
      })
      .then( (response) =>  {
        this.dashboard_status = {
          core: response.data.core,
          update: response.data.updated,
        }
      })
      .catch( (error) => {
        console.log(error)
      })
    },

    getStatusPlugins(){
      this.dashboard_status_plugins = [];
      axios.get('/dashboard_plugin', {
        params: {
          periodId: this.period_id,
        }
      })
      .then( (response) =>  {
        var active = 0;
        for (var i = 0; i <= response.data.length - 1; i++) {
          this.dashboard_status_plugins.push({ 
            id: response.data[i].id, 
            name: response.data[i].name, 
            version: response.data[i].pivot.version,
            active: response.data[i].pivot.active,
          });

          if(response.data[i].pivot.active == 1)
          {
            active++;
          }

        }

        this.dashboard_status_plugins_period.installed = this.dashboard_status_plugins.length;
        this.dashboard_status_plugins_period.active = active;

      })
      .catch( (error) => {
        console.log(error)
      })
    },

    /*customFormatter(date) {
      var day = ("0" + (date.getDate())).slice(-2);
      var month = ("0" + (date.getMonth() + 1)).slice(-2);
      var year = date.getFullYear();

       return year + '-' + month + '-' + day;
    },*/

    /*create_period_form_submit() {
      this.create_period_form.client_id = this.client_id;
      this.create_period_form.submit('post', '/report/create_period/')
      .then(data => {
        this.period_id =  data.period.id;

        this.select_periods.push({
          id: data.period.id,
          name: data.period.start,
        });

        $('#periodModal').modal('hide');


      })
      .catch(errors => console.log(errors));
    },*/

    /*plugin_form_submit(id) {
      this.plugin_form.submit('post', '/plugin')
      .then(data => {
        $('#pluginModal').modal('hide');
        this.select_plugins = [];
        this.getPlugins();
      })
      .catch(errors => console.log(errors));
    },*/

    /*status_form_submit() {
      this.status_form.period_id = this.period_id;
      this.status_form.submit('post', '/status')
      .then(data => {
        $('#pluginModal').modal('hide');
        this.select_plugins = [];
        this.getPlugins();
      })
      .catch(errors => console.log(errors));
    }*/
  }
});