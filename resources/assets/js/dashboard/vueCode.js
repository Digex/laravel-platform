
new Vue({
  el: '#appCode',

  data: {
    period_id: Dashboard.period_id,
    client_id: Dashboard.client_id,
    dashboard_code: null

  },

  mounted: function(){
    this.getSecurity();

    VueBus.$on('updatePeriod', (periodId) => {
        this.period_id = periodId;
        this.getSecurity();
    });
  },

  methods: {

    getSecurity(){
      axios.get('/dashboard_code', {
        params: {
          periodId: this.period_id,
        }
      })
      .then( (response) =>  {
        this.dashboard_code = {
          conflicts :response.data.conflicts,
          broken_links: response.data.broken_links,
          mobile_friendly: response.data.mobile_friendly,
          alt_text: response.data.alt_text,
          title_tags: response.data.title_tags,
          meta_description: response.data.meta_description,
          alt_text_format: response.data.alt_text_format,
          title_tags_format: response.data.title_tags_format,
          meta_description_format: response.data.meta_description_format,
        }
      })
      .catch( (error) => {
        console.log(error)
      })
    },
  }  

});