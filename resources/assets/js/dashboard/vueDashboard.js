import ClientComponent from '../components/dashboard/ClientComponent';

window.Dashboard = new Vue({
  el: '#appDashboard',

  components: {
    ClientComponent
  },

  data: {
    period_id : '',
    client_id: '',
    listPeriods: null
  },

  mounted: function(){
    axios.get('/period/'+ this.client_id)
      .then((response) =>  {
        this.listPeriods = response.data;
        console.log(response.data);
      })
      .catch((error) => {
        console.log(error)
      });

  },

  methods: {

    changePeriod() {
        VueBus.$emit('updatePeriod', this.period_id);
        console.log('send');
    },

    getClient(value){
      this.client_id = value;
    },

    getPeriod(value){
      this.period_id = value;
    },

    printDashboard(){
        window.print();
    }
  }  

});