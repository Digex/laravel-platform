
new Vue({
  el: '#appServer',

  data: {
    period_id: Dashboard.period_id,
    client_id: Dashboard.client_id,
    dashboard_server: null

  },

  mounted: function(){
    this.getServerMetrics();

    VueBus.$on('updatePeriod', (periodId) => {
        this.period_id = periodId;
        this.getServerMetrics();
    });
    
  },

  methods: {

    getServerMetrics(){
      axios.get('/dashboard_server', {
        params: {
          periodId: this.period_id,
        }
      })
      .then( (response) =>  {
        this.dashboard_server = {
          backup : response.data.backup,
          host: response.data.host,
          load_time: response.data.load_time,
          content: response.data.delivery,
          items:response.data.media_items,
          storage: response.data.storage,
          uptime: response.data.uptime,
          last_down:response.data.last_down,
        }
      })
      .catch( (error) => {
        console.log(error)
      })
    },
  }  

});