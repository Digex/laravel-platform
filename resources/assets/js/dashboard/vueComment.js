import Form from '../core/Form';

new Vue({
  el: '#appComment',

  data: {
    period_id: Dashboard.period_id,
    client_id: Dashboard.client_id,
    commentBox: false,
    comment_form: new Form({
      comment: '',
      client_id: '',
    }),
  },

  mounted: function(){
    VueBus.$on('updatePeriod', (periodId) => {
        this.period_id = periodId;
    });

    VueBus.$on('showCommentBox', (periodId) => {
        if(this.commentBox)
            this.commentBox = false;
        else
            this.commentBox = true;
    });
  },

  methods: {

    comment_form_submit(){
      this.comment_form.client_id = this.client_id;
      this.comment_form.submit('post', '/dashboard_store_comment')
      .then(response => {
        alert(response);
      })
      .catch(errors => console.log(errors));
    },
  }  

});