import Form from '../core/Form';
import Datepicker from 'vuejs-datepicker';

new Vue({
  el: '#appReport',

  components: {
    Datepicker,
  },

  data: {
    period_id: Dashboard.period_id,
    client_id: Dashboard.client_id,


    select_periods: [],
    
    dashboard_status : null,
    dashboard_status_plugins: [],
    dashboard_status_plugins_period: {
      active: '',
      installed: '',
      show: false,
    }
  },

  mounted() {
    /*this.getPlugins();*/
    this.getStatus();
    this.getStatusPlugins();

    VueBus.$on('updatePeriod', (periodId) => {
        this.period_id = periodId;
        this.getStatus();
        this.getStatusPlugins();
    });
    
  },

  methods: {

    showPlugins(){
      this.dashboard_status_plugins_period.show = true;
    },

    hidePlugins(){
      this.dashboard_status_plugins_period.show = false;
    },

    updateClientId(id){
      this.client_id = id;
    },

    getStatus(){
      axios.get('/dashboard_status', {
        params: {
          periodId: this.period_id,
        }
      })
      .then( (response) =>  {
        this.dashboard_status = {
          core: response.data.core,
          update: response.data.updated,
        }
      })
      .catch( (error) => {
        console.log(error)
      })
    },

    getStatusPlugins(){
      this.dashboard_status_plugins = [];
      axios.get('/dashboard_plugin', {
        params: {
          periodId: this.period_id,
        }
      })
      .then( (response) =>  {
        var active = 0;
        for (var i = 0; i <= response.data.length - 1; i++) {
          this.dashboard_status_plugins.push({ 
            id: response.data[i].id, 
            name: response.data[i].name, 
            version: response.data[i].pivot.version,
            active: response.data[i].pivot.active,
          });

          if(response.data[i].pivot.active == 1)
          {
            active++;
          }

        }

        this.dashboard_status_plugins_period.installed = this.dashboard_status_plugins.length;
        this.dashboard_status_plugins_period.active = active;

      })
      .catch( (error) => {
        console.log(error)
      })
    },

  }
});