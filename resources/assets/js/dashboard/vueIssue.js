import IssueModal from '../components/dashboard/IssueModal';

new Vue({
  el: '#appIssue',

  components: { 
        IssueModal 
    },

  data: {
    period_id: Dashboard.period_id,
    client_id: Dashboard.client_id,
    dashboard_issues: [],
    total_hours: '',
    show: false,
    issueMessage: ''

  },

  mounted: function(){
    this.getIssues();
    
    VueBus.$on('updatePeriod', (periodId) => {
        this.period_id = periodId;
        this.getIssues();
    });
    
  },

  methods: {

    showCommentBox(){
        VueBus.$emit('showCommentBox');
    },

    getIssues(){
      this.dashboard_issues = [];
      axios.get('/dashboard_issue', {
        params: {
          clientId: this.client_id,
          periodId: this.period_id,
        }
      })
      .then( (response) =>  {
        if(response.data.issues.length > 0){
            this.show = true;   
            var total_hours = 0;

            for (var i = 0; i <= response.data.issues.length - 1; i++) {
              total_hours += response.data.issues[i].fix_time;
              this.dashboard_issues.push ({
               name: response.data.issues[i].name,
               status: response.data.issues[i].status,
               fix_time: response.data.issues[i].fix_time,
               fix_number: response.data.issues[i].fix_number,
               fix_date: response.data.issues[i].fix_date,
              });

              this.total_hours = total_hours;

              this.show = response.data.period.issues_status 
              /*if(response.data.period.issues_status == "ignore")
              {
                this.show = false;  
              }
              else{
                this.show = true;
              }*/
            }
        }
      })
      .catch( (error) => {
        console.log(error)
      })
    },

    ignoreIssues(status){
      axios.put('/dashboard_update_issue/'+this.period_id, {
          issues_status: status,
      })
      .then( (response) =>  {
        if(status == 'fix'){
            this.issueMessage = 'We will begin fixing the issues identified and report back to you when our work is completed. Thank you.';
            this.show = "fix";
        }
        else{
            this.issueMessage = 'Thank you for your reply. We will ignore the site issues per your instructions.';
            this.show = "ignore";
        }
        $('#issueModal').modal('show');
          //this.show = false;
      })
      .catch( (error) => {
        console.log(error)
      })
    },
  }  

});