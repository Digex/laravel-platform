import FlagIcon from 'vue-flag-icon'
Vue.use(FlagIcon);
var countries = require("i18n-iso-countries");
countries.registerLocale(require("i18n-iso-countries/langs/en.json"));

new Vue({
  el: '#appSecurity',

  data: {
    period_id: Dashboard.period_id,
    client_id: Dashboard.client_id,
    dashboard_security: null,

  },

  mounted: function(){
    this.getSecurity();

    
    VueBus.$on('updatePeriod', (periodId) => {
        this.period_id = periodId;
        this.getSecurity();
    });
    
  },

  methods: {

    getSecurity(){
      axios.get('/dashboard_security', {
        params: {
          periodId: this.period_id,
        }
      })
      .then( (response) =>  {
        this.dashboard_security = {
          https :response.data.https,
          certified: response.data.certified,
          attacks: response.data.attacks,
          expiration: response.data.expiration,
          country1: response.data.country_1,
          country1_number: response.data.country_1_number,
          country1_full: countries.getName(response.data.country_1, "en"),
          country2: response.data.country_2,
          country2_number: response.data.country_2_number,
          country2_full: countries.getName(response.data.country_2, "en"),
          country3: response.data.country_3,
          country3_number: response.data.country_3_number,
          country3_full: countries.getName(response.data.country_3, "en"),
          country4: response.data.country_4,
          country4_number: response.data.country_4_number,
          country4_full: countries.getName(response.data.country_4, "en"),
          country5: response.data.country_5,
          country5_number: response.data.country_5_number,
          country5_full: countries.getName(response.data.country_5, "en"),
        }
      })
      .catch( (error) => {
        console.log(error)
      })
    },

  }  

});