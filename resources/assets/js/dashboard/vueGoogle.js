import PieChart from '../components/dashboard/PieChart';

new Vue({
    el: '#appGoogleAnalytics',

    components: { 
        PieChart 
    },

  data: {
    period_id: Dashboard.period_id,
    client_id: Dashboard.client_id,
    selectedClient: '',
    selectedPeriod: '',
    metrics: null,
    traffic: null,
    client: null,
    googleViewId: null,
    periods: null,
    authorize: true,
  },

  mounted(){
    
    VueBus.$on('updatePeriod', (periodId) => {
        this.period_id = periodId;
        this.getAnalytics();
    });


  },

  created(){
    this.getAnalytics();
  },

  methods: {

    getAnalytics() {
        axios.get('/dashboard_google', {
            params: {
                period_id: this.period_id,
                client_id: this.client_id,
            }
        })
        .then((response) => {
            this.metrics = response.data.metrics;
            this.traffic = response.data.traffic;
            this.client = response.data.client;

            if(response.data.traffic){
              this.authorize = true;
            }
        })
        .catch((error) => console.log(error));
    },

    authorizeGtm() {
        axios.get('/dashboard_authorize_gtm/', {
            params: {
                client_id: this.client_id,
            }
        })
        .then( (response) =>  {
            alert(response.data);
        })
        .catch( (error) => {
            console.log(error)
        })
    },

  }
});