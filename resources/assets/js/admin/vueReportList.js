import ClientComponent from '../components/dashboard/ClientComponent';
import PeriodInformation from '../components/admin/PeriodInformation';
import ConfirmModal from '../components/admin/ConfirmModal';

new Vue({
    el: '#appReportList',

    components: { 
        ClientComponent,
        PeriodInformation,
        ConfirmModal
    },

    data: {
        clientId: null,
        clientName: null,
        periods: null,
        deleteId: null,
        modal: {
            title: '',
            text: '',
            button: ''
        }
    },

    mounted() {
        this.getPeriods();
        this.getClientName();
    },

    methods: {
        getPeriods(){
            axios.get('/period/'+this.clientId)
            .then( (response) => {
                this.periods = response.data;
            })
            .catch( (error) => console.log(error) );
        },

        deleteReport(id, period, year){
            this.deleteId = id;
            this.modal.title = 'Delete Confirm';
            this.modal.text = 'Are you sure you want to delete the report of <b>'+ period +' '+ year +'</b> ?';
            this.modal.button = 'Delete Report';
            $('#confirmModal').modal('show');
        },

        resetReport(reportId){
            axios.put('/period/reset/'+reportId)
            .then( (response) => {
              console.log(this.periods);
              this.periods.forEach( function(element){
                  if(element.id == response.data){
                      element.issues_status = "show";
                  }
              });
            })
            .catch( (error) => console.log(error) );
        },

        confirm(){
            axios.delete('/period/'+this.deleteId)
            .then( (response) => this.getPeriods() )
            .catch( (error) => console.log(error));
        },

        getClientName(){
            axios.get('/client/getClientName/'+this.clientId)
            .then((response) => this.clientName = response.data)
            .catch((error) => console.log(error));
        },

        getClient(value){
            this.clientId = value;
        },
    }  
});
