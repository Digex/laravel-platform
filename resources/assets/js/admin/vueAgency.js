import Form from '../core/Form';

new Vue({
  el: '#appAgency',

  data: {
    form_agency: new Form({
      name: '',
      file: '',
      address: '',
      site: '',
      subdomain: '',
    }),

    
    
  },


  methods: {
    previewFile(){

        let reader = new FileReader();
        let vm = this;
        reader.onload = (e) => {
            vm.form_agency.file = e.target.result;
        };
        reader.readAsDataURL(this.$refs.file.files[0]);

        //console.log(reader);
    },


    onSubmit() {
      this.form_agency.submit('post', '/agency')
      .then(response => { window.location.href="/client";} )
      .catch(errors => console.log(errors));
    },
 
  }
});