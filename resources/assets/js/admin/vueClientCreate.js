import Form from '../core/Form';
import ColorPicker  from '../components/admin/ColorPicker';
import AgencyModal from '../components/admin/AgencyModal';



new Vue({
  el: '#appClient',
  components: { ColorPicker, AgencyModal },
  data: {
    form: new Form({
      name: '',
      subdomain: '',
      logo: '',
      color1: '#1a6980',
      color2: '#FFFFFF',
      view_id: '',
      site: '', 
      agency: '',
    }),
    listAgencies: null,
    
    
  },

  mounted: function(){
    this.getAgencies();
  },

  methods: {
    previewFile(){

        let reader = new FileReader();
        let vm = this;
        reader.onload = (e) => {
            vm.form.logo = e.target.result;
        };
        reader.readAsDataURL(this.$refs.file.files[0]);

        //console.log(reader);
    },

    getAgencies() {
      axios.get('/agency')
      .then( (response) =>  {
        this.listAgencies = response.data;
      })
      .catch( (error) => {
        console.log(error)
      })
    }, 

    getNewAgency(id){
      this.getAgencies();
      this.form.agency = id;
    },

    onSubmit() {
      this.form.submit('post', '/client')
      .then(data => window.location.href="/client")
      .catch(errors => console.log(errors));
    },
 
  }
});