import Form from '../core/Form';
import ColorPicker  from '../components/admin/ColorPicker';
import AgencyModal from '../components/admin/AgencyModal';



new Vue({
  el: '#appAgency',
  data: {
    form_agency: new Form({
      agency_id: '',
      name: '',
      logo: '',
      address: '',
      site: '',
      subdomain: '',
    }),
  },

  mounted: function(){
    var url = window.location.href.split('/');
    var id = url.splice(4, 1);
    console.log(id);
    this.getAgency(id);
  },

  methods: {
    previewFile(){

        let reader = new FileReader();
        let vm = this;
        reader.onload = (e) => {
            vm.form_agency.logo = e.target.result;
        };
        reader.readAsDataURL(this.$refs.file.files[0]);

        //console.log(reader);
    },

    getAgency(id) {
      axios.get('/agency/get/'+id)
      .then( (response) =>  {
        this.form_agency.agency_id = response.data.id;
        this.form_agency.name = response.data.name;
        this.form_agency.logo = response.data.logo;
        this.form_agency.address = response.data.address;
        this.form_agency.site = response.data.site;
        this.form_agency.subdomain =  response.data.subdomain;
      })
      .catch( (error) => {
        console.log(error)
      })
    }, 


    onSubmit() {
      this.form_agency.submit('put', '/agency/'+this.form_agency.agency_id)
      .then(data => {
        window.location.href="/client";
      })
      .catch(errors => console.log(errors));
    },
 
  }
});