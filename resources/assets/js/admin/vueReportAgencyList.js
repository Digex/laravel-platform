import ClientComponent from '../components/dashboard/ClientComponent';
import PeriodAgencyInformation from '../components/admin/PeriodAgencyInformation';


new Vue({
    el: '#appReportAgencyList',

    components: { 
        ClientComponent,
        PeriodAgencyInformation,
    },

    data: {
        clientId: null,
        clientName: null,
        periods: null,
        subdomain: null,
    },

    mounted() {
        this.getPeriods();
        this.getClientName();
    },

    methods: {
        getPeriods(){
            axios.get('/period/'+this.clientId)
            .then( (response) => {
                this.periods = response.data;
            })
            .catch( (error) => console.log(error) );
        },


        getClientName(){
            axios.get('/' + this.subdomain+ '/client/getClientName/'+this.clientId)
            .then((response) => this.clientName = response.data)
            .catch((error) => console.log(error));
        },

        getClient(value){
            this.clientId = value;
        },

        getSubdomain(value){
            this.subdomain = value;
        },
    }  
});
