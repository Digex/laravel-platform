import ClientComponent from '../components/dashboard/ClientComponent';
import UserInformation from '../components/admin/UserInformation';
import ConfirmModal from '../components/admin/ConfirmModal';
import UserModal from '../components/admin/UserModal';

new Vue({
    el: '#appClientShow',

    components: { 
        UserInformation,
        ClientComponent,
        ConfirmModal,
        UserModal
    },

    data: {
        clientId: null,
        clientName: null,
        users: null,
        clientInfo: [],
        editInfo: [],
        edit: 0,
        deleteId: null,
        modal: {
            title: '',
            text: '',
            button: ''
        }
    },

    mounted() {
        this.getUsers();
        this.getClientName();
    },

    methods: {
        getUsers(){
            axios.get('/user',{
                params: {
                    id: this.clientId
                }
            })
            .then( (response) => {
                this.users = response.data;
                this.edit = 0;
            })
            .catch( (error) => console.log(error) );
        },

        getClientInfo(){
            axios.get('/client/get/'+this.clientId)
            .then( (response) => {
                this.clientInfo = response.data;
            })
            .catch( (error) => console.log(error) );
        },

        editUser(id){
            axios.get('/user/'+id)
            .then( (response) => {
                this.edit = id;
                this.editInfo = response.data;
                $('#userModal').modal('show');
            })
            .catch( (error) => console.log(error));
        },

        newUser(){
            this.edit = 0;
            $('#userModal').modal('show');
        },

        deleteUser(id, email){
            this.deleteId = id;
            this.modal.title = 'Delete Confirm';
            this.modal.text = 'Are you sure you want to delete <b>'+ email +'</b> ?';
            this.modal.button = 'Delete User';
            $('#confirmModal').modal('show');
        },

        confirm(){
            axios.delete('/user/'+this.deleteId)
            .then( (response) => this.getUsers() )
            .catch( (error) => console.log(error));
        },

        getClientName(){
            axios.get('/client/getClientName/'+this.clientId)
            .then((response) => this.clientName = response.data)
            .catch((error) => console.log(error));
        },

        getClient(value){
            this.clientId = value;
        },
    }  
});
