import ClientComponent from '../components/dashboard/ClientComponent';
import UserInformation from '../components/admin/UserInformation';
import ConfirmModal from '../components/admin/ConfirmModal';
import UserAgencyModal from '../components/admin/UserAgencyModal';

new Vue({
    el: '#appAgencyShow',

    components: { 
        UserInformation,
        ClientComponent,
        ConfirmModal,
        UserAgencyModal
    },

    data: {
        agencyId: null,
        agencyName: null,
        users: null,
        agencyInfo: [],
        editInfo: [],
        edit: 0,
        deleteId: null,
        modal: {
            title: '',
            text: '',
            button: ''
        }
    },

    mounted() {
        this.getUsers();
        this.getAgencyName();
    },

    methods: {
        getUsers(){
            axios.get('/agency/users/'+this.agencyId)
            .then( (response) => {
                this.users = response.data;
                this.edit = 0;
            })
            .catch( (error) => console.log(error) );
        },

        getAgencyInfo(){
            axios.get('/agency/get/'+this.agencyId)
            .then( (response) => {
                this.agencyInfo = response.data;
            })
            .catch( (error) => console.log(error) );
        },

        editUser(id){
            axios.get('/user/'+id)
            .then( (response) => {
                this.edit = id;
                this.editInfo = response.data;
                $('#userModal').modal('show');
            })
            .catch( (error) => console.log(error));
        },

        newUser(){
            this.edit = 0;
            $('#userModal').modal('show');
        },

        deleteUser(id, email){
            this.deleteId = id;
            this.modal.title = 'Delete Confirm';
            this.modal.text = 'Are you sure you want to delete <b>'+ email +'</b> ?';
            this.modal.button = 'Delete User';
            $('#confirmModal').modal('show');
        },

        confirm(){
            axios.delete('/user/'+this.deleteId)
            .then( (response) => this.getUsers() )
            .catch( (error) => console.log(error));
        },

        getAgencyName(){
            axios.get('/agency/getAgencyName/'+this.agencyId)
            .then((response) => this.agencyName = response.data)
            .catch((error) => console.log(error));
        },

        getAgency(value){
            this.agencyId = value;
        },
    }  
});
