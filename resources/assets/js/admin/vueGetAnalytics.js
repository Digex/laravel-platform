new Vue({
  el: '#appGoogleAnalytics',

  data: {
    selectedClient: '',
    selectedPeriod: '',
    metrics: null,
    traffic: null,
    googleViewId: null,
    clients: null,
    periods: null,
  },

  mounted(){
    axios.get('/analytics/clients')
    .then((response) =>{
        this.clients = response.data;
    })
    .catch((error) => console.log(error));
  },

  methods: {

    getPeriods() {
       this.metrics = this.traffic = null; 
        axios.get('/analytics/periods', {
            params: {
                clientId: this.selectedClient.id
            }
        })
        .then((response) =>{
            this.periods = response.data;
        })
        .catch((error) => console.log(error));
    },

    getAnalytics() {
        if(!this.selectedPeriod){
            alert('Select a period');
            return;
        }
        axios.get('/google', {
            params: {
                client_id: this.selectedClient.id,
                period_id: this.selectedPeriod.id,
                start: this.selectedPeriod.start,
                end: this.selectedPeriod.end,
            }
        })
        .then((response) => {
            console.log(response.data);
            this.metrics = response.data.metrics;
            this.traffic = response.data.traffic;
        })
        .catch((error) => console.log(error));
    }, 

    storeAnalytics() {
        axios.post('/google', {
            periodId: this.selectedPeriod.id,
            metrics: this.metrics,
            traffic: this.traffic,
        })
        .then((response) =>{
            alert('Saved information for '+ this.selectedClient.name + ' for the period '+ this.selectedPeriod.start + ' '+ this.selectedPeriod.end);
            console.log(response.data);
        })
        .catch((error) => console.log(error));
    },

  }
});