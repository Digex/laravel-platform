import AgencyInformation from '../components/admin/AgencyInformation';
import ConfirmModal from '../components/admin/ConfirmModal';

new Vue({
    el: '#appClient',

    components: { 
        AgencyInformation,
        ConfirmModal 
    },

    data: {
        agencies: null,
        deleteId: null,
        modal: {
            title: '',
            text: '',
            button: '',
            type: '',
        }
    },

    mounted() {
        this.get();
    },

    methods: {
        get(){
            console.log("update");
            axios.get('/agency/information')
            .then( (response) => this.agencies = response.data )
            .catch( (error) => console.log(error) );
        },

        getClients(agency_id){
            axios.get('/client/information/'+agency_id)
            .then( (response) => this.clients = response.data )
            .catch( (error) => console.log(error) );
        },

        deleteagency(id, name){
            this.deleteId = id;
            this.modal.title = 'Delete Confirm';
            this.modal.text = 'This process will delete all the <b>'+ name +'</b> users and clients. <br>Are you sure you want to delete <b>'+ name +'</b> agency? <br>';
            this.modal.button = 'Delete';
            this.modal.type = 'agency';
            $('#confirmModal').modal('show');
        },

        deleteclientagency(id, name){
            this.deleteId = id;
            this.modal.title = 'Delete Confirm';
            this.modal.text = 'This process also will delete all the <b>'+ name +'</b> users. <br>Are you sure you want to delete <b>'+ name +'</b> client? <br>';
            this.modal.button = 'Delete';
            this.modal.type = 'client';
            $('#confirmModal').modal('show');
        },


        confirm(type){
          if(type=="agency"){
            axios.delete('/agency/'+this.deleteId)
            .then( (response) => this.get() )
            .catch( (error) => console.log(error));
          }

          if(type=="client"){
            console.log("delete");
            axios.delete('/client/'+this.deleteId)
            .then( (response) => this.get() )
            .catch( (error) => console.log(error));
            
          }
        }
    }  
});
