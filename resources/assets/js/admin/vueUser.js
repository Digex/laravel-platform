import Form from '../core/Form';

new Vue({
  el: '#appUser',

  data: {
    form: new Form({
      name: '',
      lastname: '',
      email: '',
      password: '',
      password_confirmation: ''
    }),
  },


  methods: {
    onSubmit() {
      this.form.submit('post', '/user')
      .then(data => console.log(data))
      .catch(errors => console.log(errors));
    } 
  }
});