import Form from '../core/Form';
//import DatePicker from 'vue2-datepicker'
import DatePicker from '@4keys/vuejs-datepicker';
import PluginModal from '../components/admin/PluginModal';



new Vue({
  el: '#appReport',
  components: { DatePicker, PluginModal },
  data: {
    period_id : null,
    client_id : '',
    clientName: null,
    metrics: null,
    traffic: null,
    client: {
        client: {
            google_view_id: null
        }
    },
    buttonAnalytics: true,
    edit_form: new Form({
      //period
      start_date:'',
      end_date: '',
      
      //status  
      core: '',
      last_update: '',
      plugins: [],
      
      //server
      backup:'',
      load_time:'',
      media:'',
      uptime:'',
      host:'',
      cdn:'',
      files:'',
      last_down:'',

      //security
      attacks:'',
      certified:'',
      expiration:null,
      https:'',
      country_1:'',
      country_1_number:'',
      country_2:'',
      country_2_number:'',
      country_3:'',
      country_3_number:'',
      country_4:'',
      country_4_number:'',            
      country_5:'',
      country_5_number:'',

      //code
      conflicts: '',
      broken_links: '',
      mobile_friendly: '',
      alt_text:'',
      title_tags:'',
      meta_description:'',

      //issue
      issue: [],

    }),

    select_plugins: [],
    user_plugins: [],
    plugin_selected: '',
    plugin_name: '',
    plugin_version: '',

    
    
  },

  mounted: function(){
    var url = window.location.href.split('/');
    var id = url.splice(4, 1);
    this.client_id = id[0];
    this.getClientName();
    this.getPlugins();
    this.getClient();
    this.getPeriod();
  },

  methods: {

    getPeriod() {
      axios.get('/period/client_period/'+this.client_id)
      .then( (response) =>  {
        console.log(response.data);
        
        if(response.data.length != 0){
          this.edit_form.start_date = response.data.period.start;
          this.edit_form.end_date = response.data.period.end;
          this.edit_form.core = response.data.status.core;
          this.edit_form.last_update = response.data.status.updated;

          for (var i = 0; i <= response.data.plugins.length - 1; i++) {
            this.select_plugins.push({ id: response.data.plugins[i].id, name: response.data.plugins[i].name} )
          }

          for (var i = 0; i <= response.data.plugins_period.length - 1; i++) {
            this.edit_form.plugins.push({ 
              version: response.data.plugins_period[i].pivot.version,
              active: response.data.plugins_period[i].pivot.active,
              id: response.data.plugins_period[i].pivot.plugin_id,
            });

            this.user_plugins.push({ 
              id: response.data.plugins_period[i].id, 
              name: response.data.plugins_period[i].name, 
              version: response.data.plugins_period[i].pivot.version,
            });
          }  

            this.edit_form.backup = response.data.server.backup;
            this.edit_form.load_time = response.data.server.load_time.toFixed(2);;
            this.edit_form.media = response.data.server.media_items;
            this.edit_form.uptime = response.data.server.uptime.toFixed(2);;
            this.edit_form.host = response.data.server.host;
            this.edit_form.cdn = response.data.server.delivery;
            this.edit_form.files = response.data.server.storage;
            this.edit_form.last_down = response.data.server.last_down;

            this.edit_form.attacks = response.data.security.attacks;
            this.edit_form.certified = response.data.security.certified;
            this.edit_form.expiration = response.data.security.expiration;
            this.edit_form.https = response.data.security.https;
            this.edit_form.country_1 = response.data.security.country_1;
            this.edit_form.country_1_number = response.data.security.country_1_number;
            this.edit_form.country_2 = response.data.security.country_2;
            this.edit_form.country_2_number = response.data.security.country_2_number;
            this.edit_form.country_3 = response.data.security.country_3;
            this.edit_form.country_3_number = response.data.security.country_3_number;
            this.edit_form.country_4 = response.data.security.country_4;
            this.edit_form.country_4_number = response.data.security.country_4_number;
            this.edit_form.country_5 = response.data.security.country_5;
            this.edit_form.country_5_number = response.data.security.country_5_number;

            this.edit_form.conflicts = response.data.code.conflicts;
            this.edit_form.broken_links = response.data.code.broken_links;
            this.edit_form.mobile_friendly = response.data.code.mobile_friendly;
            this.edit_form.alt_text = response.data.code.alt_text;
            this.edit_form.title_tags = response.data.code.title_tags;
            this.edit_form.meta_description = response.data.code.meta_description;

            for (var i = 0; i <= response.data.issues.length - 1; i++) {
              this.edit_form.issue.push({ 
                id: response.data.issues[i].id, 
                name: response.data.issues[i].name, 
                fix_number: response.data.issues[i].fix_number,
                fix_time: response.data.issues[i].fix_time,
              });
            }  
        }
        



      })
      .catch( (error) => {
        console.log(error)
      })
    }, 

    getClient(){
      axios.get('/client/get/'+this.client_id)
      .then( (response) =>  {
         this.client = response.data;
      })
      .catch( (error) => {
        console.log(error)
      })
    },

    getAnalytics() {
        this.buttonAnalytics = false;
        axios.get('/google', {
            params: {
                client_id: this.client.client.id,
                period_id: this.period_id,
                start: this.edit_form.start_date,
                end: this.edit_form.end_date,
            }
        })
        .then((response) => {
            this.buttonAnalytics = true;
            this.metrics = response.data.metrics;
            this.traffic = response.data.traffic;
        })
        .catch((error) => {
            this.buttonAnalytics = true;
            console.log(error);
        });
    },

    storeAnalytics() {
        axios.post('/google', {
            period_id: this.period_id,
            metrics: this.metrics,
            traffic: this.traffic,
        })
        .then((response) =>{
            console.log(response.data);
        })
        .catch((error) => console.log(error));
    },

    getPlugins(){
      axios.get('/plugin')
      .then( (response) =>  {
        this.select_plugins = [];
        for (var i = 0; i <= response.data.length - 1; i++) {
          this.select_plugins.push({ id: response.data[i].id, name: response.data[i].name} )
        }
        this.plugin_selected = this.newPlugin;
      })
      .catch( (error) => {
        console.log(error)
      })
    },   

    addPlugin(){
      this.edit_form.plugins.push({
        version: this.edit_form.plugin_version || '',
        active: 1,
        id: this.plugin_selected,
      });

      this.user_plugins.push({ 
        id: this.plugin_selected,
        name: this.plugin_name, 
        version: this.plugin_version || '',
      });

      delete this.edit_form.errors.errors.plugins;

    },

    updatePluginName(){

      for (var i = 0 ; i <= this.select_plugins.length; i++) {
        if(this.select_plugins[i].id == this.plugin_selected){
          this.plugin_name = this.select_plugins[i].name;    
        }
      }
      

    },

    removePlugin(index){
      this.user_plugins.splice(index, 1);
      this.edit_form.plugins.splice(index, 1);
    },

    getNewPlugin(plugin){
      //this.select_plugins = [];
      this.getPlugins();
      this.newPlugin = plugin.id;      
      this.plugin_name = plugin.name;
    },

    addIssue(){
      this.edit_form.issue.push({
        id: '',
        name: this.edit_form.issue.name, 
        fix_number: this.edit_form.issue.fix_number,
        fix_time: this.edit_form.issue.fix_time,

      });

    },

    removeIssue(index){
      this.edit_form.issue.splice(index, 1);

    },


    edit_form_submit() {
      this.edit_form.expiration = this.edit_form.certified ? this.edit_form.expiration : null;
      this.edit_form.submit('post', '/period/'+this.client_id)
      .then(data => {
        this.period_id = data.id;
        this.storeAnalytics();
        window.location.href="/period/show/"+this.period_id;
      })
      .catch(errors => {
        console.log(errors);
    });
    },

    customFormatter(date) {
      var day = ("0" + (date.getDate())).slice(-2);
      var month = ("0" + (date.getMonth() + 1)).slice(-2);
      var year = date.getFullYear();

       return year + '-' + month + '-' + day;
    },

    getClientName(){
        axios.get('/client/getClientName/'+this.client_id)
        .then((response) => this.clientName = response.data)
        .catch((error) => console.log(error));
    },

    clearError(error_name){
      delete this.edit_form.errors.errors[error_name]; 
    },

   /* setEndDate(data){
      delete this.edit_form.errors.errors["start_date"];  
      delete this.edit_form.errors.errors["end_date"];  
      //var date = new Date(data);
      data = data.setMonth(data.getMonth()+1);
      var date = new Date(data);
      //console.log(date.toString());
      this.edit_form.end_date = date.toString();
    },*/

    scrollTop(){
      //window.scrollTo(0, 0);
      var scrollStep = -window.scrollY / (100 / 15),
          scrollInterval = setInterval(function(){
          if ( window.scrollY != 0 ) {
              window.scrollBy( 0, scrollStep );
          }
          else clearInterval(scrollInterval); 
      },15);
    }
 
  }
});