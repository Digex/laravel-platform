import Form from '../core/Form';
import ColorPicker  from '../components/admin/ColorPicker';
import AgencyModal from '../components/admin/AgencyModal';



new Vue({
  el: '#appClient',
  components: { ColorPicker, AgencyModal },
  data: {
    form: new Form({
      client_id: '',
      name: '',
      subdomain: '',
      logo: '',
      color1: '',
      color2: '',
      view_id: '',
      site: '', 
      agency: '',
    }),
    listAgencies: null,
    
    
  },

  mounted: function(){
    this.getAgencies();

    var url = window.location.href.split('/');
    var id = url.splice(4, 1);
    this.getClient(id);

  },

  methods: {
    previewFile(){

        let reader = new FileReader();
        let vm = this;
        reader.onload = (e) => {
            vm.form.logo = e.target.result;
        };
        reader.readAsDataURL(this.$refs.file.files[0]);

        //console.log(reader);
    },

    getClient(id) {
      axios.get('/client/get/'+id)
      .then( (response) =>  {
        console.log(response.data);
        this.form.client_id = response.data.client.id;
        this.form.name = response.data.client.name;
        this.form.subdomain = response.data.client.subdomain;
        this.form.logo = response.data.logo;
        this.form.color1 = "#" + response.data.client.primary_color;
        this.form.color2 = "#" + response.data.client.secondary_color;
        this.form.site = response.data.client.site;
        this.form.view_id = response.data.client.google_view_id;
        this.form.agency = response.data.client.agency_id;
      })
      .catch( (error) => {
        console.log(error)
      })
    }, 

    getAgencies() {
      axios.get('/agency')
      .then( (response) =>  {
        this.listAgencies = response.data;
      })
      .catch( (error) => {
        console.log(error)
      })
    }, 

    getNewAgency(id){
      this.getAgencies();
      this.form.agency = id;
    },


    onSubmit() {
      this.form.submit('put', '/client/'+this.form.client_id)
      .then(data => {
        window.location.href="/client";
      })
      .catch(errors => console.log(errors));
    },
 
  }
});