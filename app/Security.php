<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Security extends Model
{
    //
    protected $fillable = [
        'https', 'certified', 'attacks', 'expiration', 'country_1', 'country_1_number', 'country_2', 'country_2_number', 'country_3', 'country_3_number', 'country_4', 'country_4_number', 'country_5', 'country_5_number', 'period_id'
    ];
}
