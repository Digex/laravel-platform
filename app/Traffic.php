<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Traffic extends Model
{
    protected $guarded = [];

    public function period()
    {
        return $this->belongsTo('App\Period');
    }
}
