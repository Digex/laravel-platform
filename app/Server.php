<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Server extends Model
{
    protected $fillable = [
        'backup', 'host', 'load_time', 'delivery', 'media_items', 'storage', 'uptime', 'last_down', 'period_id'
    ];
}
