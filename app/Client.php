<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name', 'subdomain', 'logo', 'site', 'primary_color', 'secondary_color', 'google_view_id', 'agency_id', 
    ];

    public function users()
    {
        return $this->hasMany('App\User');
    }

    public function periods()
    {
        return $this->hasMany('App\Period');
    }
}
