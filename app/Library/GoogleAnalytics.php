<?php

namespace App\Library;

use Analytics;
use App\Client;
use App\Metric;
use App\Period;
use Carbon\Carbon;
use Spatie\Analytics\Period as SpatiePeriod;

class GoogleAnalytics
{
    
    public $metrics = [];
    public $traffic = [];
    public $period;
    public $pastMonth;
    public $client;

    public function __construct($clientId, $start, $end)
    {
        $start              = substr($start, 0, 10);
        $end                = substr($end, 0, 10);
        $startDate          = Carbon::createFromFormat('Y-m-d', $start);
        $endDate            = Carbon::createFromFormat('Y-m-d', $end);
        $this->period       = SpatiePeriod::create($startDate, $endDate);
        $startPastMonth     = Carbon::createFromFormat('Y-m-d', $start)->subDays(30)->startOfDay();
        $this->pastMonth    = SpatiePeriod::create($startPastMonth, $startDate);
        $this->client       = Client::find($clientId);
        
        config(['analytics.view_id' => $this->client->google_view_id]);
    }

    public function getMetrics()
    {
        $this->setVisitors();
        $this->setTopTraffic();
        $this->setVisits();
        $this->setAveragePagesVisits();
        
        return ($this->metrics);
    }

    public function setVisitors()
    {
        $visitors = Analytics::fetchUserTypes($this->period)->toArray();
        
        $returning_visitors = (array_key_exists(1, $visitors)) ? $visitors[1]['sessions'] : 0;

        $this->metrics['new_visitors']          = round($visitors[0]['sessions']*100 / ($visitors[0]['sessions']+ $returning_visitors), 2);
        $this->metrics['returning_visitors']    = round($returning_visitors*100 / ($visitors[0]['sessions']+$returning_visitors), 2);
    }

    public function setTopTraffic()
    {
        $topTraffic = Analytics::performQuery(
            $this->period, 
            'ga:pageviews', 
            [
                'dimensions' => 'ga:date',
                'sort' => '-ga:pageviews',
                'max-results' => 3,
            ]
        );

        $this->metrics['day_top']       = isset($topTraffic['rows'][0][0]) ? Carbon::createFromFormat('Ymd', $topTraffic['rows'][0][0])->format('d') : '';
        $this->metrics['day_1']         = isset($topTraffic['rows'][0][0]) ? Carbon::createFromFormat('Ymd', $topTraffic['rows'][0][0])->format('F d, Y') : '';
        $this->metrics['day_1_visits']  = $topTraffic['rows'][0][1] ?? 0;
        $this->metrics['day_2']         = isset($topTraffic['rows'][1][0]) ? Carbon::createFromFormat('Ymd', $topTraffic['rows'][1][0])->format('F d, Y') : '';
        $this->metrics['day_2_visits']  = $topTraffic['rows'][1][1] ?? 0;
        $this->metrics['day_3']         = isset($topTraffic['rows'][2][0]) ? Carbon::createFromFormat('Ymd', $topTraffic['rows'][2][0])->format('F d, Y') : '';
        $this->metrics['day_3_visits']  = $topTraffic['rows'][2][1] ?? 0;
    }

    public function setVisits()
    {
        $uniquePast        = Analytics::performQuery($this->pastMonth, 'ga:uniquePageviews')['rows'][0][0] ?? 0;
        $totalPast         = Analytics::performQuery($this->pastMonth, 'ga:pageviews')['rows'][0][0] ?? 0;
        $conversionPast    = Analytics::performQuery($this->pastMonth, 'ga:transactionsPerSession')['rows'][0][0] ?? 0;
        $conversionPast    = round($conversionPast, 2);

        $this->metrics['unique']                = Analytics::performQuery($this->period, 'ga:uniquePageviews')['rows'][0][0] ?? 0;
        $this->metrics['unique_percentage']     = $uniquePast ? round((($this->metrics['unique'] - $uniquePast) / $uniquePast) * 100, 1) : 0;
        $this->metrics['total']                 = Analytics::performQuery($this->period, 'ga:pageviews')['rows'][0][0] ?? 0;
        $this->metrics['total_percentage']      = $totalPast ? round((($this->metrics['total'] - $totalPast) / $totalPast) * 100, 1) : 0;
        $this->metrics['conversion']            = Analytics::performQuery($this->period, 'ga:transactionsPerSession')['rows'][0][0] ?? 0;
        $this->metrics['conversion']            = round($this->metrics['conversion'], 2);
        $this->metrics['conversion_percentage'] = $conversionPast ? round((($this->metrics['conversion'] - $conversionPast) / $conversionPast) * 100, 1) : 0;
    }

    public function setAveragePagesVisits()
    {
        $this->metrics['avg_pages']     = Analytics::performQuery($this->period, 'ga:pageviewsPerSession')['rows'][0][0] ?? 0;
        $this->metrics['avg_pages']     = round($this->metrics['avg_pages'], 2);
        $this->metrics['avg_time']      = Analytics::performQuery($this->period, 'ga:avgSessionDuration')['rows'][0][0] ?? 0;
        $this->metrics['avg_time']      = gmdate("i:s", $this->metrics['avg_time']);
    }

    public function getTraffic()
    {
        $this->setPopularPages();
        $this->setOperatingSystem();
        $this->setBrowsers();
        $this->setDevice();

        return ($this->traffic);
    }

    public function setDevice()
    {
        $devices = Analytics::performQuery($this->period, 'ga:sessions', ['dimensions' => 'ga:deviceCategory']);
        $this->traffic['desktop'] = $this->traffic['mobile'] = $this->traffic['tablet'] = $total = 0;

        foreach ($devices['rows'] as $device) {
            $total += $device[1];
            switch ($device[0]) {
                case 'desktop':
                    $this->traffic['desktop'] += $device[1];
                    break;

                case 'mobile':
                    $this->traffic['mobile'] = $device[1];
                    break;

                case 'tablet':
                    $this->traffic['tablet'] = $device[1];
                    break;
                
                default:
                    $this->traffic['desktop'] += $device[1];
                    break;
            }
        }
        $this->traffic['desktop']  = round(($this->traffic['desktop']*100) / $total, 2);
        $this->traffic['mobile']   = round(($this->traffic['mobile']*100) / $total, 2);
        $this->traffic['tablet']   = round(($this->traffic['tablet']*100) / $total, 2);
    }

    public function setPopularPages()
    {
        //$url = $this->client->site;
        $popularPages = Analytics::performQuery(
            $this->period, 
            'ga:pageviews', 
            [
                'dimensions' => 'ga:pagePath', 
                'sort' => '-ga:pageviews', 
                'max-results' => 5
            ]
        );

        $this->traffic['page_1_link']       = isset($popularPages['rows'][0][0]) ? $popularPages['rows'][0][0] : '';
        $this->traffic['page_1_link_views'] = $popularPages['rows'][0][1] ?? 0;
        $this->traffic['page_2_link']       = isset($popularPages['rows'][1][0]) ? $popularPages['rows'][1][0] : '';
        $this->traffic['page_2_link_views'] = $popularPages['rows'][1][1] ?? 0;
        $this->traffic['page_3_link']       = isset($popularPages['rows'][2][0]) ? $popularPages['rows'][2][0] : '';
        $this->traffic['page_3_link_views'] = $popularPages['rows'][2][1] ?? 0;
        $this->traffic['page_4_link']       = isset($popularPages['rows'][3][0]) ? $popularPages['rows'][3][0] : '';
        $this->traffic['page_4_link_views'] = $popularPages['rows'][3][1] ?? 0;
        $this->traffic['page_5_link']       = isset($popularPages['rows'][4][0]) ? $popularPages['rows'][4][0] : '';
        $this->traffic['page_5_link_views'] = $popularPages['rows'][4][1] ?? 0;
    }

    public function setOperatingSystem()
    {
        $operatingSystem = Analytics::performQuery($this->period, 'ga:sessions', ['dimensions' => 'ga:operatingSystem']);
        $this->traffic['android'] = $this->traffic['ios'] = $this->traffic['windows'] = 0;
        $this->traffic['mac'] = $this->traffic['linux'] = $this->traffic['os_other'] = $total = 0;

        foreach ($operatingSystem['rows'] as $os) {
            $total += $os[1];
            switch ($os[0]) {
                case 'Android':
                    $this->traffic['android'] = $os[1];
                    break;

                case 'iOS':
                    $this->traffic['ios'] = $os[1];
                    break;

                case 'Windows':
                    $this->traffic['windows'] = $os[1];
                    break;

                case 'Macintosh':
                    $this->traffic['mac'] = $os[1];
                    break;

                case 'Linux':
                    $this->traffic['linux'] = $os[1];
                    break;
                
                default:
                    $this->traffic['os_other'] += $os[1];
                    break;
            }
        }
        $this->traffic['android']   = round(($this->traffic['android']*100) / $total, 2);
        $this->traffic['ios']       = round(($this->traffic['ios']*100) / $total, 2);
        $this->traffic['windows']   = round(($this->traffic['windows']*100) / $total, 2);
        $this->traffic['mac']       = round(($this->traffic['mac']*100) / $total, 2);
        $this->traffic['linux']     = round(($this->traffic['linux']*100) / $total, 2);
        $this->traffic['os_other']  = round(($this->traffic['os_other']*100) / $total, 2);
    }

    public function setBrowsers()
    {
        $browsers = Analytics::performQuery($this->period, 'ga:sessions', ['dimensions' => 'ga:browser']);
        $this->traffic['chrome'] = $this->traffic['firefox'] = $this->traffic['explorer'] = 0;
        $this->traffic['edge'] = $this->traffic['safari'] = $this->traffic['browser_other'] = $total = 0;

        foreach ($browsers['rows'] as $browser) {
            $total += $browser[1];
            switch ($browser[0]) {
                case 'Chrome':
                    $this->traffic['chrome'] = $browser[1];
                    break;

                case 'Firefox':
                    $this->traffic['firefox'] = $browser[1];
                    break;

                case 'Internet Explorer':
                    $this->traffic['explorer'] = $browser[1];
                    break;

                case 'Edge':
                    $this->traffic['edge'] = $browser[1];
                    break;

                case 'Safari':
                    $this->traffic['safari'] += $browser[1];
                    break;

                case 'Safari (in-app)':
                    $this->traffic['safari'] += $browser[1];
                    break;
                
                default:
                    $this->traffic['browser_other'] += $browser[1];
                    break;
            }
        }
        $this->traffic['chrome']        = round(($this->traffic['chrome']*100) / $total, 2);
        $this->traffic['firefox']       = round(($this->traffic['firefox']*100) / $total, 2);
        $this->traffic['explorer']      = round(($this->traffic['explorer']*100) / $total, 2);
        $this->traffic['edge']          = round(($this->traffic['edge']*100) / $total, 2);
        $this->traffic['safari']        = round(($this->traffic['safari']*100) / $total, 2);
        $this->traffic['browser_other'] = round(($this->traffic['browser_other']*100) / $total, 2);
    }

    public function setGoogleViewId()
    {
        /*$path = base_path('.env');
        $oldValue = env($key);

        if (file_exists($path)) {
            file_put_contents(
                $path, str_replace(
                    $key.'='.$delim.$oldValue.$delim, 
                    $key.'='.$delim.$newValue.$delim, 
                    file_get_contents($path)
                )
            );
        }*/
    }


}
