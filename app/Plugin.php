<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plugin extends Model
{
    //
      protected $fillable = [
        'name'
    ];

    /**
     * The period that belong to the plugin.
     */
    public function periods()
    {
        return $this->belongsToMany('App\Period', "plugin_period")->withPivot('version', 'active');
    }
}
