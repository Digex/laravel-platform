<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Period extends Model
{
    protected $fillable = [
        'start', 'end', 'month', 'year', 'issues_status', 'client_id'
    ];

    public function metric()
    {
        return $this->hasOne('App\Metric');
    }

    public function traffic()
    {
        return $this->hasOne('App\Traffic');
    }

    public function client()
    {
        return $this->belongsTo('App\Client');
    }

    public function plugins()
    {
        return $this->belongsToMany('App\Plugin','plugin_period')->withPivot('version', 'active');
    }

}
