<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;

use App\User;
use App\Client;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response (User::where('client_id', request('id'))->get(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate(request(), [
            'name'                  => 'required|max:50',
            'lastname'              => 'required|max:50',
            'email'                 => 'required|email|unique:users|max:100',
            'password'              => 'required|between:8,30|confirmed',
            'password_confirmation' => 'required|between:8,30'
        ]);

        $client =  Client::where("id", request('client_id'))->first();

        $user = User::Create([
            'name'      => request('name'),
            'email'     => request('email'),
            'lastname'  => request('lastname'),
            'password'  => Hash::make(request('password')),
            'client_id' => request('client_id'),
            'agency_id' => $client->agency_id,
        ]);

        if(request('client_id') == 1){
            $user->assignRole('Admin');
        }

        return response('User Created!', 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return User::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('user/edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate(request(), [
            'name'                  => 'required|max:50',
            'lastname'              => 'required|max:50',
            'email'                 => 'required|email|max:100|unique:users,email,'.request('user_id'),
        ]);

        if(request('password') != ''){
            $this->validate(request(), [
                'password'              => 'required|between:8,30|confirmed',
                'password_confirmation' => 'required|between:8,30'
            ]);
        }

        User::where('id', request('user_id'))->update([
            'name'      => request('name'),
            'email'     => request('email'),
            'lastname'  => request('lastname'),
            'password'  => Hash::make(request('password')),
        ]);

        return response('User Created!', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
        return response(null, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeUserAgency(Request $request)
    {
        

        $this->validate(request(), [
            'name'                  => 'required|max:50',
            'lastname'              => 'required|max:50',
            'email'                 => 'required|email|unique:users|max:100',
            'password'              => 'required|between:8,30|confirmed',
            'password_confirmation' => 'required|between:8,30'
        ]);

        $user = User::Create([
            'name'      => request('name'),
            'email'     => request('email'),
            'lastname'  => request('lastname'),
            'password'  => Hash::make(request('password')),
            'agency_id' => request('agency_id')
        ]);

        if(request('agency_id') == 1){
            $user->assignRole('SuperAdmin');
        }
        else{
          $user->assignRole('Admin'); 
        }

        return response('User Created!', 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateUserAgency(Request $request, $id)
    {
        $this->validate(request(), [
            'name'                  => 'required|max:50',
            'lastname'              => 'required|max:50',
            'email'                 => 'required|email|max:100|unique:users,email,'.request('user_id'),
        ]);

        if(request('password') != ''){
            $this->validate(request(), [
                'password'              => 'required|between:8,30|confirmed',
                'password_confirmation' => 'required|between:8,30'
            ]);
        }

        User::where('id', request('user_id'))->update([
            'name'      => request('name'),
            'email'     => request('email'),
            'lastname'  => request('lastname'),
            'password'  => Hash::make(request('password')),
        ]);

        return response('User Created!', 200);
    }
}
