<?php

namespace App\Http\Controllers;

use App\Issue;
use App\Period;
use App\Client;
use App\Mail\IssueMail;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class IssueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Period::where('id', $id)->update(['issues_status'=> request('issues_status')]);
        $period = Period::find($id);

        $message = [];
        $message['client'] = Client::find($period->client_id)->name;
        $message['issues'] = Issue::where('client_id', $period->client_id)->get();
        $message['status'] = request('issues_status');
        $message['date'] = date('Y-m-d H:i:s');
        \Mail::to(config('dashboard.emails'))->send(new IssueMail($message));

        if(request('issues_status') == 'fix'){
            $response = 'The issues will be fixed, we will be in touch with you, thank you!';
        }else{
            $response = 'Your information has been updated.';
        }

        return response($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateAdmin($id)
    {
        return Issue::where('id', $id)->update(['status'=> 1]);
    }

    public function getIssues($period_id)
    {
        $period = Period::where('id', $period_id)->first();
        $issues = Issue::where("client_id", $period->client_id)->where("status", 0)->get();
        return response($issues, 200);
    }

}
