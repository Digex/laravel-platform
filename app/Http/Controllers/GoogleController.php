<?php

namespace App\Http\Controllers;

use Analytics;
use App\Metric;
use App\Traffic;
use Carbon\Carbon;
use App\Period;
use App\Client;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Library\GoogleAnalytics;

class GoogleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $googleAnalytics = new GoogleAnalytics(request('client_id'), request('start'), request('end'));
        $website['metrics'] = $googleAnalytics->getMetrics();
        $website['traffic'] = $googleAnalytics->getTraffic();

        return response($website, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('google/create'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        Metric::updateOrCreate(['period_id' => request('period_id')], [
            'new_visitors'          => request('metrics')['new_visitors'],
            'returning_visitors'    => request('metrics')['returning_visitors'],
            'day_1'                 => Carbon::createFromFormat('F d, Y', request('metrics')['day_1'])->format('Y-m-d'),
            'day_1_visits'          => request('metrics')['day_1_visits'],
            'day_2'                 => Carbon::createFromFormat('F d, Y', request('metrics')['day_2'])->format('Y-m-d'),
            'day_2_visits'          => request('metrics')['day_2_visits'],
            'day_3'                 => Carbon::createFromFormat('F d, Y', request('metrics')['day_3'])->format('Y-m-d'),
            'day_3_visits'          => request('metrics')['day_3_visits'],
            'unique'                => request('metrics')['unique'],
            'unique_percentage'     => request('metrics')['unique_percentage'],
            'total'                 => request('metrics')['total'],
            'total_percentage'      => request('metrics')['total_percentage'],
            'conversion'            => request('metrics')['conversion'],
            'conversion_percentage' => request('metrics')['conversion_percentage'],
            'avg_pages'             => request('metrics')['avg_pages'],
            'avg_time'              => request('metrics')['avg_time']
        ]);

        Traffic::updateOrCreate(['period_id' => request('period_id')], [
            'page_1_link'       => request('traffic')['page_1_link'],
            'page_1_link_views' => request('traffic')['page_1_link_views'],
            'page_2_link'       => request('traffic')['page_2_link'],
            'page_2_link_views' => request('traffic')['page_2_link_views'],
            'page_3_link'       => request('traffic')['page_3_link'],
            'page_3_link_views' => request('traffic')['page_3_link_views'],
            'page_4_link'       => request('traffic')['page_4_link'],
            'page_4_link_views' => request('traffic')['page_4_link_views'],
            'page_5_link'       => request('traffic')['page_5_link'],
            'page_5_link_views' => request('traffic')['page_5_link_views'],
            'android'           => request('traffic')['android'],
            'ios'               => request('traffic')['ios'],
            'windows'           => request('traffic')['windows'],
            'mac'               => request('traffic')['mac'],
            'linux'             => request('traffic')['linux'],
            'os_other'          => request('traffic')['os_other'],
            'chrome'            => request('traffic')['chrome'],
            'firefox'           => request('traffic')['firefox'],
            'explorer'          => request('traffic')['explorer'],
            'edge'              => request('traffic')['edge'],
            'safari'            => request('traffic')['safari'],
            'browser_other'     => request('traffic')['browser_other'],
            'desktop'           => request('traffic')['desktop'],
            'mobile'            => request('traffic')['mobile'],
            'tablet'            => request('traffic')['tablet']
        ]);

        return response('Created', 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }




}
