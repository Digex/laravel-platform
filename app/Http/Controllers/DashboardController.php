<?php

namespace App\Http\Controllers;

use App\Code;
use App\Issue;
use App\Server;
use App\Period;
use App\Client;
use App\Agency;
use App\Status;
use App\Traffic;
use App\Metric;
use App\Comment;
use App\Security;
use Carbon\Carbon;
use App\Mail\GtmMail;
use App\Mail\IssueMail;
use App\Mail\CommentMail;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class DashboardController extends Controller
{   

    /*
     * Create a new controller instance, only available if the user
     * is aunthenticated.
     *
     * @return void
     */
    public function __construct()
    { 
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($subdomain)
    {   
        //dd(auth()->user());
        if (auth()->user()->hasRole('SuperAdmin')){
            return redirect('/client');
        }
        if(auth()->user()->hasRole('Admin')){
            return redirect()->route('agency.clients', ['subdomain' => $subdomain]);      
        }
        
        $client = null;
        $agency = null;

        if(auth()->user()->client_id){
          $clientId = (auth()->user()->client_id);  
          $client = Client::find($clientId);
        }

        if(auth()->user()->agency_id){
          $agencyId = (auth()->user()->agency_id); 
          $agency = Agency::find($agencyId);          
        }
        
        $userName = auth()->user()->name;
        
        //dd($agency->subdomain);
        if($subdomain != $client->subdomain){
            return redirect()->route('dashboard', ['subdomain' => $client->subdomain]);
        }
        else{

          $periodId = Period::where('client_id', $clientId)->orderBy('id', 'desc')->first()->id;
          return view('dashboard/index', compact('clientId', 'periodId', 'client', 'userName', 'agency') );
        }



        

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard_status(Request $request)
    { 
        
        $period_id = request('periodId');
        $status = Status::where("period_id", $period_id)->orderBy("id", "desc")->first();
        $status['updated'] = Carbon::createFromFormat('Y-m-d', $status['updated'])->format('M d, Y');
        return response($status, 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard_plugin(Request $request)
    {


        $period_id = request('periodId');
        $period = Period::find($period_id);
        
        $plugin_period = [];
        foreach ($period->plugins as $plugin) {
            $plugin_period[] = $plugin;
        }

        return $plugin_period;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard_server(Request $request)
    {
        //
      $period_id = request('periodId');
      $server = Server::where("period_id", $period_id)->orderBy("id", "desc")->first();
      $server['backup']     = Carbon::createFromFormat('Y-m-d', $server['backup'])->format('M d, Y');
      $server['last_down']  = Carbon::createFromFormat('Y-m-d', $server['last_down'])->format('M d, Y');
      return response($server, 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard_security(Request $request)
    {
        $period_id = request('periodId');
        $security = Security::where("period_id", $period_id)->orderBy("id", "desc")->first();
        if($security['expiration']){
          $security['expiration'] = Carbon::createFromFormat('Y-m-d', $security['expiration'])->format('M d, Y');  
        }
        
        $security['attacks'] = number_format($security['attacks']);
        return response($security, 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard_code(Request $request)
    {
        $period_id = request('periodId');
        $codes = Code::where("period_id", $period_id)->orderBy("id", "desc")->first();
        $codes['alt_text_format'] = number_format($codes['alt_text']);
        $codes['title_tags_format'] = number_format($codes['title_tags']);
        $codes['meta_description_format'] = number_format($codes['meta_description']);
        return $codes; 
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard_issue(Request $request)
    {
        
        $issues_period['issues'] = Issue::where([
                                            ['client_id', '=', request('clientId')],
                                            ['status', '=', 0]
                                        ])->orderBy('id', 'desc')->get();
        $issues_period['period'] = Period::where("id", request('periodId'))->orderBy('id', 'desc')->first();
        
        return response($issues_period, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function dashboard_update_issue(Request $request, $id)
    {
        Period::where('id', $id)->update(['issues_status'=> request('issues_status')]);
        $period = Period::find($id);

        $message = [];
        $message['client'] = Client::find($period->client_id)->name;
        $message['issues'] = Issue::where('client_id', $period->client_id)->get();
        $message['status'] = request('issues_status');
        $message['date'] = date('Y-m-d H:i:s');

        //\Mail::to(config('dashboard.emails'))->send(new IssueMail($message));

        if(request('issues_status') == 'fix'){
            $response = 'The issues will be fixed, we will be in touch with you, thank you!';
        }else{
            $response = 'Your information has been updated.';
        }

        return response($response, 200);
    }

    public function dashboard_authorize_gtm(Request $request)
    {
        $response = 'We will insert an additional Google Tag Manager code on your website. Thank you!';

        $message = [];
        $message['client'] = Client::find(request('client_id'))->name;
        $message['date'] = date('Y-m-d H:i:s');

        \Mail::to(config('dashboard.emails'))->send(new GtmMail($message));

        return response($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function dashboard_store_comment(Request $request)
    {
        $this->validate(request(), [
            "comment" => 'required',
        ]);

        Comment::Create([
          "message" => request('comment'),
          "client_id" =>request('client_id'),
          "date" => date("Y-m-d H:i:s"),
      ]);

        $message = [];
        $message['client'] = Client::find(request('client_id'))->name;
        $message['comment'] = request('comment');
        $message['date'] = date('Y-m-d H:i:s');

        \Mail::to(config('dashboard.emails'))->send(new CommentMail($message));

        return response ('Your comment has been sent, Thank you!', 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard_google(Request $request)
    {   
        $period = Period::find(request('period_id'));
        $analytics['client']    = Client::find(request('client_id'));
        $site                   = $analytics['client']['site'];
        $siteUrl                = $this->cleanUrl($site);
        $analytics['traffic']   =  Traffic::where('period_id', $period->id)->orderBy('id', 'desc')->first();
        $analytics['metrics']   =  Metric::where('period_id', $period->id)->orderBy('id', 'desc')->first();
        if($analytics['metrics'] && $analytics['traffic']){
            $analytics['metrics']['day_top'] = Carbon::createFromFormat('Y-m-d', $analytics['metrics']['day_1'])->format('d');
            $analytics['metrics']['month_top'] = Carbon::createFromFormat('Y-m-d', $analytics['metrics']['day_1'])->format('M');
            $analytics['metrics']['day_1']   = Carbon::createFromFormat('Y-m-d', $analytics['metrics']['day_1'])->format('l, M, d');
            $analytics['metrics']['day_2']   = Carbon::createFromFormat('Y-m-d', $analytics['metrics']['day_2'])->format('l, M, d');
            $analytics['metrics']['day_3']   = Carbon::createFromFormat('Y-m-d', $analytics['metrics']['day_3'])->format('l, M, d');
            $analytics['metrics']['unique']  = number_format($analytics['metrics']['unique']);
            $analytics['metrics']['total']   = number_format($analytics['metrics']['total']);
            $analytics['metrics']['conversion']  = number_format($analytics['metrics']['conversion']);
            $analytics['traffic']['page_1_url']  = ($analytics['traffic']['page_1_link'] == '/') ? $site : $site.$analytics['traffic']['page_1_link'];
            $analytics['traffic']['page_2_url']  = ($analytics['traffic']['page_2_link'] == '/') ? $site : $site.$analytics['traffic']['page_2_link'];
            $analytics['traffic']['page_3_url']  = ($analytics['traffic']['page_3_link'] == '/') ? $site : $site.$analytics['traffic']['page_3_link'];
            $analytics['traffic']['page_4_url']  = ($analytics['traffic']['page_4_link'] == '/') ? $site : $site.$analytics['traffic']['page_4_link'];
            $analytics['traffic']['page_5_url']  = ($analytics['traffic']['page_5_link'] == '/') ? $site : $site.$analytics['traffic']['page_5_link'];
            $analytics['traffic']['page_1_link'] = ($analytics['traffic']['page_1_link'] == '/') ? $siteUrl : $analytics['traffic']['page_1_link'];
            $analytics['traffic']['page_2_link'] = ($analytics['traffic']['page_2_link'] == '/') ? $siteUrl : $analytics['traffic']['page_2_link'];
            $analytics['traffic']['page_3_link'] = ($analytics['traffic']['page_3_link'] == '/') ? $siteUrl : $analytics['traffic']['page_3_link'];
            $analytics['traffic']['page_4_link'] = ($analytics['traffic']['page_4_link'] == '/') ? $siteUrl : $analytics['traffic']['page_4_link'];
            $analytics['traffic']['page_5_link'] = ($analytics['traffic']['page_5_link'] == '/') ? $siteUrl : $analytics['traffic']['page_5_link'];
            $analytics['traffic']['page_1_link_views'] = number_format($analytics['traffic']['page_1_link_views']);
            $analytics['traffic']['page_2_link_views'] = number_format($analytics['traffic']['page_2_link_views']);
            $analytics['traffic']['page_3_link_views'] = number_format($analytics['traffic']['page_3_link_views']);
            $analytics['traffic']['page_4_link_views'] = number_format($analytics['traffic']['page_4_link_views']);
            $analytics['traffic']['page_5_link_views'] = number_format($analytics['traffic']['page_5_link_views']);
        }

        return response($analytics, 200);
    }

    public function cleanUrl($siteUrl)
    {
        $input = $siteUrl;
        if (!preg_match('#^http(s)?://#', $input)) {
            $input = 'http://' . $input;
        }
        $urlParts = parse_url($input);
        return preg_replace('/^www\./', '', $urlParts['host']);
    }


   
}
