<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Client;
use App\Agency;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Foundation\Auth\ResetsPasswords;

class ResetPasswordClientController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Display the password reset view for the given token.
     *
     * If no token is present, display the link request form.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $token
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showResetForm(Request $request, $subdomain = 'root', $token = null)
    {
        $client = Client::where('subdomain', $subdomain)->first();
        $agency = Agency::find($client->agency_id);
        /*return view('auth.passwords.reset')->with(
            ['subdomain' => $subdomain, 'token' => $token, 'email' => $request->email, 'client' => $client, 'agency' => $agency]
        );*/
        return view('auth.passwords.reset', compact('subdomain', 'token', 'email', 'client', 'agency'));
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function reset(Request $request)
    {
        $this->validate($request, $this->rules(), $this->validationErrorMessages());

        $passwordReset = $this->getRequestedToken($request->email);

        if($passwordReset && $passwordReset->token == $request->token){
            $this->savePassword($request->email, $request->password);
            $credentials = $request->only('email', 'password');
            $this->guard()->attempt($credentials);
            return redirect()->route('dashboard', ['subdomain' => $request->subdomain]);
        }else{
            return redirect()->route('password.reset', ['subdomain' => $request->subdomain, 'token' => $request->token])->with(['error' => 'Invalid link for password reset.']);
        }

    }

    public function savePassword($email, $password)
    {
        User::where('email', $email)
            ->update([
                'password' => Hash::make($password)
            ]);
    }

    public function getRequestedToken($email)
    {
        return DB::table('password_resets')
                   ->select('email', 'token')
                   ->where('email', $email)
                   ->first();
    }

    /**
     * Get the password reset validation rules.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'token' => 'required',
            'email' => 'required|email|exists:users,email',
            'password' => 'required|confirmed|min:6',
        ];
    }

    /**
     * Get the password reset validation error messages.
     *
     * @return array
     */
    protected function validationErrorMessages()
    {
        return [];
    }

    /**
     * Get the guard to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }
}
