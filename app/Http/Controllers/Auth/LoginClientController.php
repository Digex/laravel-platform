<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Client;
use App\Agency;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LoginClientController extends Controller
{

    public $errorMessage = 'Username or password is incorrect.';

    /**
     * Create a new controller instance, only the logout function
     * needs an authenticated user.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Show the login page for a given subdomain, if no subdomain 
     * is given, the redirect will send the user to the default login
     * page with a subdomain of 'root.
     * 
     * @param  string $subdomain
     * @return view
     */
    public function show($subdomain = 'root')
    {
        $client = ($subdomain == 'root') ? Client::find(1) : Client::where('subdomain', $subdomain)->first();
        $agency = ($client) ? Agency::find($client->agency_id) : Agency::where('subdomain', $subdomain)->first();
        
        if(!$client && !$agency){
          return redirect()->route('login');  
        } 
        
        return view('auth/login', compact('subdomain', 'client', 'agency'));
    }

    /**
     * Handle an authentication attempt, if the user is athenticated 
     * correctly will be redirected to the dashboard view, otherwise
     * will be redirected again to the login page (depending on the 
     * subdomain) with an error message.
     *
     * @param  \Illuminate\Http\Request $request
     * @return view
     */
    public function authenticate(Request $request)
    {
        
        $credentials = $request->only('email', 'password');

        if ($this->guard()->attempt($credentials, $request->filled('remember'))) {  
            // Authentication passed...
            $user = User::where('email', request('email'))->first();


            if ($user->hasRole('SuperAdmin')){
                return redirect('/client');
            }
            else{
                if($user->hasRole('Admin')){
                    return redirect()->route('agency.clients', ['subdomain' => $request->subdomain]);      
                }
                else{
                    return redirect()->route('dashboard', ['subdomain' => $request->subdomain]);
                }
            }
            
        }
        
        // Error
        if($request->subdomain == 'root'){
            return redirect()->route('login')->with(['error' => $this->errorMessage]);
        }else{
            return redirect()->route('client.login', ['subdomain' => $request->subdomain])->with(['error' => $this->errorMessage]);
        }
    }

    /**
     * Log the user out of the application and redirect to the correct
     * login page.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return view
     */
    public function logout(Request $request)
    {
        $user = User::find(auth()->user()->id);
        $agency = Agency::find($user->agency_id);

        $this->guard()->logout();
        $request->session()->invalidate();

        if($user->hasRole('SuperAdmin')){
            $subdomain = $agency->subdomain;
        }
        else{
          if($user->hasRole('Admin') ){
            $subdomain = $agency->subdomain;
          }
          else{
            $subdomain = $user->client->subdomain;
          } 
        }

        return redirect()->route('client.login', ['subdomain' => $subdomain]);
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }
}