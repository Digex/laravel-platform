<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Client;
use App\Agency;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Mail\ResetPasswordMail;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
//use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class ForgotPasswordClientController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Display the form to request a password reset link.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLinkRequestForm($subdomain = 'root')
    {
        $client = ($subdomain == 'root') ? $client = Client::find(1) : $client = Client::where('subdomain', $subdomain)->first();
        if(!$client) return redirect()->route('login');
        $agency = Agency::find($client->agency_id);

        return view('auth.passwords.email', compact('client', 'subdomain', 'agency'));
    }

    /**
     * Send an email with a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    public function sendResetLinkEmail(Request $request)
    {
        $this->validateEmail($request);

        $user = User::where('email', $request->email)->first();
        
        if($user){
            $status = 'success';
            $token = Password::getRepository()->create($user);
            $this->saveToken($request->email, $token);
            $link = ($request->subdomain == 'root') ? url('/password/reset/'.$token) : $link = url('/'.$request->subdomain.'/password/reset/'.$token);
            \Mail::to($request->email)->send(new ResetPasswordMail($link));
        }else{
            $status = 'error';
        }

        if($request->subdomain == 'root'){
            return redirect()->route('password.request')->with(['status' => $status]);
        }else{
            return redirect()->route('client.password.request', ['subdomain' => $request->subdomain])->with(['status' => $status]);
        }
    }

    /**
     * Save the reset token on the database for a given user, if a
     * user have previouly requested a reset token, update the token
     * otherwise insert a new token.
     * 
     * @param  string $email
     * @param  string $token
     * @return void
     */
    public function saveToken($email, $token)
    {
        $user = DB::table('password_resets')
                    ->select('email', 'token')
                    ->where('email', $email)
                    ->get();

        if($user){
            DB::table('password_resets')
                ->where('email', $email)
                ->update([
                    'token' => $token,
                    'created_at' => Carbon::now()
                ]);
        }else{
            DB::table('password_resets')->insert([
                'email' => $email,
                'token' => $token,
                'created_at' => Carbon::now()
            ]);
        }
    }

    /**
     * Validate the email for the given request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function validateEmail(Request $request)
    {
        $this->validate($request, ['email' => 'required|email']);
    }

}
