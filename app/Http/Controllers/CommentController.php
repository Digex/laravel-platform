<?php

namespace App\Http\Controllers;

use App\Client;
use App\Comment;
use App\Mail\CommentMail;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            "comment" => 'required',
        ]);

        Comment::Create([
          "message" => request('comment'),
          "client_id" =>request('client_id'),
          "date" => date("Y-m-d H:i:s"),
      ]);

        $message = [];
        $message['client'] = Client::find(request('client_id'))->name;
        $message['comment'] = request('comment');
        $message['date'] = date('Y-m-d H:i:s');

        \Mail::to(config('dashboard.emails'))->send(new CommentMail($message));

        return response ('Your comment has been sent, Thank you!', 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
