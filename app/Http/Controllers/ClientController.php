<?php

namespace App\Http\Controllers;

use App\User;
use App\Period;
use App\Client;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Intervention\Image\Facades\Image;


class ClientController extends Controller
{
        
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('client/index');
    }

    public function information()
    { 

        $clients = Client::orderBy("name", "asc")->get();

        $filteredClients = $clients->whereNotIn('id', [1]);
        
        $filteredClients->map(function ($client) {
            $client['users']    = $client->users()->count();
            $client['reports']  = $client->periods()->count();
            return $client;
        });
        
        return response($filteredClients, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('client/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        


        $this->validate(request(), [
            "name" => 'required',
            'subdomain' => 'required|unique:agencies|unique:clients',
            "logo" => array('required', 'regex:/^data:image\/(png|jpeg|jpg|bmp)/'),
            "color1" => 'required',
            "color2" => 'required',
            "site" => 'required|url',
            "agency" => 'required',
        ]);

        $primary_color    = str_replace('#', '', request('color1'));
        $secondary_color  = str_replace('#', '', request('color2'));

        $client = Client::Create([
            "name" => request('name'),
            "subdomain" => request('subdomain'),
            "logo" => '',
            "primary_color" => $primary_color,
            "secondary_color" =>  $secondary_color,
            "google_view_id" => request('view_id'),
            "site" => request('site'),
            "agency_id" => request('agency'),
        ]);

        //$imageData = $request->get('image');
        $imageData = request('logo');
        $fileName = 'client_' . $client->id .'.' . explode('/', explode(':', substr($imageData, 0, strpos($imageData, ';')))[1])[1];
        Image::make(request('logo'))->save(public_path('uploads/').$fileName);
        
        Client::where('id', $client->id)->update(['logo'=> $fileName]);


        return ['message' => 'Project Created!'];

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('client/show', compact('id'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getClient($id)
    {   
        $answer['client'] = Client::find($id); 
        //$logo = file_get_contents();
        $answer['logo'] = (string) Image::make(public_path('uploads/').$answer['client']->logo)->encode('data-url');
    
        return response($answer, 200);
    }

    public function getClientName($id)
    {
        return response(Client::find($id)->name, 200);
    }

    public function getAgencyClientName($subdomain, $id)
    {
        return response(Client::find($id)->name, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      return view('client/edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate(request(), [
            "name" => 'required',
            'subdomain' => 'required',
            "logo" => 'required',
            "color1" => 'required',
            "color2" => 'required',
            "site" => 'required|url',
            "agency" => 'required',
        ]);

        $primary_color    = str_replace('#', '', request('color1'));
        $secondary_color  = str_replace('#', '', request('color2'));

        Client::where('id', request('client_id'))->update([
          'name' => request('name'),
          'subdomain' => request('subdomain'),
          'site' => request('site'),
          'primary_color' => $primary_color,
          'secondary_color' => $secondary_color,
          'google_view_id' => request('view_id'),
          'agency_id' => request('agency'),
        ]);

        $imageData = request('logo');
        $fileName = 'client_' . request('client_id') .'.' . explode('/', explode(':', substr($imageData, 0, strpos($imageData, ';')))[1])[1];
        Image::make(request('logo'))->save(public_path('uploads/').$fileName);
        
        Client::where('id', request('client_id'))->update(['logo'=> $fileName]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client = Client::find($id);
        $client->users()->delete();
        $client->delete();
        $agency = $client->agency_id;
        return response($agency, 200);
    }

    /**
     * Get client information for specific agency
     *
     * @param  int  $agency_id
     * @return \Illuminate\Http\Response
     */
    public function informationAgency($agency_id)
    { 
        
        $clients = Client::where("agency_id", $agency_id)->orderBy("name", "asc")->get();

        //$filteredClients = $clients->whereNotIn('id', [1]);
        
        $clients->map(function ($client) {
            $client['users']    = $client->users()->count();
            $client['reports']  = $client->periods()->count();
            return $client;
        });
        
        return response($clients, 200);
    }

    /**
     * Get all the clients
     *
     * @return \Illuminate\Http\Response
     */
    public function getClients()
    {
        //
        return Client::all();

        
    }
}
