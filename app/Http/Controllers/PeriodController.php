<?php

namespace App\Http\Controllers;


use App\Code;
use App\Issue;
use App\Agency;
use App\Period;
use App\Plugin;
use App\Security;
use App\Server;
use App\Status;
use App\Client;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Intervention\Image\Facades\Image;

class PeriodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {   
        
        return view('report/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        //
        $client_id = $id;

        $rules =[ "plugins.*.version"  => "required",];

        $customMessages = [ 'required' => 'The version of the plugin field is required.'];

        $this->validate(request(), $rules, $customMessages );
         
            
        $this->validate(request(), [
            "start_date" => 'required',
            "end_date" => 'required',
            "plugins"    => "required",

            "core" => 'required',
            "last_update" => 'required',

            "backup" => 'required',
            "load_time" => 'required',
            "media" => 'required',
            "uptime" => 'required',
            "host" => 'required',
            "cdn" => 'required',
            "files" => 'required',
            "last_down" => 'required',

            "attacks" => 'required',
            //"certified" => 'required',
            //"expiration" => 'required',
            "https" => 'required',
            /*"country_1" => 'required',
            "country_1_number" => 'required',
            "country_2" => 'required',
            "country_2_number" => 'required',
            "country_3" => 'required',
            "country_3_number" => 'required',
            "country_4" => 'required',
            "country_4_number" => 'required',
            "country_5" => 'required',
            "country_5_number" => 'required',*/

            "conflicts" => 'required',
            "broken_links" => 'required',
            "mobile_friendly" => 'required',
            "alt_text" => 'required',
            "title_tags" => 'required',
            "meta_description" => 'required',



        ]);

        // PERIOD
        $start_date = substr(request('start_date'), 0,10);
        $end_date   = substr(request('end_date'), 0,10);
        $month      = substr($end_date, 5,-3);
        $year       = substr(request('start_date'), 0, 4);  

        $period = Period::Create([
          'start'=> $start_date,
          'end'=> $end_date,
          'month'=> $month,
          'year'=> $year,
          'client_id' => $client_id,
        ]);

        //STATUS
        $update = substr(request('last_update'), 0, 10);  

        Status::Create([
          'core'=> request('core'),
          'updated'=> $update,
          'period_id'=> $period->id,

        ]);

        //PLUGINS
        $plugins = request('plugins');
        $period = Period::find($period->id);
          

        for ($i=0; $i < count($plugins); $i++) { 
          $period->plugins()->attach($period->id, [
            'version' => $plugins[$i]['version'],
            'active' => $plugins[$i]['active'],
            'plugin_id' => $plugins[$i]['id'],
            'created_at' => date('Y-m-d H:i:s'),
          ]);
        }


        
        
        //SERVER
        $last_down = substr(request('last_down'), 0,10);
        $backup = substr(request('backup'), 0,10);


        Server::Create([
          'backup'=> $backup,
          'host'=> request('host'),
          'load_time'=> request('load_time'),
          'delivery'=> request('cdn'),
          'media_items'=> request('media'),
          'storage'=> request('files'),
          'uptime'=> request('uptime'),
          'last_down'=> $last_down,
          'period_id'=> $period->id,

        ]);

        

        //SECURITY
        $expiration = substr(request('expiration'), 0,10);
        $expiration = ($expiration != "") ? $expiration : null;

        Security::Create([
          'https' => request('https'),
          'certified' => request('certified'),
          'attacks' => request('attacks'),
          'expiration' => $expiration,
          'country_1' => request('country_1'),
          'country_1_number' => request('country_1_number'),
          'country_2' => request('country_2'),
          'country_2_number' => request('country_2_number'),
          'country_3' => request('country_3'),
          'country_3_number' => request('country_3_number'),
          'country_4' => request('country_4'),
          'country_4_number' => request('country_4_number'),
          'country_5' => request('country_5'),
          'country_5_number' => request('country_5_number'),
          'period_id' => $period->id,

        ]);

       

        //CODE
        Code::Create([
          'conflicts' => request('conflicts'),
          'broken_links' => request('broken_links'),
          'mobile_friendly' => request('mobile_friendly'),
          'alt_text' => request('alt_text'),
          'title_tags' => request('title_tags'),
          'meta_description' => request('meta_description'),
          'period_id' => $period->id,
        ]);

         
        //ISSUE
        $issues = request('issue');
        
        for ($i=0; $i < count($issues); $i++) { 
          Issue::updateOrCreate( ['id' => $issues[$i]['id']] , [
            'name' => $issues[$i]['name'],
            'status' => 0,
            'fix_time' => $issues[$i]['fix_time'],
            'fix_number' => $issues[$i]['fix_number'],
            'client_id' => $period->client_id,
          ]);
        }


        return response( $period, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($periodId)
    {
        $period = Period::find($periodId);
        $client = Client::where('id', $period->client_id)->first();
        $agency = Agency::where('id', $client->agency_id)->first();
        $period->month = Carbon::createFromFormat('n', $period->month)->format('F');
        $clientId = $client->id;
        return view('report.show', compact('client', 'period', 'clientId', 'periodId', 'agency'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $period = Period::find($id);
        $client = Client::find($period->client_id);
        
        return view('report/edit', compact('period', 'client') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $period_id = $id;

        $rules =[ "plugins.*.version"  => "required",];

        $customMessages = [ 'required' => 'The version of the plugin field is required.'];

        $this->validate(request(), $rules, $customMessages );

            
        $this->validate(request(), [
            "start_date" => 'required',
            "end_date" => 'required',
            "plugins"    => "required",

            "core" => 'required',
            "last_update" => 'required',

            "backup" => 'required',
            "load_time" => 'required',
            "media" => 'required',
            "uptime" => 'required',
            "host" => 'required',
            "cdn" => 'required',
            "files" => 'required',
            "last_down" => 'required',

            "attacks" => 'required',
            //"certified" => 'required',
            //"expiration" => 'required',
            "https" => 'required',
            /*"country_1" => 'required',
            "country_1_number" => 'required',
            "country_2" => 'required',
            "country_2_number" => 'required',
            "country_3" => 'required',
            "country_3_number" => 'required',
            "country_4" => 'required',
            "country_4_number" => 'required',
            "country_5" => 'required',
            "country_5_number" => 'required',*/

            "conflicts" => 'required',
            "broken_links" => 'required',
            "mobile_friendly" => 'required',
            "alt_text" => 'required',
            "title_tags" => 'required',
            "meta_description" => 'required',

        ]);

        $rules_issues = [
           "issue.*.name" => 'required_with:issue',
           "issue.*.fix_number" => 'required_with:issue',
           "issue.*.fix_time" => 'required_with:issue', 
        ];

        $issuesMessages = [ 'required_with' => 'This field is required.'];
        
        $this->validate(request(), $rules_issues, $issuesMessages ); 

        // PERIOD
        $start_date = substr(request('start_date'), 0,10);
        $end_date   = substr(request('end_date'), 0,10);
        $month      = substr($end_date, 5,-3);
        $year       = substr(request('start_date'), 0, 4);  

        Period::where('id', $period_id)->update([
          'start'=> $start_date,
          'end'=> $end_date,
          'month'=> $month,
          'year'=> $year,
        ]);

        //STATUS
        $update       = substr(request('last_update'), 0, 10);  

        Status::where('period_id', $period_id)->update([
          'core'=> request('core'),
          'updated'=> $update,
        ]);

        //PLUGINS
        $plugins = request('plugins');
        $period = Period::find($period_id);
         
        $period->plugins()->detach();

        for ($i=0; $i < count($plugins); $i++) { 
          $period->plugins()->attach($period_id, [
            'version' => $plugins[$i]['version'],
            'active' => $plugins[$i]['active'],
            'plugin_id' => $plugins[$i]['id'],
            'created_at' => date('Y-m-d H:i:s'),
          ]);
        }
        
        //SERVER
        $last_down = substr(request('last_down'), 0,10);
        $backup = substr(request('backup'), 0,10);

        Server::where('period_id', $period_id)->update([
          'backup'=> $backup,
          'host'=> request('host'),
          'load_time'=> request('load_time'),
          'delivery'=> request('cdn'),
          'media_items'=> request('media'),
          'storage'=> request('files'),
          'uptime'=> request('uptime'),
          'last_down'=> $last_down,

        ]);

        //SECURITY
        $expiration = substr(request('expiration'), 0,10);
        $expiration = ($expiration != "") ? $expiration : null;

        Security::where('period_id', $period_id)->update([
          'https' => request('https'),
          'certified' => request('certified'),
          'attacks' => request('attacks'),
          'expiration' => $expiration,
          'country_1' => request('country_1'),
          'country_1_number' => request('country_1_number'),
          'country_2' => request('country_2'),
          'country_2_number' => request('country_2_number'),
          'country_3' => request('country_3'),
          'country_3_number' => request('country_3_number'),
          'country_4' => request('country_4'),
          'country_4_number' => request('country_4_number'),
          'country_5' => request('country_5'),
          'country_5_number' => request('country_5_number'),

        ]);

        //CODE
        Code::where('period_id', $period_id)->update([
          'conflicts' => request('conflicts'),
          'broken_links' => request('broken_links'),
          'mobile_friendly' => request('mobile_friendly'),
          'alt_text' => request('alt_text'),
          'title_tags' => request('title_tags'),
          'meta_description' => request('meta_description'),
        ]);

        
        //ISSUE
        $issues = request('issue');
        
        for ($i=0; $i < count($issues); $i++) { 
          Issue::updateOrCreate( ['id' => $issues[$i]['id']] , [
            'name' => $issues[$i]['name'],
            'status' => 0,
            'fix_time' => $issues[$i]['fix_time'],
            'fix_number' => $issues[$i]['fix_number'],
            'client_id' => $period->client_id,
          ]);
        }


        return response( $period, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Period::destroy($id);
        return response(null, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getPeriod($period_id)
    {
        //here needs to get the last report
        $period['period'] = Period::find($period_id);
        $period['status'] = Status::where("period_id", $period_id)->orderBy("id", "desc")->first();
        $period['server'] = Server::where("period_id", $period_id)->orderBy("id", "desc")->first();
        $period['security'] = Security::where("period_id", $period_id)->orderBy("id", "desc")->first();
        $period['code'] = Code::where("period_id", $period_id)->orderBy("id", "desc")->first();
        $period['plugins'] = Plugin::orderBy('name', 'asc')->get();
        $period['plugins_period'] = Period::find($period_id)->plugins()->orderBy('name', 'asc')->get();
        $period['issues'] = Issue::where("client_id", $period['period']->client_id)->where("status", 0)->orderBy("id", "desc")->get();

        return response($period, 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getLastPeriod($client_id)
    {
        //here needs to get the last report
        $period = [];
        $lastPeriod = Period::where("client_id", $client_id)->orderBy("id", "desc")->first();
        
        if($lastPeriod){
          $period['period'] = Period::find($lastPeriod->id);
          $period['status'] = Status::where("period_id", $lastPeriod->id)->orderBy("id", "desc")->first();
          $period['server'] = Server::where("period_id", $lastPeriod->id)->orderBy("id", "desc")->first();
          $period['security'] = Security::where("period_id", $lastPeriod->id)->orderBy("id", "desc")->first();
          $period['code'] = Code::where("period_id", $lastPeriod->id)->orderBy("id", "desc")->first();
          $period['plugins'] = Plugin::orderBy("name", "asc")->get();
          $period['plugins_period'] = Period::find($lastPeriod->id)->plugins()->orderBy('name', "asc")->get();
          $period['issues'] = Issue::where("client_id", $period['period']->client_id)->where("status", 0)->orderBy("id", "desc")->get();
        }



        return response($period, 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getClient($period_id)
    {   
        $period = Period::find($period_id);
        $answer['client'] = Client::where("id", $period->client_id)->orderBy("id", "desc")->first();
        //$logo = file_get_contents();
        $answer['logo'] = (string) Image::make(public_path('uploads/').$answer['client']->logo)->encode('data-url');
        return response($answer, 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showReportAgency($subdomain, $periodId)
    {
        $period = Period::find($periodId);
        $client = Client::where('id', $period->client_id)->first();
        $agency = Agency::where('id', $client->agency_id)->first();
        $period->month = Carbon::createFromFormat('n', $period->month)->format('F');
        $clientId = $client->id;
        return view('agency.show', compact('client', 'period', 'clientId', 'periodId', 'agency', 'subdomain'));
    }

    /**
     *Reset the issues field to show.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function reset($periodId)
    {
        $period = Period::where("id", $periodId)->update(["issues_status" => "show"]);
        return response($periodId, 200);
    }
}
