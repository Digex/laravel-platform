<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

use App\Client;
use App\Agency;
use App\Period;
use App\User;

class AgencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return Agency::all()->toArray();

        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('agency/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //dd($request);
        $this->validate(request(), [
            'name' => 'required',
            'subdomain' => 'required|unique:agencies|unique:clients',
            'file' => 'required',
            'address' => 'required',
            'site' => 'required|url'
        ]);

        $agency = Agency::Create([
            'name' => request('name'),
            'subdomain' => request('subdomain'),
            'logo' => '',
            'address' => request('address'),
            'site' => request('site'),
        ]);

        $imageData = request('file');
        $fileName = 'agency_' . $agency->id . '.' . explode('/', explode(':', substr($imageData, 0, strpos($imageData, ';')))[1])[1];
        Image::make(request('file'))->save(public_path('uploads/').$fileName);

        Agency::where('id', $agency->id)->update(['logo'=> $fileName]);

        return response($agency, 200);
        


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('agency/users', compact('id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      return view('agency/edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request);
        $this->validate(request(), [
            'name' => 'required',
            'subdomain' => 'required',
            'logo' => 'required',
            'address' => 'required',
            'site' => 'required|url'
        ]);

        Agency::where('id', $id)->update([
            'name' => request('name'),
            'subdomain' => request('subdomain'),
            'logo' => '',
            'address' => request('address'),
            'site' => request('site'),
        ]);

        $imageData = request('logo');
        $fileName = 'agency_' . $id . '.' . explode('/', explode(':', substr($imageData, 0, strpos($imageData, ';')))[1])[1];
        Image::make(request('logo'))->save(public_path('uploads/').$fileName);

        Agency::where('id', $id)->update(['logo'=> $fileName]);

        return response(null, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $agency = Agency::find($id);
        $agency->clients()->delete();
        $agency->users()->delete();
        $agency->delete();
        return response(null, 200);
    }

    /**
     * Show specific clients for each agency.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showClients($subdomain)
    {
        
        $clients =  Client::where("agency_id", auth()->user()->agency_id)->get();
        $agency  =  Agency::where("id", auth()->user()->agency_id)->first();

        if($subdomain != $agency->subdomain){
            return redirect()->route('agency.clients', ['subdomain' => $agency->subdomain]);
        }
        
        return view('agency/clients', compact('clients', 'period') );
    }

    public function information()
    { 

        $agencies = Agency::orderBy("name", "asc")->get();

        //$filteredClients = $clients->whereNotIn('id', [1]);
        
        $agencies->map(function ($agency) {
            $agency['users']    = $agency->users()->where("client_id", null)->count();
            $agency['clients']  = $agency->clients()->count();
            return $agency;
        });
        
        return response($agencies, 200);
    }

    /**
     * return agency based on the id
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getAgency($id)
    {
        $agency = Agency::find($id); 
        //$logo = file_get_contents();
        $agency->logo = (string) Image::make(public_path('uploads/').$agency->logo)->encode('data-url');
        
        return response($agency, 200);
    }

    /**
     * return name agency based on the id
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getAgencyName($id)
    {
        return response(Agency::find($id)->name, 200);
    }

    /**
     * return users by agency based on the id
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getUsers($id)
    {
        return response(User::where("agency_id", $id)->where("client_id", null)->get(), 200);
    }
}
