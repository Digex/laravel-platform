<?php

namespace App\Http\Controllers;

use App\Code;
use App\Comment;
use App\Issue;
use App\Metric;
use App\Period;
use App\Plugin;
use App\Security;
use App\Server;
use App\Status;
use App\Traffic;
use App\Client;
use Carbon\Carbon;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($clientId)
    {
        return view('report.list', compact('clientId'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($client_id)
    {
        $client = Client::find($client_id);
        return view('report/create', ["client" => $client]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($client_id)
    {
        //here needs to get the last report

        $client = Client::find($client_id);
        $period = Period::where("client_id", $client_id)->orderBy("id", "desc")->first();
        $periodId = isset($period->id) ? $period->id : 0;
        return view('report/index', ["client" => $client, "clientId" => $client->id, "periodId" =>  $periodId]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_period(Request $request)
    {

        $this->validate(request(), [
            "start_date" => 'required',
            "end_date" => 'required',
        ]);

        $start_date = substr(request('start_date'), 0,10);
        $end_date   = substr(request('end_date'), 0,10);
        $month      = substr(request('start_date'), 5,-17);
        $year       = substr(request('start_date'), 0, 4);  
        $id         = request('client_id');
        
        $period = Period::Create([
            "start" => $start_date,
            "end" =>$end_date,
            "month" => $month,
            "year" => $year,
            "client_id" => $id,
        ]);

        
        return ['period' => $period, 'client_id' => $id];

    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function getPeriods($clientId)
    {  

       $periods = Period::where('client_id', $clientId)->orderBy('id', 'desc')->get();
        $periods->each(function ($period) {
            $period->month = Carbon::createFromFormat('!n', $period->month)->format('F');
        });
        return response($periods, 200);

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function showReportsAgency($subdomain, $clientId)
    { 
        return view('agency.list', compact('subdomain','clientId'));
    }
}
