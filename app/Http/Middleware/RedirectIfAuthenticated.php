<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {   
        if (Auth::guard($guard)->check()) {

            if(Auth::user()->hasRole('SuperAdmin')){
              return redirect('client');  
            }
            else{
              if(Auth::user()->hasRole('Admin') ){
                return redirect()->route('agency.clients', ['subdomain' => $request->subdomain]);  
              }
              else{
                return redirect('/home');   
              } 
            }
        
        }


        return $next($request);
    }
}
