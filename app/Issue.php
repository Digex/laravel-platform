<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Issue extends Model
{
    protected $fillable = [
        'name', 'status', 'fix_time', 'fix_date', 'client_id', 'fix_number'
    ];
}
