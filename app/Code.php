<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Code extends Model
{
    //
    protected $fillable = [
        'conflicts', 'broken_links', 'mobile_friendly', 'alt_text', 'title_tags', 'meta_description', 'period_id'
    ];
}
