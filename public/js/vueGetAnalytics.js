/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 568);
/******/ })
/************************************************************************/
/******/ ({

/***/ 568:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(569);


/***/ }),

/***/ 569:
/***/ (function(module, exports) {

new Vue({
    el: '#appGoogleAnalytics',

    data: {
        selectedClient: '',
        selectedPeriod: '',
        metrics: null,
        traffic: null,
        googleViewId: null,
        clients: null,
        periods: null
    },

    mounted: function mounted() {
        var _this = this;

        axios.get('/analytics/clients').then(function (response) {
            _this.clients = response.data;
        }).catch(function (error) {
            return console.log(error);
        });
    },


    methods: {
        getPeriods: function getPeriods() {
            var _this2 = this;

            this.metrics = this.traffic = null;
            axios.get('/analytics/periods', {
                params: {
                    clientId: this.selectedClient.id
                }
            }).then(function (response) {
                _this2.periods = response.data;
            }).catch(function (error) {
                return console.log(error);
            });
        },
        getAnalytics: function getAnalytics() {
            var _this3 = this;

            if (!this.selectedPeriod) {
                alert('Select a period');
                return;
            }
            axios.get('/google', {
                params: {
                    client_id: this.selectedClient.id,
                    period_id: this.selectedPeriod.id,
                    start: this.selectedPeriod.start,
                    end: this.selectedPeriod.end
                }
            }).then(function (response) {
                console.log(response.data);
                _this3.metrics = response.data.metrics;
                _this3.traffic = response.data.traffic;
            }).catch(function (error) {
                return console.log(error);
            });
        },
        storeAnalytics: function storeAnalytics() {
            var _this4 = this;

            axios.post('/google', {
                periodId: this.selectedPeriod.id,
                metrics: this.metrics,
                traffic: this.traffic
            }).then(function (response) {
                alert('Saved information for ' + _this4.selectedClient.name + ' for the period ' + _this4.selectedPeriod.start + ' ' + _this4.selectedPeriod.end);
                console.log(response.data);
            }).catch(function (error) {
                return console.log(error);
            });
        }
    }
});

/***/ })

/******/ });