/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 645);
/******/ })
/************************************************************************/
/******/ ({

/***/ 645:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(646);


/***/ }),

/***/ 646:
/***/ (function(module, exports) {


new Vue({
  el: '#appCode',

  data: {
    period_id: Dashboard.period_id,
    client_id: Dashboard.client_id,
    dashboard_code: null

  },

  mounted: function mounted() {
    var _this = this;

    this.getSecurity();

    VueBus.$on('updatePeriod', function (periodId) {
      _this.period_id = periodId;
      _this.getSecurity();
    });
  },

  methods: {
    getSecurity: function getSecurity() {
      var _this2 = this;

      axios.get('/dashboard_code', {
        params: {
          periodId: this.period_id
        }
      }).then(function (response) {
        _this2.dashboard_code = {
          conflicts: response.data.conflicts,
          broken_links: response.data.broken_links,
          mobile_friendly: response.data.mobile_friendly,
          alt_text: response.data.alt_text,
          title_tags: response.data.title_tags,
          meta_description: response.data.meta_description,
          alt_text_format: response.data.alt_text_format,
          title_tags_format: response.data.title_tags_format,
          meta_description_format: response.data.meta_description_format
        };
      }).catch(function (error) {
        console.log(error);
      });
    }
  }

});

/***/ })

/******/ });