/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 643);
/******/ })
/************************************************************************/
/******/ ({

/***/ 643:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(644);


/***/ }),

/***/ 644:
/***/ (function(module, exports) {


new Vue({
  el: '#appServer',

  data: {
    period_id: Dashboard.period_id,
    client_id: Dashboard.client_id,
    dashboard_server: null

  },

  mounted: function mounted() {
    var _this = this;

    this.getServerMetrics();

    VueBus.$on('updatePeriod', function (periodId) {
      _this.period_id = periodId;
      _this.getServerMetrics();
    });
  },

  methods: {
    getServerMetrics: function getServerMetrics() {
      var _this2 = this;

      axios.get('/dashboard_server', {
        params: {
          periodId: this.period_id
        }
      }).then(function (response) {
        _this2.dashboard_server = {
          backup: response.data.backup,
          host: response.data.host,
          load_time: response.data.load_time,
          content: response.data.delivery,
          items: response.data.media_items,
          storage: response.data.storage,
          uptime: response.data.uptime,
          last_down: response.data.last_down
        };
      }).catch(function (error) {
        console.log(error);
      });
    }
  }

});

/***/ })

/******/ });