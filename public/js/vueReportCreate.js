/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 572);
/******/ })
/************************************************************************/
/******/ ({

/***/ 128:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/*!
 * vuejs-datepicker v0.10.0
 * (c) 2016-2018 Charlie Kassel
 * Released under the MIT License.
 */
var Language = function Language (language, months, monthsAbbr, days) {
  this.language = language;
  this.months = months;
  this.monthsAbbr = monthsAbbr;
  this.days = days;
  this.rtl = false;
  this.ymd = false;
  this.yearSuffix = '';
};

var prototypeAccessors = { language: { configurable: true },months: { configurable: true },monthsAbbr: { configurable: true },days: { configurable: true } };

prototypeAccessors.language.get = function () {
  return this._language
};

prototypeAccessors.language.set = function (language) {
  if (typeof language !== 'string') {
    throw new TypeError('Language must be a string')
  }
  this._language = language;
};

prototypeAccessors.months.get = function () {
  return this._months
};

prototypeAccessors.months.set = function (months) {
  if (months.length !== 12) {
    throw new RangeError(("There must be 12 months for " + (this.language) + " language"))
  }
  this._months = months;
};

prototypeAccessors.monthsAbbr.get = function () {
  return this._monthsAbbr
};

prototypeAccessors.monthsAbbr.set = function (monthsAbbr) {
  if (monthsAbbr.length !== 12) {
    throw new RangeError(("There must be 12 abbreviated months for " + (this.language) + " language"))
  }
  this._monthsAbbr = monthsAbbr;
};

prototypeAccessors.days.get = function () {
  return this._days
};

prototypeAccessors.days.set = function (days) {
  if (days.length !== 7) {
    throw new RangeError(("There must be 7 days for " + (this.language) + " language"))
  }
  this._days = days;
};

Object.defineProperties( Language.prototype, prototypeAccessors );

var en = new Language(
  'English',
  ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
  ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
  ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
)
// eslint-disable-next-line
;

var DateUtils = {

  /**
   * Returns the full year, using UTC or not
   * @param {Date} date
   * @param {Boolean} useUtc
   */
  getFullYear: function getFullYear (date, useUtc) {
    return useUtc ? date.getUTCFullYear() : date.getFullYear()
  },

  /**
   * Returns the month, using UTC or not
   * @param {Date} date
   * @param {Boolean} useUtc
   */
  getMonth: function getMonth (date, useUtc) {
    return useUtc ? date.getUTCMonth() : date.getMonth()
  },

  /**
   * Returns the date, using UTC or not
   * @param {Date} date
   * @param {Boolean} useUtc
   */
  getDate: function getDate (date, useUtc) {
    return useUtc ? date.getUTCDate() : date.getDate()
  },

  /**
   * Returns the day, using UTC or not
   * @param {Date} date
   * @param {Boolean} useUtc
   */
  getDay: function getDay (date, useUtc) {
    return useUtc ? date.getUTCDay() : date.getDay()
  },

  /**
   * Returns the hours, using UTC or not
   * @param {Date} date
   * @param {Boolean} useUtc
   */
  getHours: function getHours (date, useUtc) {
    return useUtc ? date.getUTCHours() : date.getHours()
  },

  /**
   * Returns the minutes, using UTC or not
   * @param {Date} date
   * @param {Boolean} useUtc
   */
  getMinutes: function getMinutes (date, useUtc) {
    return useUtc ? date.getUTCMinutes() : date.getMinutes()
  },

  /**
   * Sets the full year, using UTC or not
   * @param {Date} date
   * @param {Boolean} useUtc
   */
  setFullYear: function setFullYear (date, value, useUtc) {
    return useUtc ? date.setUTCFullYear(value) : date.setFullYear(value)
  },

  /**
   * Sets the month, using UTC or not
   * @param {Date} date
   * @param {Boolean} useUtc
   */
  setMonth: function setMonth (date, value, useUtc) {
    return useUtc ? date.setUTCMonth(value) : date.setMonth(value)
  },

  /**
   * Sets the date, using UTC or not
   * @param {Date} date
   * @param {Boolean} useUtc
   */
  setDate: function setDate (date, value, useUtc) {
    return useUtc ? date.setUTCDate(value) : date.setDate(value)
  },

  /**
   * Check if date1 is equivalent to date2, without comparing the time
   * @see https://stackoverflow.com/a/6202196/4455925
   * @param {Date} date1
   * @param {Date} date2
   * @param {Boolean} useUtc
   */
  compareDates: function compareDates (date1, date2, useUtc) {
    var d1 = new Date(date1.getTime());
    var d2 = new Date(date2.getTime());

    if (useUtc) {
      d1.setUTCHours(0, 0, 0, 0);
      d2.setUTCHours(0, 0, 0, 0);
    } else {
      d1.setHours(0, 0, 0, 0);
      d2.setHours(0, 0, 0, 0);
    }
    return d1.getTime() === d2.getTime()
  },

  /**
   * Validates a date object
   * @param {Date} date - an object instantiated with the new Date constructor
   * @return {Boolean}
   */
  isValidDate: function isValidDate (date) {
    if (Object.prototype.toString.call(date) !== '[object Date]') {
      return false
    }
    return !isNaN(date.getTime())
  },

  /**
   * Return abbreviated week day name
   * @param {Date}
   * @param {Array}
   * @return {String}
   */
  getDayNameAbbr: function getDayNameAbbr (date, days, useUtc) {
    if (typeof date !== 'object') {
      throw TypeError('Invalid Type')
    }
    return days[this.getDay(date, useUtc)]
  },

  /**
   * Return name of the month
   * @param {Number|Date}
   * @param {Array}
   * @return {String}
   */
  getMonthName: function getMonthName (month, months, useUtc) {
    if (!months) {
      throw Error('missing 2nd parameter Months array')
    }
    if (typeof month === 'object') {
      return months[this.getMonth(month, useUtc)]
    }
    if (typeof month === 'number') {
      return months[month]
    }
    throw TypeError('Invalid type')
  },

  /**
   * Return an abbreviated version of the month
   * @param {Number|Date}
   * @return {String}
   */
  getMonthNameAbbr: function getMonthNameAbbr (month, monthsAbbr, useUtc) {
    if (!monthsAbbr) {
      throw Error('missing 2nd paramter Months array')
    }
    if (typeof month === 'object') {
      return monthsAbbr[this.getMonth(month, useUtc)]
    }
    if (typeof month === 'number') {
      return monthsAbbr[month]
    }
    throw TypeError('Invalid type')
  },

  /**
   * Alternative get total number of days in month
   * @param {Number} year
   * @param {Number} m
   * @return {Number}
   */
  daysInMonth: function daysInMonth (year, month) {
    return /8|3|5|10/.test(month) ? 30 : month === 1 ? (!(year % 4) && year % 100) || !(year % 400) ? 29 : 28 : 31
  },

  /**
   * Get nth suffix for date
   * @param {Number} day
   * @return {String}
   */
  getNthSuffix: function getNthSuffix (day) {
    switch (day) {
      case 1:
      case 21:
      case 31:
        return 'st'
      case 2:
      case 22:
        return 'nd'
      case 3:
      case 23:
        return 'rd'
      default:
        return 'th'
    }
  },

  /**
   * Formats date object
   * @param {Date}
   * @param {String}
   * @param {Object}
   * @return {String}
   */
  formatDate: function formatDate (date, format, translation, useUtc) {
    translation = (!translation) ? en : translation;
    var year = this.getFullYear(date, useUtc);
    var month = this.getMonth(date, useUtc) + 1;
    var day = this.getDate(date, useUtc);
    var str = format
      .replace(/dd/, ('0' + day).slice(-2))
      .replace(/d/, day)
      .replace(/yyyy/, year)
      .replace(/yy/, String(year).slice(2))
      .replace(/MMMM/, this.getMonthName(this.getMonth(date, useUtc), translation.months, useUtc))
      .replace(/MMM/, this.getMonthNameAbbr(this.getMonth(date, useUtc), translation.monthsAbbr, useUtc))
      .replace(/MM/, ('0' + month).slice(-2))
      .replace(/M(?!a|ä|e)/, month)
      .replace(/su/, this.getNthSuffix(this.getDate(date, useUtc)))
      .replace(/D(?!e|é|i)/, this.getDayNameAbbr(date, translation.days, useUtc));
    return str
  },

  /**
   * Creates an array of dates for each day in between two dates.
   * @param {Date} start
   * @param {Date} end
   * @return {Array}
   */
  createDateArray: function createDateArray (start, end, useUtc) {
    var this$1 = this;

    var dates = [];
    while (start <= end) {
      dates.push(new Date(start));
      start = this$1.setDate(new Date(start), this$1.getDate(new Date(start), useUtc) + 1, useUtc);
    }
    return dates
  }

}
// eslint-disable-next-line
;

(function(){ if(typeof document !== 'undefined'){ var head=document.head||document.getElementsByTagName('head')[0], style=document.createElement('style'), css=""; style.type='text/css'; if (style.styleSheet){ style.styleSheet.cssText = css; } else { style.appendChild(document.createTextNode(css)); } head.appendChild(style); } })();
var DateInput = {render: function(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{class:{'input-group' : _vm.bootstrapStyling}},[(_vm.calendarButton)?_c('span',{staticClass:"vdp-datepicker__calendar-button",class:{'input-group-addon' : _vm.bootstrapStyling},style:({'cursor:not-allowed;' : _vm.disabled}),on:{"click":_vm.showCalendar}},[_c('i',{class:_vm.calendarButtonIcon},[_vm._v(" "+_vm._s(_vm.calendarButtonIconContent)+" "),(!_vm.calendarButtonIcon)?_c('span',[_vm._v("…")]):_vm._e()])]):_vm._e(),_vm._v(" "),_c('input',{ref:_vm.refName,class:_vm.computedInputClass,attrs:{"type":_vm.inline ? 'hidden' : 'text',"name":_vm.name,"id":_vm.id,"open-date":_vm.openDate,"placeholder":_vm.placeholder,"clear-button":_vm.clearButton,"disabled":_vm.disabled,"required":_vm.required},domProps:{"value":_vm.formattedValue},on:{"click":_vm.showCalendar,"keypress":_vm.parseTypedDate,"blur":_vm.inputBlurred}}),_vm._v(" "),(_vm.clearButton && _vm.selectedDate)?_c('span',{staticClass:"vdp-datepicker__clear-button",class:{'input-group-addon' : _vm.bootstrapStyling},on:{"click":function($event){_vm.clearDate();}}},[_c('i',{class:_vm.clearButtonIcon},[(!_vm.clearButtonIcon)?_c('span',[_vm._v("×")]):_vm._e()])]):_vm._e()])},staticRenderFns: [],
  props: {
    selectedDate: Date,
    format: [String, Function],
    translation: Object,
    inline: Boolean,
    id: String,
    name: String,
    refName: String,
    openDate: Date,
    placeholder: String,
    inputClass: [String, Object],
    clearButton: Boolean,
    clearButtonIcon: String,
    calendarButton: Boolean,
    calendarButtonIcon: String,
    calendarButtonIconContent: String,
    disabled: Boolean,
    required: Boolean,
    typeable: Boolean,
    bootstrapStyling: Boolean,
    useUtc: Boolean
  },
  data: function data () {
    return {
      input: null,
      typedDate: false
    }
  },
  computed: {
    formattedValue: function formattedValue () {
      if (!this.selectedDate) {
        return null
      }
      if (this.typedDate) {
        return this.typedDate
      }
      return typeof this.format === 'function'
        ? this.format(this.selectedDate)
        : DateUtils.formatDate(new Date(this.selectedDate), this.format, this.translation, this.useUtc)
    },

    computedInputClass: function computedInputClass () {
      var cssClass = [this.inputClass];
      if (this.bootstrapStyling) {
        cssClass.push('form-control');
      }
      return cssClass.join(' ')
    }
  },
  methods: {
    showCalendar: function showCalendar () {
      this.$emit('showCalendar');
    },
    /**
     * Attempt to parse a typed date
     */
    parseTypedDate: function parseTypedDate (e) {
      if (!this.typeable) {
        e.preventDefault();
        return false
      }
      var typedDate = Date.parse(this.input.value);
      if (!isNaN(typedDate)) {
        this.typedDate = this.input.value;
        this.$emit('typedDate', new Date(this.typedDate));
      }
    },
    /**
     * nullify the typed date to defer to regular formatting
     * called once the input is blurred
     */
    inputBlurred: function inputBlurred () {
      if (!this.typedDate) {
        return
      }
      if (isNaN(Date.parse(this.input.value))) {
        this.clearDate();
      }
      this.input.value = null;
      this.typedDate = null;
    },
    /**
     * emit a clearDate event
     */
    clearDate: function clearDate () {
      this.$emit('clearDate');
    }
  },
  mounted: function mounted () {
    this.input = this.$el.querySelector('input');
  }
}
// eslint-disable-next-line
;

(function(){ if(typeof document !== 'undefined'){ var head=document.head||document.getElementsByTagName('head')[0], style=document.createElement('style'), css=""; style.type='text/css'; if (style.styleSheet){ style.styleSheet.cssText = css; } else { style.appendChild(document.createTextNode(css)); } head.appendChild(style); } })();
var PickerDay = {render: function(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{directives:[{name:"show",rawName:"v-show",value:(_vm.showDayView),expression:"showDayView"}],class:[_vm.calendarClass, 'vdp-datepicker__calendar'],style:(_vm.calendarStyle)},[_c('header',[_c('span',{staticClass:"prev",class:{ 'disabled' : _vm.isRtl ? _vm.isNextMonthDisabled(_vm.pageTimestamp) : _vm.isPreviousMonthDisabled(_vm.pageTimestamp) },on:{"click":function($event){_vm.isRtl ? _vm.nextMonth() : _vm.previousMonth();}}},[_vm._v("<")]),_vm._v(" "),_c('span',{staticClass:"day__month_btn",class:_vm.allowedToShowView('month') ? 'up' : '',on:{"click":_vm.showMonthCalendar}},[_vm._v(_vm._s(_vm.isYmd ? _vm.currYearName : _vm.currMonthName)+" "+_vm._s(_vm.isYmd ? _vm.currMonthName : _vm.currYearName))]),_vm._v(" "),_c('span',{staticClass:"next",class:{ 'disabled' : _vm.isRtl ? _vm.isPreviousMonthDisabled(_vm.pageTimestamp) : _vm.isNextMonthDisabled(_vm.pageTimestamp) },on:{"click":function($event){_vm.isRtl ? _vm.previousMonth() : _vm.nextMonth();}}},[_vm._v(">")])]),_vm._v(" "),_c('div',{class:_vm.isRtl ? 'flex-rtl' : ''},[_vm._l((_vm.daysOfWeek),function(d){return _c('span',{key:d.timestamp,staticClass:"cell day-header"},[_vm._v(_vm._s(d))])}),_vm._v(" "),(_vm.blankDays > 0)?_vm._l((_vm.blankDays),function(d){return _c('span',{key:d.timestamp,staticClass:"cell day blank"})}):_vm._e(),_vm._l((_vm.days),function(day){return _c('span',{key:day.timestamp,staticClass:"cell day",class:_vm.dayClasses(day),on:{"click":function($event){_vm.selectDate(day);}}},[_vm._v(_vm._s(day.date))])})],2)])},staticRenderFns: [],
  props: {
    showDayView: Boolean,
    selectedDate: Date,
    pageDate: Date,
    pageTimestamp: Number,
    fullMonthName: Boolean,
    allowedToShowView: Function,
    disabledDates: Object,
    highlighted: Object,
    calendarClass: String,
    calendarStyle: Object,
    translation: Object,
    isRtl: Boolean,
    mondayFirst: Boolean,
    useUtc: Boolean
  },
  computed: {
    /**
     * Returns an array of day names
     * @return {String[]}
     */
    daysOfWeek: function daysOfWeek () {
      if (this.mondayFirst) {
        var tempDays = this.translation.days.slice();
        tempDays.push(tempDays.shift());
        return tempDays
      }
      return this.translation.days
    },
    /**
     * Returns the day number of the week less one for the first of the current month
     * Used to show amount of empty cells before the first in the day calendar layout
     * @return {Number}
     */
    blankDays: function blankDays () {
      var d = this.pageDate;
      var dObj = this.useUtc
        ? new Date(Date.UTC(d.getUTCFullYear(), d.getUTCMonth(), 1))
        : new Date(d.getFullYear(), d.getMonth(), 1, d.getHours(), d.getMinutes());
      if (this.mondayFirst) {
        return DateUtils.getDay(dObj, this.useUtc) > 0 ? DateUtils.getDay(dObj, this.useUtc) - 1 : 6
      }
      return DateUtils.getDay(dObj, this.useUtc)
    },
    /**
     * @return {Object[]}
     */
    days: function days () {
      var this$1 = this;

      var d = this.pageDate;
      var days = [];
      // set up a new date object to the beginning of the current 'page'
      var dObj = this.useUtc
        ? new Date(Date.UTC(d.getUTCFullYear(), d.getUTCMonth(), 1))
        : new Date(d.getFullYear(), d.getMonth(), 1, d.getHours(), d.getMinutes());
      var daysInMonth = DateUtils.daysInMonth(DateUtils.getFullYear(dObj, this.useUtc), DateUtils.getMonth(dObj, this.useUtc));
      for (var i = 0; i < daysInMonth; i++) {
        days.push({
          date: DateUtils.getDate(dObj, this$1.useUtc),
          timestamp: dObj.getTime(),
          isSelected: this$1.isSelectedDate(dObj),
          isDisabled: this$1.isDisabledDate(dObj),
          isHighlighted: this$1.isHighlightedDate(dObj),
          isHighlightStart: this$1.isHighlightStart(dObj),
          isHighlightEnd: this$1.isHighlightEnd(dObj),
          isToday: DateUtils.compareDates(dObj, new Date(), this$1.useUtc),
          isWeekend: DateUtils.getDay(dObj, this$1.useUtc) === 0 || DateUtils.getDay(dObj, this$1.useUtc) === 6,
          isSaturday: DateUtils.getDay(dObj, this$1.useUtc) === 6,
          isSunday: DateUtils.getDay(dObj, this$1.useUtc) === 0
        });
        DateUtils.setDate(dObj, DateUtils.getDate(dObj, this$1.useUtc) + 1, this$1.useUtc);
      }
      return days
    },
    /**
     * Gets the name of the month the current page is on
     * @return {String}
     */
    currMonthName: function currMonthName () {
      var monthName = this.fullMonthName ? this.translation.months : this.translation.monthsAbbr;
      return DateUtils.getMonthNameAbbr(DateUtils.getMonth(this.pageDate, this.useUtc), monthName, this.useUtc)
    },
    /**
     * Gets the name of the year that current page is on
     * @return {Number}
     */
    currYearName: function currYearName () {
      var yearSuffix = this.translation.yearSuffix;
      return ("" + (DateUtils.getFullYear(this.pageDate, this.useUtc)) + yearSuffix)
    },
    /**
     * Is this translation using year/month/day format?
     * @return {Boolean}
     */
    isYmd: function isYmd () {
      return this.translation.ymd && this.translation.ymd === true
    }
  },
  methods: {
    selectDate: function selectDate (date) {
      if (date.isDisabled) {
        this.$emit('selectedDisabled', date);
        return false
      }
      this.$emit('selectDate', date);
    },
    /**
     * @return {Number}
     */
    getPageMonth: function getPageMonth () {
      return DateUtils.getMonth(this.pageDate, this.useUtc)
    },
    /**
     * Emit an event to show the month picker
     */
    showMonthCalendar: function showMonthCalendar () {
      this.$emit('showMonthCalendar');
    },
    /**
     * Change the page month
     * @param {Number} incrementBy
     */
    changeMonth: function changeMonth (incrementBy) {
      var date = this.pageDate;
      DateUtils.setMonth(date, DateUtils.getMonth(date, this.useUtc) + incrementBy, this.useUtc);
      this.$emit('changedMonth', date);
    },
    /**
     * Decrement the page month
     */
    previousMonth: function previousMonth () {
      if (!this.isPreviousMonthDisabled()) {
        this.changeMonth(-1);
      }
    },
    /**
     * Is the previous month disabled?
     * @return {Boolean}
     */
    isPreviousMonthDisabled: function isPreviousMonthDisabled () {
      if (!this.disabledDates || !this.disabledDates.to) {
        return false
      }
      var d = this.pageDate;
      return DateUtils.getMonth(this.disabledDates.to, this.useUtc) >= DateUtils.getMonth(d, this.useUtc) &&
        DateUtils.getFullYear(this.disabledDates.to, this.useUtc) >= DateUtils.getFullYear(d, this.useUtc)
    },
    /**
     * Increment the current page month
     */
    nextMonth: function nextMonth () {
      if (!this.isNextMonthDisabled()) {
        this.changeMonth(+1);
      }
    },
    /**
     * Is the next month disabled?
     * @return {Boolean}
     */
    isNextMonthDisabled: function isNextMonthDisabled () {
      if (!this.disabledDates || !this.disabledDates.from) {
        return false
      }
      var d = this.pageDate;
      return DateUtils.getMonth(this.disabledDates.from, this.useUtc) <= DateUtils.getMonth(d, this.useUtc) &&
        DateUtils.getFullYear(this.disabledDates.from, this.useUtc) <= DateUtils.getFullYear(d, this.useUtc)
    },
    /**
     * Whether a day is selected
     * @param {Date}
     * @return {Boolean}
     */
    isSelectedDate: function isSelectedDate (dObj) {
      return this.selectedDate && DateUtils.compareDates(this.selectedDate, dObj, this.useUtc)
    },
    /**
     * Whether a day is disabled
     * @param {Date}
     * @return {Boolean}
     */
    isDisabledDate: function isDisabledDate (date) {
      var this$1 = this;

      var disabledDates = false;

      if (typeof this.disabledDates === 'undefined') {
        return false
      }

      if (typeof this.disabledDates.dates !== 'undefined') {
        this.disabledDates.dates.forEach(function (d) {
          if (DateUtils.compareDates(date, d, this$1.useUtc)) {
            disabledDates = true;
            return true
          }
        });
      }
      if (typeof this.disabledDates.to !== 'undefined' && this.disabledDates.to && date < this.disabledDates.to) {
        disabledDates = true;
      }
      if (typeof this.disabledDates.from !== 'undefined' && this.disabledDates.from && date > this.disabledDates.from) {
        disabledDates = true;
      }
      if (typeof this.disabledDates.ranges !== 'undefined') {
        this.disabledDates.ranges.forEach(function (range) {
          if (typeof range.from !== 'undefined' && range.from && typeof range.to !== 'undefined' && range.to) {
            if (date < range.to && date > range.from) {
              disabledDates = true;
              return true
            }
          }
        });
      }
      if (typeof this.disabledDates.days !== 'undefined' && this.disabledDates.days.indexOf(DateUtils.getDay(date, this.useUtc)) !== -1) {
        disabledDates = true;
      }
      if (typeof this.disabledDates.daysOfMonth !== 'undefined' && this.disabledDates.daysOfMonth.indexOf(DateUtils.getDate(date, this.useUtc)) !== -1) {
        disabledDates = true;
      }
      if (typeof this.disabledDates.customPredictor === 'function' && this.disabledDates.customPredictor(date)) {
        disabledDates = true;
      }
      return disabledDates
    },
    /**
     * Whether a day is highlighted (only if it is not disabled already except when highlighted.includeDisabled is true)
     * @param {Date}
     * @return {Boolean}
     */
    isHighlightedDate: function isHighlightedDate (date) {
      var this$1 = this;

      if (!(this.highlighted && this.highlighted.includeDisabled) && this.isDisabledDate(date)) {
        return false
      }

      var highlighted = false;

      if (typeof this.highlighted === 'undefined') {
        return false
      }

      if (typeof this.highlighted.dates !== 'undefined') {
        this.highlighted.dates.forEach(function (d) {
          if (DateUtils.compareDates(date, d, this$1.useUtc)) {
            highlighted = true;
            return true
          }
        });
      }

      if (this.isDefined(this.highlighted.from) && this.isDefined(this.highlighted.to)) {
        highlighted = date >= this.highlighted.from && date <= this.highlighted.to;
      }

      if (typeof this.highlighted.days !== 'undefined' && this.highlighted.days.indexOf(DateUtils.getDay(date, this.useUtc)) !== -1) {
        highlighted = true;
      }

      if (typeof this.highlighted.daysOfMonth !== 'undefined' && this.highlighted.daysOfMonth.indexOf(DateUtils.getDate(date, this.useUtc)) !== -1) {
        highlighted = true;
      }

      if (typeof this.highlighted.customPredictor === 'function' && this.highlighted.customPredictor(date)) {
        highlighted = true;
      }

      return highlighted
    },
    dayClasses: function dayClasses (day) {
      return {
        'selected': day.isSelected,
        'disabled': day.isDisabled,
        'highlighted': day.isHighlighted,
        'today': day.isToday,
        'weekend': day.isWeekend,
        'sat': day.isSaturday,
        'sun': day.isSunday,
        'highlight-start': day.isHighlightStart,
        'highlight-end': day.isHighlightEnd
      }
    },
    /**
     * Whether a day is highlighted and it is the first date
     * in the highlighted range of dates
     * @param {Date}
     * @return {Boolean}
     */
    isHighlightStart: function isHighlightStart (date) {
      return this.isHighlightedDate(date) &&
        (this.highlighted.from instanceof Date) &&
        (DateUtils.getFullYear(this.highlighted.from, this.useUtc) === DateUtils.getFullYear(date, this.useUtc)) &&
        (DateUtils.getMonth(this.highlighted.from, this.useUtc) === DateUtils.getMonth(date, this.useUtc)) &&
        (DateUtils.getDate(this.highlighted.from, this.useUtc) === DateUtils.getDate(date, this.useUtc))
    },
    /**
     * Whether a day is highlighted and it is the first date
     * in the highlighted range of dates
     * @param {Date}
     * @return {Boolean}
     */
    isHighlightEnd: function isHighlightEnd (date) {
      return this.isHighlightedDate(date) &&
        (this.highlighted.to instanceof Date) &&
        (DateUtils.getFullYear(this.highlighted.to, this.useUtc) === DateUtils.getFullYear(date, this.useUtc)) &&
        (DateUtils.getMonth(this.highlighted.to, this.useUtc) === DateUtils.getMonth(date, this.useUtc)) &&
        (DateUtils.getDate(this.highlighted.to, this.useUtc) === DateUtils.getDate(date, this.useUtc))
    },
    /**
     * Helper
     * @param  {mixed}  prop
     * @return {Boolean}
     */
    isDefined: function isDefined (prop) {
      return typeof prop !== 'undefined' && prop
    }
  }
}
// eslint-disable-next-line
;

(function(){ if(typeof document !== 'undefined'){ var head=document.head||document.getElementsByTagName('head')[0], style=document.createElement('style'), css=""; style.type='text/css'; if (style.styleSheet){ style.styleSheet.cssText = css; } else { style.appendChild(document.createTextNode(css)); } head.appendChild(style); } })();
var PickerMonth = {render: function(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{directives:[{name:"show",rawName:"v-show",value:(_vm.showMonthView),expression:"showMonthView"}],class:[_vm.calendarClass, 'vdp-datepicker__calendar'],style:(_vm.calendarStyle)},[_c('header',[_c('span',{staticClass:"prev",class:{ 'disabled' : _vm.isPreviousYearDisabled(_vm.pageTimestamp) },on:{"click":_vm.previousYear}},[_vm._v("<")]),_vm._v(" "),_c('span',{staticClass:"month__year_btn",class:_vm.allowedToShowView('year') ? 'up' : '',on:{"click":_vm.showYearCalendar}},[_vm._v(_vm._s(_vm.pageYearName))]),_vm._v(" "),_c('span',{staticClass:"next",class:{ 'disabled' : _vm.isNextYearDisabled(_vm.pageTimestamp) },on:{"click":_vm.nextYear}},[_vm._v(">")])]),_vm._v(" "),_vm._l((_vm.months),function(month){return _c('span',{key:month.timestamp,staticClass:"cell month",class:{'selected': month.isSelected, 'disabled': month.isDisabled},on:{"click":function($event){$event.stopPropagation();_vm.selectMonth(month);}}},[_vm._v(_vm._s(month.month))])})],2)},staticRenderFns: [],
  props: {
    showMonthView: Boolean,
    selectedDate: Date,
    pageDate: Date,
    pageTimestamp: Number,
    disabledDates: Object,
    calendarClass: String,
    calendarStyle: Object,
    translation: Object,
    allowedToShowView: Function,
    useUtc: Boolean
  },
  computed: {
    months: function months () {
      var this$1 = this;

      var d = this.pageDate;
      var months = [];
      // set up a new date object to the beginning of the current 'page'
      var dObj = this.useUtc
        ? new Date(Date.UTC(d.getUTCFullYear(), 0, d.getUTCDate()))
        : new Date(d.getFullYear(), 0, d.getDate(), d.getHours(), d.getMinutes());
      for (var i = 0; i < 12; i++) {
        months.push({
          month: DateUtils.getMonthName(i, this$1.translation.months, this$1.useUtc),
          timestamp: dObj.getTime(),
          isSelected: this$1.isSelectedMonth(dObj),
          isDisabled: this$1.isDisabledMonth(dObj)
        });
        DateUtils.setMonth(dObj, DateUtils.getMonth(dObj, this$1.useUtc) + 1, this$1.useUtc);
      }
      return months
    },
    /**
     * Get year name on current page.
     * @return {String}
     */
    pageYearName: function pageYearName () {
      var yearSuffix = this.translation.yearSuffix;
      return ("" + (DateUtils.getFullYear(this.pageDate, this.useUtc)) + yearSuffix)
    }
  },
  methods: {
    /**
     * Emits a selectMonth event
     * @param {Object} month
     */
    selectMonth: function selectMonth (month) {
      if (month.isDisabled) {
        return false
      }
      this.$emit('selectMonth', month);
    },
    /**
     * Changes the year up or down
     * @param {Number} incrementBy
     */
    changeYear: function changeYear (incrementBy) {
      var date = this.pageDate;
      DateUtils.setFullYear(date, DateUtils.getFullYear(date, this.useUtc) + incrementBy, this.useUtc);
      this.$emit('changedYear', date);
    },
    /**
     * Decrements the year
     */
    previousYear: function previousYear () {
      if (!this.isPreviousYearDisabled()) {
        this.changeYear(-1);
      }
    },
    /**
     * Checks if the previous year is disabled or not
     * @return {Boolean}
     */
    isPreviousYearDisabled: function isPreviousYearDisabled () {
      if (!this.disabledDates || !this.disabledDates.to) {
        return false
      }
      return DateUtils.getFullYear(this.disabledDates.to, this.useUtc) >= DateUtils.getFullYear(this.pageDate, this.useUtc)
    },
    /**
     * Increments the year
     */
    nextYear: function nextYear () {
      if (!this.isNextYearDisabled()) {
        this.changeYear(1);
      }
    },
    /**
     * Checks if the next year is disabled or not
     * @return {Boolean}
     */
    isNextYearDisabled: function isNextYearDisabled () {
      if (!this.disabledDates || !this.disabledDates.from) {
        return false
      }
      return DateUtils.getFullYear(this.disabledDates.from, this.useUtc) <= DateUtils.getFullYear(this.pageDate, this.useUtc)
    },
    /**
     * Emits an event that shows the year calendar
     */
    showYearCalendar: function showYearCalendar () {
      this.$emit('showYearCalendar');
    },
    /**
     * Whether the selected date is in this month
     * @param {Date}
     * @return {Boolean}
     */
    isSelectedMonth: function isSelectedMonth (date) {
      return (this.selectedDate &&
        DateUtils.getFullYear(this.selectedDate, this.useUtc) === DateUtils.getFullYear(date, this.useUtc) &&
        DateUtils.getMonth(this.selectedDate, this.useUtc) === DateUtils.getMonth(date, this.useUtc))
    },
    /**
     * Whether a month is disabled
     * @param {Date}
     * @return {Boolean}
     */
    isDisabledMonth: function isDisabledMonth (date) {
      var disabledDates = false;

      if (typeof this.disabledDates === 'undefined') {
        return false
      }

      if (typeof this.disabledDates.to !== 'undefined' && this.disabledDates.to) {
        if (
          (DateUtils.getMonth(date, this.useUtc) < DateUtils.getMonth(this.disabledDates.to, this.useUtc) && DateUtils.getFullYear(date, this.useUtc) <= DateUtils.getFullYear(this.disabledDates.to, this.useUtc)) ||
          DateUtils.getFullYear(date, this.useUtc) < DateUtils.getFullYear(this.disabledDates.to, this.useUtc)
        ) {
          disabledDates = true;
        }
      }
      if (typeof this.disabledDates.from !== 'undefined' && this.disabledDates.from) {
        if (
          this.disabledDates.from &&
          (DateUtils.getMonth(date, this.useUtc) > DateUtils.getMonth(this.disabledDates.from, this.useUtc) && DateUtils.getFullYear(date, this.useUtc) >= DateUtils.getFullYear(this.disabledDates.from, this.useUtc)) ||
          DateUtils.getFullYear(date, this.useUtc) > DateUtils.getFullYear(this.disabledDates.from, this.useUtc)
        ) {
          disabledDates = true;
        }
      }
      return disabledDates
    }
  }
}
// eslint-disable-next-line
;

(function(){ if(typeof document !== 'undefined'){ var head=document.head||document.getElementsByTagName('head')[0], style=document.createElement('style'), css=""; style.type='text/css'; if (style.styleSheet){ style.styleSheet.cssText = css; } else { style.appendChild(document.createTextNode(css)); } head.appendChild(style); } })();
var PickerYear = {render: function(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{directives:[{name:"show",rawName:"v-show",value:(_vm.showYearView),expression:"showYearView"}],class:[_vm.calendarClass, 'vdp-datepicker__calendar'],style:(_vm.calendarStyle)},[_c('header',[_c('span',{staticClass:"prev",class:{ 'disabled' : _vm.isPreviousDecadeDisabled(_vm.pageTimestamp) },on:{"click":_vm.previousDecade}},[_vm._v("<")]),_vm._v(" "),_c('span',[_vm._v(_vm._s(_vm.getPageDecade))]),_vm._v(" "),_c('span',{staticClass:"next",class:{ 'disabled' : _vm.isNextDecadeDisabled(_vm.pageTimestamp) },on:{"click":_vm.nextDecade}},[_vm._v(">")])]),_vm._v(" "),_vm._l((_vm.years),function(year){return _c('span',{key:year.timestamp,staticClass:"cell year",class:{ 'selected': year.isSelected, 'disabled': year.isDisabled },on:{"click":function($event){$event.stopPropagation();_vm.selectYear(year);}}},[_vm._v(_vm._s(year.year))])})],2)},staticRenderFns: [],
  props: {
    showYearView: Boolean,
    selectedDate: Date,
    pageDate: Date,
    pageTimestamp: Number,
    disabledDates: Object,
    highlighted: Object,
    calendarClass: String,
    calendarStyle: Object,
    translation: Object,
    allowedToShowView: Function,
    useUtc: Boolean
  },
  computed: {
    years: function years () {
      var this$1 = this;

      var d = this.pageDate;
      var years = [];
      // set up a new date object to the beginning of the current 'page'7
      var dObj = this.useUtc
        ? new Date(Date.UTC(Math.floor(d.getUTCFullYear() / 10) * 10, d.getUTCMonth(), d.getUTCDate()))
        : new Date(Math.floor(d.getFullYear() / 10) * 10, d.getMonth(), d.getDate(), d.getHours(), d.getMinutes());
      for (var i = 0; i < 10; i++) {
        years.push({
          year: DateUtils.getFullYear(dObj, this$1.useUtc),
          timestamp: dObj.getTime(),
          isSelected: this$1.isSelectedYear(dObj),
          isDisabled: this$1.isDisabledYear(dObj)
        });
        DateUtils.setFullYear(dObj, DateUtils.getFullYear(dObj, this$1.useUtc) + 1, this$1.useUtc);
      }
      return years
    },
    /**
     * @return {String}
     */
    getPageDecade: function getPageDecade () {
      var decadeStart = Math.floor(DateUtils.getFullYear(this.pageDate, this.useUtc) / 10) * 10;
      var decadeEnd = decadeStart + 9;
      var yearSuffix = this.translation.yearSuffix;
      return (decadeStart + " - " + decadeEnd + yearSuffix)
    }
  },
  methods: {
    selectYear: function selectYear (year) {
      if (year.isDisabled) {
        return false
      }
      this.$emit('selectYear', year);
    },
    changeYear: function changeYear (incrementBy) {
      var date = this.pageDate;
      DateUtils.setFullYear(date, DateUtils.getFullYear(date, this.useUtc) + incrementBy, this.useUtc);
      this.$emit('changedDecade', date);
    },
    previousDecade: function previousDecade () {
      if (this.isPreviousDecadeDisabled()) {
        return false
      }
      this.changeYear(-10);
    },
    isPreviousDecadeDisabled: function isPreviousDecadeDisabled () {
      if (!this.disabledDates || !this.disabledDates.to) {
        return false
      }
      return Math.floor(DateUtils.getFullYear(this.disabledDates.to, this.useUtc) / 10) * 10 >= Math.floor(DateUtils.getFullYear(this.pageDate, this.useUtc) / 10) * 10
    },
    nextDecade: function nextDecade () {
      if (this.isNextDecadeDisabled()) {
        return false
      }
      this.changeYear(10);
    },
    isNextDecadeDisabled: function isNextDecadeDisabled () {
      if (!this.disabledDates || !this.disabledDates.from) {
        return false
      }
      return Math.ceil(DateUtils.getFullYear(this.disabledDates.from, this.useUtc) / 10) * 10 <= Math.ceil(DateUtils.getFullYear(this.pageDate, this.useUtc) / 10) * 10
    },

    /**
     * Whether the selected date is in this year
     * @param {Date}
     * @return {Boolean}
     */
    isSelectedYear: function isSelectedYear (date) {
      return this.selectedDate && DateUtils.getFullYear(this.selectedDate, this.useUtc) === DateUtils.getFullYear(date, this.useUtc)
    },
    /**
     * Whether a year is disabled
     * @param {Date}
     * @return {Boolean}
     */
    isDisabledYear: function isDisabledYear (date) {
      var disabledDates = false;
      if (typeof this.disabledDates === 'undefined' || !this.disabledDates) {
        return false
      }

      if (typeof this.disabledDates.to !== 'undefined' && this.disabledDates.to) {
        if (DateUtils.getFullYear(date, this.useUtc) < DateUtils.getFullYear(this.disabledDates.to, this.useUtc)) {
          disabledDates = true;
        }
      }
      if (typeof this.disabledDates.from !== 'undefined' && this.disabledDates.from) {
        if (DateUtils.getFullYear(date, this.useUtc) > DateUtils.getFullYear(this.disabledDates.from, this.useUtc)) {
          disabledDates = true;
        }
      }

      return disabledDates
    }
  }
}
// eslint-disable-next-line
;

(function(){ if(typeof document !== 'undefined'){ var head=document.head||document.getElementsByTagName('head')[0], style=document.createElement('style'), css=".rtl { direction: rtl; } .vdp-datepicker { position: relative; text-align: left; } .vdp-datepicker * { box-sizing: border-box; } .vdp-datepicker__calendar { position: absolute; z-index: 100; background: #fff; width: 300px; border: 1px solid #ccc; } .vdp-datepicker__calendar header { display: block; line-height: 40px; } .vdp-datepicker__calendar header span { display: inline-block; text-align: center; width: 71.42857142857143%; float: left; } .vdp-datepicker__calendar header .prev, .vdp-datepicker__calendar header .next { width: 14.285714285714286%; float: left; text-indent: -10000px; position: relative; } .vdp-datepicker__calendar header .prev:after, .vdp-datepicker__calendar header .next:after { content: ''; position: absolute; left: 50%; top: 50%; -webkit-transform: translateX(-50%) translateY(-50%); transform: translateX(-50%) translateY(-50%); border: 6px solid transparent; } .vdp-datepicker__calendar header .prev:after { border-right: 10px solid #000; margin-left: -5px; } .vdp-datepicker__calendar header .prev.disabled:after { border-right: 10px solid #ddd; } .vdp-datepicker__calendar header .next:after { border-left: 10px solid #000; margin-left: 5px; } .vdp-datepicker__calendar header .next.disabled:after { border-left: 10px solid #ddd; } .vdp-datepicker__calendar header .prev:not(.disabled), .vdp-datepicker__calendar header .next:not(.disabled), .vdp-datepicker__calendar header .up:not(.disabled) { cursor: pointer; } .vdp-datepicker__calendar header .prev:not(.disabled):hover, .vdp-datepicker__calendar header .next:not(.disabled):hover, .vdp-datepicker__calendar header .up:not(.disabled):hover { background: #eee; } .vdp-datepicker__calendar .disabled { color: #ddd; cursor: default; } .vdp-datepicker__calendar .flex-rtl { display: flex; width: inherit; flex-wrap: wrap; } .vdp-datepicker__calendar .cell { display: inline-block; padding: 0 5px; width: 14.285714285714286%; height: 40px; line-height: 40px; text-align: center; vertical-align: middle; border: 1px solid transparent; } .vdp-datepicker__calendar .cell:not(.blank):not(.disabled).day, .vdp-datepicker__calendar .cell:not(.blank):not(.disabled).month, .vdp-datepicker__calendar .cell:not(.blank):not(.disabled).year { cursor: pointer; } .vdp-datepicker__calendar .cell:not(.blank):not(.disabled).day:hover, .vdp-datepicker__calendar .cell:not(.blank):not(.disabled).month:hover, .vdp-datepicker__calendar .cell:not(.blank):not(.disabled).year:hover { border: 1px solid #4bd; } .vdp-datepicker__calendar .cell.selected { background: #4bd; } .vdp-datepicker__calendar .cell.selected:hover { background: #4bd; } .vdp-datepicker__calendar .cell.selected.highlighted { background: #4bd; } .vdp-datepicker__calendar .cell.highlighted { background: #cae5ed; } .vdp-datepicker__calendar .cell.highlighted.disabled { color: #a3a3a3; } .vdp-datepicker__calendar .cell.grey { color: #888; } .vdp-datepicker__calendar .cell.grey:hover { background: inherit; } .vdp-datepicker__calendar .cell.day-header { font-size: 75%; white-space: no-wrap; cursor: inherit; } .vdp-datepicker__calendar .cell.day-header:hover { background: inherit; } .vdp-datepicker__calendar .month, .vdp-datepicker__calendar .year { width: 33.333%; } .vdp-datepicker__clear-button, .vdp-datepicker__calendar-button { cursor: pointer; font-style: normal; } .vdp-datepicker__clear-button.disabled, .vdp-datepicker__calendar-button.disabled { color: #999; cursor: default; } "; style.type='text/css'; if (style.styleSheet){ style.styleSheet.cssText = css; } else { style.appendChild(document.createTextNode(css)); } head.appendChild(style); } })();
var Datepicker = {render: function(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"vdp-datepicker",class:[_vm.wrapperClass, _vm.isRtl ? 'rtl' : '']},[_c('date-input',{attrs:{"selectedDate":_vm.selectedDate,"format":_vm.format,"translation":_vm.translation,"inline":_vm.inline,"id":_vm.id,"name":_vm.name,"refName":_vm.refName,"openDate":_vm.openDate,"placeholder":_vm.placeholder,"inputClass":_vm.inputClass,"typeable":_vm.typeable,"clearButton":_vm.clearButton,"clearButtonIcon":_vm.clearButtonIcon,"calendarButton":_vm.calendarButton,"calendarButtonIcon":_vm.calendarButtonIcon,"calendarButtonIconContent":_vm.calendarButtonIconContent,"disabled":_vm.disabled,"required":_vm.required,"bootstrapStyling":_vm.bootstrapStyling,"use-utc":_vm.useUtc},on:{"showCalendar":_vm.showCalendar,"typedDate":_vm.setTypedDate,"clearDate":_vm.clearDate}}),_vm._v(" "),(_vm.allowedToShowView('day'))?_c('picker-day',{attrs:{"pageDate":_vm.pageDate,"selectedDate":_vm.selectedDate,"showDayView":_vm.showDayView,"fullMonthName":_vm.fullMonthName,"allowedToShowView":_vm.allowedToShowView,"disabledDates":_vm.disabledDates,"highlighted":_vm.highlighted,"calendarClass":_vm.calendarClass,"calendarStyle":_vm.calendarStyle,"translation":_vm.translation,"pageTimestamp":_vm.pageTimestamp,"isRtl":_vm.isRtl,"mondayFirst":_vm.mondayFirst,"use-utc":_vm.useUtc},on:{"changedMonth":_vm.setPageDate,"selectDate":_vm.selectDate,"showMonthCalendar":_vm.showMonthCalendar,"selectedDisabled":function($event){_vm.$emit('selectedDisabled');}}}):_vm._e(),_vm._v(" "),(_vm.allowedToShowView('month'))?_c('picker-month',{attrs:{"pageDate":_vm.pageDate,"selectedDate":_vm.selectedDate,"showMonthView":_vm.showMonthView,"allowedToShowView":_vm.allowedToShowView,"disabledDates":_vm.disabledDates,"calendarClass":_vm.calendarClass,"calendarStyle":_vm.calendarStyle,"translation":_vm.translation,"use-utc":_vm.useUtc},on:{"selectMonth":_vm.selectMonth,"showYearCalendar":_vm.showYearCalendar,"changedYear":_vm.setPageDate}}):_vm._e(),_vm._v(" "),(_vm.allowedToShowView('year'))?_c('picker-year',{attrs:{"pageDate":_vm.pageDate,"selectedDate":_vm.selectedDate,"showYearView":_vm.showYearView,"allowedToShowView":_vm.allowedToShowView,"disabledDates":_vm.disabledDates,"calendarClass":_vm.calendarClass,"calendarStyle":_vm.calendarStyle,"translation":_vm.translation,"use-utc":_vm.useUtc},on:{"selectYear":_vm.selectYear,"changedDecade":_vm.setPageDate}}):_vm._e()],1)},staticRenderFns: [],
  components: {
    DateInput: DateInput,
    PickerDay: PickerDay,
    PickerMonth: PickerMonth,
    PickerYear: PickerYear
  },
  props: {
    value: {
      validator: function (val) {
        return val === null || val instanceof Date || typeof val === 'string' || typeof val === 'number'
      }
    },
    name: String,
    refName: String,
    id: String,
    format: {
      type: [String, Function],
      default: 'dd MMM yyyy'
    },
    language: {
      type: Object,
      default: function () { return en; }
    },
    openDate: {
      validator: function (val) {
        return val === null || val instanceof Date || typeof val === 'string' || typeof val === 'number'
      }
    },
    fullMonthName: Boolean,
    disabledDates: Object,
    highlighted: Object,
    placeholder: String,
    inline: Boolean,
    calendarClass: [String, Object],
    inputClass: [String, Object],
    wrapperClass: [String, Object],
    mondayFirst: Boolean,
    clearButton: Boolean,
    clearButtonIcon: String,
    calendarButton: Boolean,
    calendarButtonIcon: String,
    calendarButtonIconContent: String,
    bootstrapStyling: Boolean,
    initialView: String,
    disabled: Boolean,
    required: Boolean,
    typeable: Boolean,
    useUtc: Boolean,
    minimumView: {
      type: String,
      default: 'day'
    },
    maximumView: {
      type: String,
      default: 'year'
    }
  },
  data: function data () {
    var startDate = this.openDate ? new Date(this.openDate) : new Date();
    var pageTimestamp = DateUtils.setDate(startDate, 1, this.useUtc);
    return {
      /*
       * Vue cannot observe changes to a Date Object so date must be stored as a timestamp
       * This represents the first day of the current viewing month
       * {Number}
       */
      pageTimestamp: pageTimestamp,
      /*
       * Selected Date
       * {Date}
       */
      selectedDate: null,
      /*
       * Flags to show calendar views
       * {Boolean}
       */
      showDayView: false,
      showMonthView: false,
      showYearView: false,
      /*
       * Positioning
       */
      calendarHeight: 0
    }
  },
  watch: {
    value: function value (value$1) {
      this.setValue(value$1);
    },
    openDate: function openDate () {
      this.setPageDate();
    },
    initialView: function initialView () {
      this.setInitialView();
    }
  },
  computed: {
    computedInitialView: function computedInitialView () {
      if (!this.initialView) {
        return this.minimumView
      }

      return this.initialView
    },
    pageDate: function pageDate () {
      return new Date(this.pageTimestamp)
    },

    translation: function translation () {
      return this.language
    },

    calendarStyle: function calendarStyle () {
      return {
        position: this.isInline ? 'static' : undefined
      }
    },
    isOpen: function isOpen () {
      return this.showDayView || this.showMonthView || this.showYearView
    },
    isInline: function isInline () {
      return !!this.inline
    },
    isRtl: function isRtl () {
      return this.translation.rtl === true
    }
  },
  methods: {
    /**
     * Called in the event that the user navigates to date pages and
     * closes the picker without selecting a date.
     */
    resetDefaultPageDate: function resetDefaultPageDate () {
      if (this.selectedDate === null) {
        this.setPageDate();
        return
      }
      this.setPageDate(this.selectedDate);
    },
    /**
     * Effectively a toggle to show/hide the calendar
     * @return {mixed}
     */
    showCalendar: function showCalendar () {
      if (this.disabled || this.isInline) {
        return false
      }
      if (this.isOpen) {
        return this.close(true)
      }
      this.setInitialView();
      if (!this.isInline) {
        this.$emit('opened');
      }
    },
    /**
     * Sets the initial picker page view: day, month or year
     */
    setInitialView: function setInitialView () {
      var initialView = this.computedInitialView;
      if (!this.allowedToShowView(initialView)) {
        throw new Error(("initialView '" + (this.initialView) + "' cannot be rendered based on minimum '" + (this.minimumView) + "' and maximum '" + (this.maximumView) + "'"))
      }
      switch (initialView) {
        case 'year':
          this.showYearCalendar();
          break
        case 'month':
          this.showMonthCalendar();
          break
        default:
          this.showDayCalendar();
          break
      }
    },
    /**
     * Are we allowed to show a specific picker view?
     * @param {String} view
     * @return {Boolean}
     */
    allowedToShowView: function allowedToShowView (view) {
      var views = ['day', 'month', 'year'];
      var minimumViewIndex = views.indexOf(this.minimumView);
      var maximumViewIndex = views.indexOf(this.maximumView);
      var viewIndex = views.indexOf(view);

      return viewIndex >= minimumViewIndex && viewIndex <= maximumViewIndex
    },
    /**
     * Show the day picker
     * @return {Boolean}
     */
    showDayCalendar: function showDayCalendar () {
      if (!this.allowedToShowView('day')) {
        return false
      }
      this.close();
      this.showDayView = true;
      this.addOutsideClickListener();
      return true
    },
    /**
     * Show the month picker
     * @return {Boolean}
     */
    showMonthCalendar: function showMonthCalendar () {
      if (!this.allowedToShowView('month')) {
        return false
      }
      this.close();
      this.showMonthView = true;
      this.addOutsideClickListener();
      return true
    },
    /**
     * Show the year picker
     * @return {Boolean}
     */
    showYearCalendar: function showYearCalendar () {
      if (!this.allowedToShowView('year')) {
        return false
      }
      this.close();
      this.showYearView = true;
      this.addOutsideClickListener();
      return true
    },
    /**
     * Set the selected date
     * @param {Number} timestamp
     */
    setDate: function setDate (timestamp) {
      var date = new Date(timestamp);
      this.selectedDate = new Date(date);
      this.setPageDate(date);
      this.$emit('selected', new Date(date));
      this.$emit('input', new Date(date));
    },
    /**
     * Clear the selected date
     */
    clearDate: function clearDate () {
      this.selectedDate = null;
      this.setPageDate();
      this.$emit('selected', null);
      this.$emit('input', null);
      this.$emit('cleared');
    },
    /**
     * @param {Object} day
     */
    selectDate: function selectDate (day) {
      this.setDate(day.timestamp);
      if (!this.isInline) {
        this.close(true);
      }
    },
    /**
     * @param {Object} month
     */
    selectMonth: function selectMonth (month) {
      var date = new Date(month.timestamp);
      if (this.allowedToShowView('day')) {
        this.setPageDate(date);
        this.$emit('changedMonth', month);
        this.showDayCalendar();
      } else {
        this.setDate(date);
        if (!this.isInline) {
          this.close(true);
        }
      }
    },
    /**
     * @param {Object} year
     */
    selectYear: function selectYear (year) {
      var date = new Date(year.timestamp);
      if (this.allowedToShowView('month')) {
        this.setPageDate(date);
        this.$emit('changedYear', year);
        this.showMonthCalendar();
      } else {
        this.setDate(date);
        if (!this.isInline) {
          this.close(true);
        }
      }
    },
    /**
     * Set the datepicker value
     * @param {Date|String|Number|null} date
     */
    setValue: function setValue (date) {
      if (typeof date === 'string' || typeof date === 'number') {
        var parsed = new Date(date);
        date = isNaN(parsed.valueOf()) ? null : parsed;
      }
      if (!date) {
        this.setPageDate();
        this.selectedDate = null;
        return
      }
      this.selectedDate = date;
      this.setPageDate(date);
    },
    /**
     * Sets the date that the calendar should open on
     */
    setPageDate: function setPageDate (date) {
      if (!date) {
        if (this.openDate) {
          date = new Date(this.openDate);
        } else {
          date = new Date();
        }
      }
      this.pageTimestamp = DateUtils.setDate(new Date(date), 1, this.useUtc);
    },
    /**
     * Set the date from a typedDate event
     */
    setTypedDate: function setTypedDate (date) {
      this.setDate(date.getTime());
    },
    /**
     * Set up an event listener for clicks outside the picker
     */
    addOutsideClickListener: function addOutsideClickListener () {
      var this$1 = this;

      if (!this.isInline) {
        setTimeout(function () {
          document.addEventListener('click', this$1.clickOutside, false);
        }, 100);
      }
    },
    /**
     * Close the calendar if clicked outside the datepicker
     * @param  {Event} event
     */
    clickOutside: function clickOutside (event) {
      if (this.$el && !this.$el.contains(event.target)) {
        this.resetDefaultPageDate();
        this.close(true);
        document.removeEventListener('click', this.clickOutside, false);
      }
    },
    /**
     * Close all calendar layers
     */
    close: function close (full) {
      this.showDayView = this.showMonthView = this.showYearView = false;
      if (!this.isInline) {
        if (full) { this.$emit('closed'); }
        document.removeEventListener('click', this.clickOutside, false);
      }
    },
    /**
     * Initiate the component
     */
    init: function init () {
      if (this.value) {
        this.setValue(this.value);
      }
      if (this.isInline) {
        this.setInitialView();
      }
    }
  },
  mounted: function mounted () {
    this.init();
  }
}
// eslint-disable-next-line
;

/* harmony default export */ __webpack_exports__["a"] = (Datepicker);


/***/ }),

/***/ 129:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(130)
/* template */
var __vue_template__ = __webpack_require__(131)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\js\\components\\admin\\PluginModal.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-4ed6c946", Component.options)
  } else {
    hotAPI.reload("data-v-4ed6c946", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 130:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__core_Form__ = __webpack_require__(8);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      plugin_form: new __WEBPACK_IMPORTED_MODULE_0__core_Form__["a" /* default */]({
        name: ''
      })
    };
  },


  methods: {
    plugin_form_submit: function plugin_form_submit() {
      var _this = this;

      this.plugin_form.submit('post', '/plugin').then(function (data) {
        _this.$emit("updateplugin", data);
        $('#pluginModal').modal('hide');
      }).catch(function (errors) {
        return console.log(errors);
      });
    }
  }
});

/***/ }),

/***/ 131:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "modal fade", attrs: { id: "pluginModal" } },
    [
      _c("div", { staticClass: "modal-dialog", attrs: { role: "document" } }, [
        _c("div", { staticClass: "modal-content" }, [
          _vm._m(0),
          _vm._v(" "),
          _c("div", { staticClass: "modal-body" }, [
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-xs-12" }, [
                _c(
                  "form",
                  {
                    attrs: { method: "POST" },
                    on: {
                      submit: function($event) {
                        $event.preventDefault()
                        return _vm.plugin_form_submit($event)
                      },
                      keydown: function($event) {
                        _vm.plugin_form.errors.clear($event.target.name)
                      }
                    }
                  },
                  [
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", [_vm._v("Name:")]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.plugin_form.name,
                            expression: "plugin_form.name"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { type: "text", name: "name" },
                        domProps: { value: _vm.plugin_form.name },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.plugin_form,
                              "name",
                              $event.target.value
                            )
                          }
                        }
                      }),
                      _vm._v(" "),
                      _vm.plugin_form.errors.has("name")
                        ? _c("span", {
                            staticClass: "help is-danger",
                            domProps: {
                              textContent: _vm._s(
                                _vm.plugin_form.errors.get("name")
                              )
                            }
                          })
                        : _vm._e()
                    ]),
                    _vm._v(" "),
                    _c(
                      "button",
                      {
                        staticClass: "button-submit",
                        attrs: { disabled: _vm.plugin_form.errors.any() }
                      },
                      [_vm._v("Create")]
                    )
                  ]
                )
              ])
            ])
          ])
        ])
      ])
    ]
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "modal-header" }, [
      _c(
        "button",
        {
          staticClass: "close",
          attrs: {
            type: "button",
            "data-dismiss": "modal",
            "aria-label": "Close"
          }
        },
        [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("×")])]
      ),
      _vm._v(" "),
      _c("h4", { staticClass: "modal-title" }, [_vm._v("Create Plugin")])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-4ed6c946", module.exports)
  }
}

/***/ }),

/***/ 15:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Errors = function () {
	/**
  * Create a new Errors instance.
  */
	function Errors() {
		_classCallCheck(this, Errors);

		this.errors = {};
	}

	/**
  * Determine if an errors exists for the given field.
  *
  * @param {string} field
  */


	_createClass(Errors, [{
		key: "has",
		value: function has(field) {
			return this.errors.hasOwnProperty(field);
		}

		/**
   * Determine if we have any errors.
   */

	}, {
		key: "any",
		value: function any() {
			return Object.keys(this.errors).length > 0;
		}

		/**
   * Retrieve the error message for a field
   *
   * @param {string} field
   */

	}, {
		key: "get",
		value: function get(field) {
			if (this.errors[field]) {
				return this.errors[field][0];
			}
		}

		/**
   * Record the new errors.
   *
   * @param {object} errors
   */

	}, {
		key: "record",
		value: function record(errors) {
			this.errors = errors;
		}

		/**
   * Clear one or all error fields.
   *
   * @param {string|null} field
   */

	}, {
		key: "clear",
		value: function clear(field) {
			if (field) {

				delete this.errors[field];
				return;
			}
			this.errors = {};
		}
	}]);

	return Errors;
}();

/* harmony default export */ __webpack_exports__["a"] = (Errors);

/***/ }),

/***/ 2:
/***/ (function(module, exports) {

/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file.
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier /* server only */
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = injectStyles
  }

  if (hook) {
    var functional = options.functional
    var existing = functional
      ? options.render
      : options.beforeCreate

    if (!functional) {
      // inject component registration as beforeCreate hook
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    } else {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return existing(h, context)
      }
    }
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 572:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(573);


/***/ }),

/***/ 573:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__core_Form__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__4keys_vuejs_datepicker__ = __webpack_require__(128);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_admin_PluginModal__ = __webpack_require__(129);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_admin_PluginModal___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__components_admin_PluginModal__);

//import DatePicker from 'vue2-datepicker'



new Vue({
    el: '#appReport',
    components: { DatePicker: __WEBPACK_IMPORTED_MODULE_1__4keys_vuejs_datepicker__["a" /* default */], PluginModal: __WEBPACK_IMPORTED_MODULE_2__components_admin_PluginModal___default.a },
    data: {
        period_id: null,
        client_id: '',
        clientName: null,
        metrics: null,
        traffic: null,
        client: {
            client: {
                google_view_id: null
            }
        },
        buttonAnalytics: true,
        edit_form: new __WEBPACK_IMPORTED_MODULE_0__core_Form__["a" /* default */]({
            //period
            start_date: '',
            end_date: '',

            //status  
            core: '',
            last_update: '',
            plugins: [],

            //server
            backup: '',
            load_time: '',
            media: '',
            uptime: '',
            host: '',
            cdn: '',
            files: '',
            last_down: '',

            //security
            attacks: '',
            certified: '',
            expiration: null,
            https: '',
            country_1: '',
            country_1_number: '',
            country_2: '',
            country_2_number: '',
            country_3: '',
            country_3_number: '',
            country_4: '',
            country_4_number: '',
            country_5: '',
            country_5_number: '',

            //code
            conflicts: '',
            broken_links: '',
            mobile_friendly: '',
            alt_text: '',
            title_tags: '',
            meta_description: '',

            //issue
            issue: []

        }),

        select_plugins: [],
        user_plugins: [],
        plugin_selected: '',
        plugin_name: '',
        plugin_version: ''

    },

    mounted: function mounted() {
        var url = window.location.href.split('/');
        var id = url.splice(4, 1);
        this.client_id = id[0];
        this.getClientName();
        this.getPlugins();
        this.getClient();
        this.getPeriod();
    },

    methods: {
        getPeriod: function getPeriod() {
            var _this = this;

            axios.get('/period/client_period/' + this.client_id).then(function (response) {
                console.log(response.data);

                if (response.data.length != 0) {
                    _this.edit_form.start_date = response.data.period.start;
                    _this.edit_form.end_date = response.data.period.end;
                    _this.edit_form.core = response.data.status.core;
                    _this.edit_form.last_update = response.data.status.updated;

                    for (var i = 0; i <= response.data.plugins.length - 1; i++) {
                        _this.select_plugins.push({ id: response.data.plugins[i].id, name: response.data.plugins[i].name });
                    }

                    for (var i = 0; i <= response.data.plugins_period.length - 1; i++) {
                        _this.edit_form.plugins.push({
                            version: response.data.plugins_period[i].pivot.version,
                            active: response.data.plugins_period[i].pivot.active,
                            id: response.data.plugins_period[i].pivot.plugin_id
                        });

                        _this.user_plugins.push({
                            id: response.data.plugins_period[i].id,
                            name: response.data.plugins_period[i].name,
                            version: response.data.plugins_period[i].pivot.version
                        });
                    }

                    _this.edit_form.backup = response.data.server.backup;
                    _this.edit_form.load_time = response.data.server.load_time.toFixed(2);;
                    _this.edit_form.media = response.data.server.media_items;
                    _this.edit_form.uptime = response.data.server.uptime.toFixed(2);;
                    _this.edit_form.host = response.data.server.host;
                    _this.edit_form.cdn = response.data.server.delivery;
                    _this.edit_form.files = response.data.server.storage;
                    _this.edit_form.last_down = response.data.server.last_down;

                    _this.edit_form.attacks = response.data.security.attacks;
                    _this.edit_form.certified = response.data.security.certified;
                    _this.edit_form.expiration = response.data.security.expiration;
                    _this.edit_form.https = response.data.security.https;
                    _this.edit_form.country_1 = response.data.security.country_1;
                    _this.edit_form.country_1_number = response.data.security.country_1_number;
                    _this.edit_form.country_2 = response.data.security.country_2;
                    _this.edit_form.country_2_number = response.data.security.country_2_number;
                    _this.edit_form.country_3 = response.data.security.country_3;
                    _this.edit_form.country_3_number = response.data.security.country_3_number;
                    _this.edit_form.country_4 = response.data.security.country_4;
                    _this.edit_form.country_4_number = response.data.security.country_4_number;
                    _this.edit_form.country_5 = response.data.security.country_5;
                    _this.edit_form.country_5_number = response.data.security.country_5_number;

                    _this.edit_form.conflicts = response.data.code.conflicts;
                    _this.edit_form.broken_links = response.data.code.broken_links;
                    _this.edit_form.mobile_friendly = response.data.code.mobile_friendly;
                    _this.edit_form.alt_text = response.data.code.alt_text;
                    _this.edit_form.title_tags = response.data.code.title_tags;
                    _this.edit_form.meta_description = response.data.code.meta_description;

                    for (var i = 0; i <= response.data.issues.length - 1; i++) {
                        _this.edit_form.issue.push({
                            id: response.data.issues[i].id,
                            name: response.data.issues[i].name,
                            fix_number: response.data.issues[i].fix_number,
                            fix_time: response.data.issues[i].fix_time
                        });
                    }
                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        getClient: function getClient() {
            var _this2 = this;

            axios.get('/client/get/' + this.client_id).then(function (response) {
                _this2.client = response.data;
            }).catch(function (error) {
                console.log(error);
            });
        },
        getAnalytics: function getAnalytics() {
            var _this3 = this;

            this.buttonAnalytics = false;
            axios.get('/google', {
                params: {
                    client_id: this.client.client.id,
                    period_id: this.period_id,
                    start: this.edit_form.start_date,
                    end: this.edit_form.end_date
                }
            }).then(function (response) {
                _this3.buttonAnalytics = true;
                _this3.metrics = response.data.metrics;
                _this3.traffic = response.data.traffic;
            }).catch(function (error) {
                _this3.buttonAnalytics = true;
                console.log(error);
            });
        },
        storeAnalytics: function storeAnalytics() {
            axios.post('/google', {
                period_id: this.period_id,
                metrics: this.metrics,
                traffic: this.traffic
            }).then(function (response) {
                console.log(response.data);
            }).catch(function (error) {
                return console.log(error);
            });
        },
        getPlugins: function getPlugins() {
            var _this4 = this;

            axios.get('/plugin').then(function (response) {
                _this4.select_plugins = [];
                for (var i = 0; i <= response.data.length - 1; i++) {
                    _this4.select_plugins.push({ id: response.data[i].id, name: response.data[i].name });
                }
                _this4.plugin_selected = _this4.newPlugin;
            }).catch(function (error) {
                console.log(error);
            });
        },
        addPlugin: function addPlugin() {
            this.edit_form.plugins.push({
                version: this.edit_form.plugin_version || '',
                active: 1,
                id: this.plugin_selected
            });

            this.user_plugins.push({
                id: this.plugin_selected,
                name: this.plugin_name,
                version: this.plugin_version || ''
            });

            delete this.edit_form.errors.errors.plugins;
        },
        updatePluginName: function updatePluginName() {

            for (var i = 0; i <= this.select_plugins.length; i++) {
                if (this.select_plugins[i].id == this.plugin_selected) {
                    this.plugin_name = this.select_plugins[i].name;
                }
            }
        },
        removePlugin: function removePlugin(index) {
            this.user_plugins.splice(index, 1);
            this.edit_form.plugins.splice(index, 1);
        },
        getNewPlugin: function getNewPlugin(plugin) {
            //this.select_plugins = [];
            this.getPlugins();
            this.newPlugin = plugin.id;
            this.plugin_name = plugin.name;
        },
        addIssue: function addIssue() {
            this.edit_form.issue.push({
                id: '',
                name: this.edit_form.issue.name,
                fix_number: this.edit_form.issue.fix_number,
                fix_time: this.edit_form.issue.fix_time

            });
        },
        removeIssue: function removeIssue(index) {
            this.edit_form.issue.splice(index, 1);
        },
        edit_form_submit: function edit_form_submit() {
            var _this5 = this;

            this.edit_form.expiration = this.edit_form.certified ? this.edit_form.expiration : null;
            this.edit_form.submit('post', '/period/' + this.client_id).then(function (data) {
                _this5.period_id = data.id;
                _this5.storeAnalytics();
                window.location.href = "/period/show/" + _this5.period_id;
            }).catch(function (errors) {
                console.log(errors);
            });
        },
        customFormatter: function customFormatter(date) {
            var day = ("0" + date.getDate()).slice(-2);
            var month = ("0" + (date.getMonth() + 1)).slice(-2);
            var year = date.getFullYear();

            return year + '-' + month + '-' + day;
        },
        getClientName: function getClientName() {
            var _this6 = this;

            axios.get('/client/getClientName/' + this.client_id).then(function (response) {
                return _this6.clientName = response.data;
            }).catch(function (error) {
                return console.log(error);
            });
        },
        clearError: function clearError(error_name) {
            delete this.edit_form.errors.errors[error_name];
        },


        /* setEndDate(data){
           delete this.edit_form.errors.errors["start_date"];  
           delete this.edit_form.errors.errors["end_date"];  
           //var date = new Date(data);
           data = data.setMonth(data.getMonth()+1);
           var date = new Date(data);
           //console.log(date.toString());
           this.edit_form.end_date = date.toString();
         },*/

        scrollTop: function scrollTop() {
            //window.scrollTo(0, 0);
            var scrollStep = -window.scrollY / (100 / 15),
                scrollInterval = setInterval(function () {
                if (window.scrollY != 0) {
                    window.scrollBy(0, scrollStep);
                } else clearInterval(scrollInterval);
            }, 15);
        }
    }
});

/***/ }),

/***/ 8:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Errors__ = __webpack_require__(15);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }



var Form = function () {

	/**
  * Create a new Form instance
  *
  * @param {object} data
  */
	function Form(data) {
		_classCallCheck(this, Form);

		this.originalData = data;

		for (var field in data) {
			this[field] = data[field];
		}

		this.errors = new __WEBPACK_IMPORTED_MODULE_0__Errors__["a" /* default */]();
	}

	/**
  * Fetch all relevant data for the form.
  */


	_createClass(Form, [{
		key: 'data',
		value: function data() {
			var data = {};

			for (var property in this.originalData) {
				data[property] = this[property];
			}

			return data;
		}

		/**
   * Reset the form fields.
   */

	}, {
		key: 'reset',
		value: function reset() {
			for (var field in this.originalData) {
				if (Array.isArray(this[field])) {
					this[field] = [];
				} else {
					this[field] = '';
				}
			}

			this.errors.clear();
		}

		/**
   * Submit the form.
   *
   * @param {string} requestType
   * @param {string} url
   */

	}, {
		key: 'submit',
		value: function submit(requestType, url) {
			var _this = this;

			return new Promise(function (resolve, reject) {
				axios[requestType](url, _this.data()).then(function (response) {
					_this.onSuccess(response.data);
					resolve(response.data);
				}).catch(function (error) {
					_this.onFail(error.response.data.errors);
					reject(error.response.data.errors);
				});
			});
		}

		/**
   * Handle a successful form submission.
   *
   * @param {object} data
   */

	}, {
		key: 'onSuccess',
		value: function onSuccess(data) {
			//alert(data.message);
			this.reset();
		}

		/**
   * Handle a failed form submission.
   *
   * @param {object} errors
   */

	}, {
		key: 'onFail',
		value: function onFail(errors) {
			this.errors.record(errors);
		}
	}]);

	return Form;
}();

/* harmony default export */ __webpack_exports__["a"] = (Form);

/***/ })

/******/ });