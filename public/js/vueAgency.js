/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 536);
/******/ })
/************************************************************************/
/******/ ({

/***/ 15:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Errors = function () {
	/**
  * Create a new Errors instance.
  */
	function Errors() {
		_classCallCheck(this, Errors);

		this.errors = {};
	}

	/**
  * Determine if an errors exists for the given field.
  *
  * @param {string} field
  */


	_createClass(Errors, [{
		key: "has",
		value: function has(field) {
			return this.errors.hasOwnProperty(field);
		}

		/**
   * Determine if we have any errors.
   */

	}, {
		key: "any",
		value: function any() {
			return Object.keys(this.errors).length > 0;
		}

		/**
   * Retrieve the error message for a field
   *
   * @param {string} field
   */

	}, {
		key: "get",
		value: function get(field) {
			if (this.errors[field]) {
				return this.errors[field][0];
			}
		}

		/**
   * Record the new errors.
   *
   * @param {object} errors
   */

	}, {
		key: "record",
		value: function record(errors) {
			this.errors = errors;
		}

		/**
   * Clear one or all error fields.
   *
   * @param {string|null} field
   */

	}, {
		key: "clear",
		value: function clear(field) {
			if (field) {

				delete this.errors[field];
				return;
			}
			this.errors = {};
		}
	}]);

	return Errors;
}();

/* harmony default export */ __webpack_exports__["a"] = (Errors);

/***/ }),

/***/ 536:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(537);


/***/ }),

/***/ 537:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__core_Form__ = __webpack_require__(8);


new Vue({
  el: '#appAgency',

  data: {
    form_agency: new __WEBPACK_IMPORTED_MODULE_0__core_Form__["a" /* default */]({
      name: '',
      file: '',
      address: '',
      site: '',
      subdomain: ''
    })

  },

  methods: {
    previewFile: function previewFile() {

      var reader = new FileReader();
      var vm = this;
      reader.onload = function (e) {
        vm.form_agency.file = e.target.result;
      };
      reader.readAsDataURL(this.$refs.file.files[0]);

      //console.log(reader);
    },
    onSubmit: function onSubmit() {
      this.form_agency.submit('post', '/agency').then(function (response) {
        window.location.href = "/client";
      }).catch(function (errors) {
        return console.log(errors);
      });
    }
  }
});

/***/ }),

/***/ 8:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Errors__ = __webpack_require__(15);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }



var Form = function () {

	/**
  * Create a new Form instance
  *
  * @param {object} data
  */
	function Form(data) {
		_classCallCheck(this, Form);

		this.originalData = data;

		for (var field in data) {
			this[field] = data[field];
		}

		this.errors = new __WEBPACK_IMPORTED_MODULE_0__Errors__["a" /* default */]();
	}

	/**
  * Fetch all relevant data for the form.
  */


	_createClass(Form, [{
		key: 'data',
		value: function data() {
			var data = {};

			for (var property in this.originalData) {
				data[property] = this[property];
			}

			return data;
		}

		/**
   * Reset the form fields.
   */

	}, {
		key: 'reset',
		value: function reset() {
			for (var field in this.originalData) {
				if (Array.isArray(this[field])) {
					this[field] = [];
				} else {
					this[field] = '';
				}
			}

			this.errors.clear();
		}

		/**
   * Submit the form.
   *
   * @param {string} requestType
   * @param {string} url
   */

	}, {
		key: 'submit',
		value: function submit(requestType, url) {
			var _this = this;

			return new Promise(function (resolve, reject) {
				axios[requestType](url, _this.data()).then(function (response) {
					_this.onSuccess(response.data);
					resolve(response.data);
				}).catch(function (error) {
					_this.onFail(error.response.data.errors);
					reject(error.response.data.errors);
				});
			});
		}

		/**
   * Handle a successful form submission.
   *
   * @param {object} data
   */

	}, {
		key: 'onSuccess',
		value: function onSuccess(data) {
			//alert(data.message);
			this.reset();
		}

		/**
   * Handle a failed form submission.
   *
   * @param {object} errors
   */

	}, {
		key: 'onFail',
		value: function onFail(errors) {
			this.errors.record(errors);
		}
	}]);

	return Form;
}();

/* harmony default export */ __webpack_exports__["a"] = (Form);

/***/ })

/******/ });