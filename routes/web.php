<?php

/*
|---------------------------------------------------------------------
| Authentication Routes
|---------------------------------------------------------------------
|
| These are the routes that define the authentication, the model of 
| the url is: mysitedashboard.com/client/login and same goes to 
| forgot password, and reset password. 
|
*/

//Route::get('login', 'Auth\LoginClientController@show')->name('login'); - Different page on main site
Route::get('/',                 'Auth\LoginClientController@show')->name('login');
Route::get('{subdomain}/login', 'Auth\LoginClientController@show')->name('client.login');
Route::post('login',            'Auth\LoginClientController@authenticate')->name('post.login');
Route::post('logout',           'Auth\LoginClientController@logout')->name('logout');

Route::get('/password/reset',                    'Auth\ForgotPasswordClientController@showLinkRequestForm')->name('password.request');
Route::get('{subdomain}/password/reset',         'Auth\ForgotPasswordClientController@showLinkRequestForm')->name('client.password.request');
Route::post('password/email',                    'Auth\ForgotPasswordClientController@sendResetLinkEmail')->name('password.email');
Route::get('/password/reset/{token}',            'Auth\ResetPasswordClientController@showResetForm')->name('password.reset');
Route::get('{subdomain}/password/reset/{token}', 'Auth\ResetPasswordClientController@showResetForm')->name('password.reset');
Route::post('password/reset',                    'Auth\ResetPasswordClientController@reset');



/*
|---------------------------------------------------------------------
| Dashboard Routes
|---------------------------------------------------------------------
|
| These routes show the dashboard information for the client, most of 
| them are axios request to pull information.
|
*/

Route::get('/period/{clientId}', 'ReportController@getPeriods');

Route::get('/dashboard_status', 'DashboardController@dashboard_status');
Route::get('/dashboard_plugin', 'DashboardController@dashboard_plugin');
Route::get('/dashboard_server', 'DashboardController@dashboard_server');
Route::get('/dashboard_security', 'DashboardController@dashboard_security');
Route::get('/dashboard_code', 'DashboardController@dashboard_code');
Route::get('/dashboard_issue', 'DashboardController@dashboard_issue');
Route::get('/dashboard_google', 'DashboardController@dashboard_google');
Route::get('/dashboard_authorize_gtm', 'DashboardController@dashboard_authorize_gtm');
Route::put('/dashboard_update_issue/{period_id}', 'DashboardController@dashboard_update_issue');
Route::post('/dashboard_store_comment', 'DashboardController@dashboard_store_comment');

/*
|---------------------------------------------------------------------
| Administration Routes
|---------------------------------------------------------------------
|
| These routes are only accessible by the admin users and define the
| administration section of the platform, on these routes are the 
| creation and managment of clients, agencies, users and reports.
|
*/

Route::group(['middleware' => ['role:SuperAdmin']], function () {

    Route::post('report/create_period/',            'ReportController@create_period');
    Route::get('report/create/{id}',                'ReportController@create');
    Route::get('report/{client_id}',                'ReportController@show');
    Route::get('analytics/clients',                 'AnalyticsController@clients'); //Erase after update
    Route::get('analytics/periods',                 'AnalyticsController@periods'); //Erase after update
    Route::get('client/get/{client_id}',            'ClientController@getClient');
    Route::get('client/information',                'ClientController@information');
    Route::get('client/information/{agency_id}',    'ClientController@informationAgency');
    Route::get('client/getClientName/{client_id}',  'ClientController@getClientName');
    Route::get('client/getClients',                 'ClientController@getClients');
    Route::put('issue/update/{issue_id}',           'IssueController@updateAdmin');
    Route::get('issue/period/{period_id}',          'IssueController@getIssues');
    Route::get('period/get/{period_id}',            'PeriodController@getPeriod');
    Route::get('/period/client_period/{client_id}', 'PeriodController@getLastPeriod');
    Route::get('period/{client_id}/create',         'PeriodController@create');
    Route::post('period/{client_id}',               'PeriodController@store');
    Route::get('period/show/{client_id}',           'PeriodController@show');
    Route::get('period/getclient/{client_id}',      'PeriodController@getClient');
    Route::put('period/reset/{period_id}',           'PeriodController@reset');
    Route::get('report/list/{client_id}',           'ReportController@index');
    Route::get('agency/information',                'AgencyController@information');
    Route::get('agency/getAgencyName/{agency_id}',  'AgencyController@getAgencyName');
    Route::get('agency/users/{agency_id}',          'AgencyController@getUsers');
    Route::get('agency/get/{agency_id}',            'AgencyController@getAgency');

    Route::post('user/agency/',                      'UserController@storeUserAgency');
    Route::put('user/agency/{agency_id}',            'UserController@updateUserAgency');

    Route::resource('client',   'ClientController');
    Route::resource('google',   'GoogleController');
    Route::resource('plugin',   'PluginController');
    Route::resource('status',   'StatusController');
    Route::resource('server',   'ServerController');
    Route::resource('security', 'SecurityController');
    Route::resource('code',     'CodeController');
    Route::resource('issue',    'IssueController');
    Route::resource('comment',  'CommentController');
    Route::resource('agency',   'AgencyController');

    Route::resource('user',     'UserController');
    Route::resource('period',   'PeriodController');
    
});

Route::group(['middleware' => ['role:Admin']], function () {

  Route::get('{subdomain}/clients', 'AgencyController@showClients')->name('agency.clients');
  Route::get('{subdomain}/agency/report/list/{client_id}', 'ReportController@showReportsAgency');
  Route::get('{subdomain}/client/getClientName/{client_id}', 'ClientController@getAgencyClientName');
  Route::get('{subdomain}/period/show/{client_id}', 'PeriodController@showReportAgency');
  
});

Route::get('/{subdomain}', 'DashboardController@index')->name('dashboard');
