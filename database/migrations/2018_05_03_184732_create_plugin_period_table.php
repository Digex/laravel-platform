<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePluginPeriodTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plugin_period', function (Blueprint $table) {
            $table->increments('id');
            $table->string('version', 10);
            $table->boolean('active');
            $table->unsignedInteger("period_id");
            $table->unsignedInteger("plugin_id");
            $table->timestamps();

            $table->foreign('period_id')->references("id")->on("periods");
            $table->foreign('plugin_id')->references("id")->on("plugins");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plugin_period');
    }
}
