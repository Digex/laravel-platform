<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrafficTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('traffic', function (Blueprint $table) {
            $table->increments('id');
            $table->string('page_1_link', 200);
            $table->integer('page_1_link_views');
            $table->string('page_2_link', 200)->nullable();
            $table->integer('page_2_link_views')->nullable();
            $table->string('page_3_link', 200)->nullable();
            $table->integer('page_3_link_views')->nullable();
            $table->string('page_4_link', 200)->nullable();
            $table->integer('page_4_link_views')->nullable();
            $table->string('page_5_link', 200)->nullable();
            $table->integer('page_5_link_views')->nullable();
            $table->float('android', 5, 2);
            $table->float('ios', 5, 2);
            $table->float('windows', 5, 2);
            $table->float('mac', 5, 2);
            $table->float('linux', 5, 2);
            $table->float('os_other', 5, 2);
            $table->float('chrome', 5, 2);
            $table->float('firefox', 5, 2);
            $table->float('explorer', 5, 2);
            $table->float('edge', 5, 2);
            $table->float('safari', 5, 2);
            $table->float('browser_other', 5, 2);
            $table->float('desktop', 5, 2);
            $table->float('mobile', 5, 2);
            $table->float('tablet', 5, 2);
            $table->unsignedInteger("period_id");
            $table->timestamps();

            $table->foreign('period_id')->references("id")->on("periods");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('traffic');
    }
}
