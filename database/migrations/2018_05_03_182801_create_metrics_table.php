<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMetricsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metrics', function (Blueprint $table) {
            $table->increments('id');
            $table->float('new_visitors', 5, 2);
            $table->float('returning_visitors', 5, 2);
            $table->date('day_1');
            $table->integer('day_1_visits');
            $table->date('day_2');
            $table->integer('day_2_visits');
            $table->date('day_3');
            $table->integer('day_3_visits');
            $table->integer('unique');
            $table->float('unique_percentage', 8, 2);
            $table->integer('total');
            $table->float('total_percentage', 8, 2);
            $table->integer('conversion');
            $table->float('conversion_percentage', 8, 2);
            $table->float('avg_pages', 5, 2);
            $table->time('avg_time');
            $table->integer('period_id')->unsigned();
            $table->timestamps();

            $table->foreign('period_id')->references('id')->on('periods');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('metrics');
    }
}
