<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSecuritiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('securities', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('https');
            $table->string('certified', 30);
            $table->integer('attacks');
            $table->date('expiration');
            $table->string('country_1', 3);
            $table->integer('country_1_number');
            $table->string('country_2', 3);
            $table->integer('country_2_number');
            $table->string('country_3', 3);
            $table->integer('country_3_number');
            $table->string('country_4', 3);
            $table->integer('country_4_number');
            $table->string('country_5', 3);
            $table->integer('country_5_number');
            $table->unsignedInteger("period_id");
            $table->timestamps();

            $table->foreign('period_id')->references("id")->on("periods");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('securities');
    }
}
