<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('codes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('conflicts');
            $table->integer('broken_links');
            $table->boolean('mobile_friendly');
            $table->integer('alt_text');
            $table->integer('title_tags');
            $table->integer('meta_description');
            $table->unsignedInteger("period_id");
            $table->timestamps();

            $table->foreign('period_id')->references("id")->on("periods");


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('codes');
    }
}
