<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCascadeToServersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('statuses', function (Blueprint $table) {
            $table->dropForeign('statuses_period_id_foreign');

            $table->foreign('period_id')
            ->references('id')->on('periods')
            ->onDelete('cascade');
        });

        Schema::table('plugin_period', function (Blueprint $table) {
            $table->dropForeign('plugin_period_period_id_foreign');

            $table->foreign('period_id')
            ->references('id')->on('periods')
            ->onDelete('cascade');
        });

        Schema::table('servers', function (Blueprint $table) {
            $table->dropForeign('servers_period_id_foreign');

            $table->foreign('period_id')
            ->references('id')->on('periods')
            ->onDelete('cascade');
        });

        Schema::table('metrics', function (Blueprint $table) {
            $table->dropForeign('metrics_period_id_foreign');

            $table->foreign('period_id')
            ->references('id')->on('periods')
            ->onDelete('cascade');
        });

        Schema::table('traffic', function (Blueprint $table) {
            $table->dropForeign('traffic_period_id_foreign');

            $table->foreign('period_id')
            ->references('id')->on('periods')
            ->onDelete('cascade');
        });

        Schema::table('securities', function (Blueprint $table) {
            $table->dropForeign('securities_period_id_foreign');

            $table->foreign('period_id')
            ->references('id')->on('periods')
            ->onDelete('cascade');
        });

        Schema::table('codes', function (Blueprint $table) {
            $table->dropForeign('codes_period_id_foreign');

            $table->foreign('period_id')
            ->references('id')->on('periods')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
