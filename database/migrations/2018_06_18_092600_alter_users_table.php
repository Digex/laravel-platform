<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            // change() tells the Schema builder that we are altering a table
            $table->integer('agency_id')->unsigned()->default(1);
            $table->integer('client_id')->nullable()->unsigned()->change();

            $table->foreign('agency_id')
            ->references("id")->on("agencies")
            ->onDelete('cascade');

        });

        Schema::table('clients', function (Blueprint $table) {
            $table->dropForeign('clients_agency_id_foreign');

            $table->foreign('agency_id')
            ->references('id')->on('agencies')
            ->onDelete('cascade');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            // change() tells the Schema builder that we are altering a table
            $table->dropColumn('agency_id');
            $table->integer('client_id')->nullable(false)->unsigned()->change();

            $table->dropForeign('users_agency_id_foreign');
        });
    }
}
