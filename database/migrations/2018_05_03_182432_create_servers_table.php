<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servers', function (Blueprint $table) {
            $table->increments('id');
            $table->date('backup');
            $table->string('host', 30);
            $table->float('load_time', 5, 2);
            $table->boolean('delivery');
            $table->integer('media_items');
            $table->string('storage',10);
            $table->float('uptime', 6, 2);
            $table->date('last_down');
            $table->unsignedInteger("period_id");
            $table->timestamps();

            $table->foreign('period_id')->references("id")->on("periods");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servers');
    }
}
