<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSecuritiesTableNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('securities', function (Blueprint $table) {
            // change() tells the Schema builder that we are altering a table
            $table->string('country_1', 3)->nullable()->change();
            $table->string('country_2', 3)->nullable()->change();
            $table->string('country_3', 3)->nullable()->change();
            $table->string('country_4', 3)->nullable()->change();
            $table->string('country_5', 3)->nullable()->change();

            $table->integer('country_1_number')->nullable()->change();
            $table->integer('country_2_number')->nullable()->change();
            $table->integer('country_3_number')->nullable()->change();
            $table->integer('country_4_number')->nullable()->change();
            $table->integer('country_5_number')->nullable()->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('securities', function (Blueprint $table) {
            // change() tells the Schema builder that we are altering a table
            $table->string('country_1', 3)->nullable(false)->change();
            $table->string('country_2', 3)->nullable(false)->change();
            $table->string('country_3', 3)->nullable(false)->change();
            $table->string('country_4', 3)->nullable(false)->change();
            $table->string('country_5', 3)->nullable(false)->change();

            $table->integer('country_1_number')->nullable(false)->change();
            $table->integer('country_2_number')->nullable(false)->change();
            $table->integer('country_3_number')->nullable(false)->change();
            $table->integer('country_4_number')->nullable(false)->change();
            $table->integer('country_5_number')->nullable(false)->change();

            
        });
    }
}
