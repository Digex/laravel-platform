<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->string('lastname', 50);
            $table->string('email', 100)->unique();
            $table->string('password');
            $table->rememberToken();
            $table->unsignedInteger("client_id");
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('client_id')->references("id")->on("clients");
        });

        DB::table('users')->insert(
            array(
                'name' => 'Rebecca',
                'lastname' => 'Vanderwerf',
                'email' => 'dev@digizent.com',
                'password' => bcrypt('D1g1z3nt!'),
                'client_id' => 1
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
