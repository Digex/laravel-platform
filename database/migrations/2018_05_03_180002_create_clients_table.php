<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 150);
            $table->string('subdomain', 30)->unique();
            $table->string('logo', 50)->nullable();
            $table->string('site', 50)->nullable();
            $table->string('primary_color', 6)->default('1a6980');
            $table->string('secondary_color', 6)->default('d7e1e3');
            $table->string('google_view_id', 10)->nullable();
            $table->integer('agency_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('agency_id')->references('id')->on('agencies');
        });

        DB::table('clients')->insert(
            array(
                'name' => 'MySiteDashboard',
                'subdomain' => 'mysitedashboard',
                'logo' => 'client_1.png',
                'site' => 'https://mysitedashboard.com',
                'google_view_id' => '',
                'agency_id' => 1
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
