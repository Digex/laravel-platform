<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSecuritiesTableExpiration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('securities', function (Blueprint $table) {
            // change() tells the Schema builder that we are altering a table
            $table->string('certified', 60)->nullable()->change();
            $table->date('expiration')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('securities', function (Blueprint $table) {
            // change() tells the Schema builder that we are altering a table
            $table->string('certified', 60)->nullable(false)->change();
            $table->date('expiration')->nullable(false)->change();
        });
    }
}
