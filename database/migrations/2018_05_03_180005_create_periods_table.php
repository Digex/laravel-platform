<?php
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeriodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('periods', function (Blueprint $table) {
            $table->increments('id');
            $table->date('start');
            $table->date('end');
            $table->integer('month');
            $table->integer('year');
            $table->enum('issues_status', ['show','fix','ignore'])->default('show');
            $table->unsignedInteger("client_id");
            $table->timestamps();

            $table->foreign('client_id')->references("id")->on("clients");


        });

        DB::table('periods')->insert(
            array(
                'start' => '2018-02-15',
                'end' => '2018-03-15',
                'month' => '03',
                'year' => '2018',
                'client_id' => 1
            )
        );

        DB::table('periods')->insert(
            array(
                'start' => '2018-03-15',
                'end' => '2018-04-15',
                'month' => '04',
                'year' => '2018',
                'client_id' => 1
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('periods');
    }
}
