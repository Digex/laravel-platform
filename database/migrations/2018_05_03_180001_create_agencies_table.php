<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agencies', function (Blueprint $table) {
            $table->increments('id');
            $table->String('name', 100);
            $table->String('logo', 50);
            $table->String('address', 100)->nullable();
            $table->String('site', 50)->nullable();;
            $table->timestamps();
            $table->softDeletes();
        });

        DB::table('agencies')->insert(
            array(
                'name' => 'Digizent',
                'logo' => 'agency_1.png'
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agencies');
    }
}
