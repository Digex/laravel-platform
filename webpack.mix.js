let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
 
mix.options({
  processCssUrls: false 
});
mix.js('resources/assets/js/vendor.js', 'public/js')
    .js('resources/assets/js/twitterBootstrap.js', 'public/js')
    .js('resources/assets/js/admin/vueUser.js', 'public/js')
    .js('resources/assets/js/admin/vueAgency.js', 'public/js')
    .js('resources/assets/js/admin/vueAgencyShow.js', 'public/js')
    .js('resources/assets/js/admin/vueAgencyEdit.js', 'public/js')
    .js('resources/assets/js/admin/vueClient.js', 'public/js')
    .js('resources/assets/js/admin/vueClientShow.js', 'public/js')
    .js('resources/assets/js/admin/vueClientCreate.js', 'public/js')
    .js('resources/assets/js/admin/vueClientEdit.js', 'public/js')
    .js('resources/assets/js/admin/vueGetAnalytics.js', 'public/js')
    .js('resources/assets/js/admin/vueReportEdit.js', 'public/js')
    .js('resources/assets/js/admin/vueReportCreate.js', 'public/js')
    .js('resources/assets/js/admin/vueReportList.js', 'public/js')
    .js('resources/assets/js/admin/vueReportAgencyList.js', 'public/js')
    .js('resources/assets/js/dashboard/vueGoogle.js', 'public/js')
    .js('resources/assets/js/dashboard/vueStatus.js', 'public/js')
    .js('resources/assets/js/dashboard/vueServer.js', 'public/js')
    .js('resources/assets/js/dashboard/vueCode.js', 'public/js')
    .js('resources/assets/js/dashboard/vueIssue.js', 'public/js')
    .js('resources/assets/js/dashboard/vueComment.js', 'public/js')
    .js('resources/assets/js/dashboard/vueDashboard.js', 'public/js')
    .js('resources/assets/js/dashboard/vueSecurity.js', 'public/js')
    .js('resources/assets/js/core/vueBus.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css');
